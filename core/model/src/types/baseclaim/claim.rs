use std::{
    fmt::Display,
    sync::{Arc, Mutex},
};

use uuid::Uuid;

use crate::{ClaimData, ClaimDataItem, ClaimLike, ClaimType, Role};

#[derive(Clone, Debug)]
pub struct Claim {
    inner: Arc<InnerClaim>,
}

#[derive(Debug)]
pub struct InnerClaim {
    pub id: Uuid,
    pub claim_type: ClaimType,
    pub data: Mutex<ClaimData>,
}

impl Claim {
    pub fn new(id: Uuid, claim_type: ClaimType) -> Self {
        Self {
            inner: Arc::new(InnerClaim {
                id,
                claim_type,
                data: Default::default(),
            }),
        }
    }

    pub fn id(&self) -> Uuid {
        self.inner.id
    }

    pub fn roles(&self) -> Vec<Role> {
        self.inner.claim_type.roles()
    }

    pub fn claim_type(&self) -> ClaimType {
        self.inner.claim_type.clone()
    }

    pub fn set(&self, role: Role, value: ClaimDataItem) {
        let mut data = self.inner.data.lock().unwrap();
        data.insert(role, value);
    }

    pub fn inner(&self) -> &Arc<InnerClaim> {
        &self.inner
    }

    #[allow(clippy::mutable_key_type)]
    pub fn data(&self) -> ClaimData {
        self.inner.data.lock().unwrap().clone()
    }
}

impl ClaimLike for &Claim {
    fn get_data(&self, role: &Role) -> Option<ClaimDataItem> {
        let data = self.inner.data.lock().unwrap();
        data.get(role).cloned()
    }
}

impl Display for Claim {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Claim {{ `id`: `{}`, `data`: [", self.id())?;
        for item in self.inner.data.lock().unwrap().iter() {
            write!(f, "{{`{}`: `{:?}`}}, ", item.0.role_name, item.1)?;
        }
        write!(f, "]}}")?;

        Ok(())
    }
}
