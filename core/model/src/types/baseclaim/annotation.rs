use std::{
    fmt::Display,
    sync::{Arc, Mutex},
};

use uuid::Uuid;

use crate::{BaseClaim, ClaimData, ClaimDataItem, ClaimLike, ClaimType, Role};

#[derive(Clone, Debug)]
pub struct Annotation {
    inner: Arc<InnerAnnotation>,
}

#[derive(Debug)]
pub struct InnerAnnotation {
    pub id: Uuid,
    pub claim_type: ClaimType,
    subject: BaseClaim,
    pub data: Mutex<ClaimData>,
}

impl Annotation {
    pub fn new(id: Uuid, claim_type: ClaimType, subject: BaseClaim) -> Self {
        Self {
            inner: Arc::new(InnerAnnotation {
                id,
                claim_type,
                data: Default::default(),
                subject,
            }),
        }
    }

    pub fn id(&self) -> Uuid {
        self.inner.id
    }

    pub fn roles(&self) -> Vec<Role> {
        self.inner.claim_type.roles()
    }

    pub fn claim_type(&self) -> ClaimType {
        self.inner.claim_type.clone()
    }

    pub fn inner(&self) -> &Arc<InnerAnnotation> {
        &self.inner
    }

    pub fn set(&self, role: Role, value: ClaimDataItem) {
        let mut lock = self.inner.data.lock().unwrap();
        lock.insert(role, value);
    }

    pub fn subject(&self) -> &BaseClaim {
        &self.inner.subject
    }

    #[allow(clippy::mutable_key_type)]
    pub fn data(&self) -> ClaimData {
        self.inner.data.lock().unwrap().clone()
    }
}

impl ClaimLike for &Annotation {
    fn get_data(&self, role: &Role) -> Option<ClaimDataItem> {
        let lock = self.inner.data.lock().unwrap();
        lock.get(role).cloned()
    }
}

impl Display for Annotation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Annotation {{ `id`: `{}`, `subject`: `{}` `data`: [",
            self.id(),
            self.subject()
        )?;
        for item in self.inner.data.lock().unwrap().iter() {
            write!(f, "{{`{}`: `{:?}`}}, ", item.0.role_name, item.1)?;
        }
        write!(f, "]}}")?;

        Ok(())
    }
}
