mod annotation;
mod claim;

use std::{collections::HashMap, fmt::Display};

pub use annotation::*;
use chrono::Utc;
pub use claim::*;
use uuid::Uuid;

use crate::{ClaimType, Role};

pub type ClaimData = HashMap<Role, ClaimDataItem>;

#[derive(Clone, Debug)]

pub enum ClaimDataItem {
    String(String),
    Integer(i64),
    Float(f64),
    Boolean(bool),
    Uuid(Uuid),
    DateTime(chrono::DateTime<Utc>),
    Date(chrono::NaiveDate),
}

#[derive(Clone, Debug)]
pub enum BaseClaim {
    Claim(Claim),
    Annotation(Annotation),
}

impl BaseClaim {
    pub fn id(&self) -> Uuid {
        match self {
            BaseClaim::Claim(claim) => claim.id(),
            BaseClaim::Annotation(annotation) => annotation.id(),
        }
    }

    pub fn variant(&self) -> &str {
        match self {
            BaseClaim::Claim(_) => "Claim",
            BaseClaim::Annotation(_) => "Annotation",
        }
    }

    pub fn roles(&self) -> Vec<Role> {
        match self {
            BaseClaim::Claim(claim) => claim.roles(),
            BaseClaim::Annotation(annotation) => annotation.roles(),
        }
    }

    pub fn claim_type(&self) -> ClaimType {
        match self {
            BaseClaim::Claim(claim) => claim.claim_type(),
            BaseClaim::Annotation(annotation) => annotation.claim_type(),
        }
    }

    #[allow(clippy::mutable_key_type)]
    pub fn data(&self) -> ClaimData {
        match self {
            BaseClaim::Claim(claim) => claim.data(),
            BaseClaim::Annotation(annotation) => annotation.data(),
        }
    }

    pub fn get_role_data_item_by_name(&self, name: &str) -> Option<ClaimDataItem> {
        let role = self.claim_type().find_role(name)?;
        self.data().get(&role).cloned()
    }
}

impl Display for BaseClaim {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            BaseClaim::Claim(claim) => write!(f, "{}", claim)?,
            BaseClaim::Annotation(annotation) => write!(f, "{}", annotation)?,
        }
        Ok(())
    }
}

impl ClaimLike for BaseClaim {
    fn get_data(&self, role: &Role) -> Option<ClaimDataItem> {
        match self {
            BaseClaim::Claim(c) => c.get_data(role),
            BaseClaim::Annotation(a) => a.get_data(role),
        }
    }
}

impl Display for ClaimDataItem {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let val = match self {
            ClaimDataItem::String(v) => v.to_string(),
            ClaimDataItem::Integer(v) => v.to_string(),
            ClaimDataItem::Float(v) => v.to_string(),
            ClaimDataItem::Boolean(v) => v.to_string(),
            ClaimDataItem::Uuid(v) => v.to_string(),
            ClaimDataItem::DateTime(v) => v.to_string(),
            ClaimDataItem::Date(v) => v.to_string(),
        };

        write!(f, "{}", val)
    }
}

pub trait ClaimLike {
    fn get_data(&self, role: &Role) -> Option<ClaimDataItem>;
}
