use std::{
    fmt::{Display, Formatter},
    hash::Hasher,
    sync::{Arc, Mutex},
};

use uuid::Uuid;

use crate::LabelType;

#[derive(Clone, Debug)]
pub struct ClaimType {
    pub(crate) inner: Arc<Mutex<InnerClaimType>>,
}

#[derive(Clone, Debug)]
pub struct InnerClaimType {
    pub id: Uuid,
    pub name: String,
    pub claim_type_expression: String, // claim type expression
    pub object_type_expression: Option<String>, // object type expression (substitution expression)
    pub roles: Vec<Role>,
    pub unicities: Vec<Unicity>,
}

impl ClaimType {
    pub fn new(name: String, claim_type_expression: String, roles: Vec<Role>) -> Self {
        ClaimType {
            inner: Arc::new(Mutex::new(InnerClaimType {
                id: Uuid::new_v4(),
                name,
                claim_type_expression,
                object_type_expression: None,
                roles,
                unicities: Default::default(),
            })),
        }
    }

    pub fn id(&self) -> Uuid {
        let claim_type_lock = self.inner.lock().unwrap();
        claim_type_lock.id
    }

    pub fn inner(&self) -> &Arc<Mutex<InnerClaimType>> {
        &self.inner
    }

    pub fn name(&self) -> String {
        let claim_type_lock = self.inner.lock().unwrap();
        claim_type_lock.name.clone()
    }

    pub fn roles(&self) -> Vec<Role> {
        let claim_type_lock = self.inner.lock().unwrap();
        claim_type_lock.roles.clone()
    }

    pub fn find_role(&self, name: &str) -> Option<Role> {
        let claim_type_lock = self.inner.lock().unwrap();
        claim_type_lock
            .roles
            .iter()
            .find(|v| v.role_name == name)
            .cloned()
    }

    pub fn claim_type_expression(&self) -> String {
        let claim_type_lock = self.inner.lock().unwrap();
        claim_type_lock.claim_type_expression.clone()
    }

    pub fn set_object_type_expression(
        &self,
        object_type_expression: String,
    ) -> Result<(), std::io::Error> {
        let mut claim_type_lock = self.inner.lock().unwrap();
        claim_type_lock.add_ote(object_type_expression)
    }

    pub fn object_type_expression(&self) -> Option<String> {
        let claim_type_lock = self.inner.lock().unwrap();
        claim_type_lock.object_type_expression.clone()
    }

    pub fn set_unicity(&self, role_names: Vec<String>) -> Result<(), std::io::Error> {
        let mut claim_type_lock = self.inner.lock().unwrap();
        claim_type_lock.add_unicity(role_names)
    }

    pub fn unicity(&self) -> Vec<Unicity> {
        let claim_type_lock = self.inner.lock().unwrap();
        claim_type_lock.unicities.clone()
    }
}

impl Display for ClaimType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let claim_type_lock = self.inner.lock().unwrap();
        write!(f, "{}", claim_type_lock)
    }
}

impl InnerClaimType {
    pub fn add_unicity<U>(&mut self, role_names: Vec<U>) -> Result<(), std::io::Error>
    where
        U: AsRef<str>,
    {
        let mut unicity_roles: Vec<Role> = vec![];

        for role_name in role_names.iter() {
            let claim_role = self
                .roles
                .iter()
                .find(|v| v.role_name == role_name.as_ref());
            let unicity_role = unicity_roles
                .iter()
                .find(|v| v.role_name == role_name.as_ref());

            if unicity_role.is_some() {
                panic!("Role already added to unicity");
            }

            if let Some(role) = claim_role {
                unicity_roles.push(role.clone());
            } else {
                panic!(
                    "Role `{}` not found in claim type `{}` roles",
                    role_name.as_ref(),
                    self.name
                );
            }
        }

        self.unicities.push(Unicity {
            roles: unicity_roles,
        });

        Ok(())
    }

    pub fn add_ote(&mut self, object_type_expression: String) -> Result<(), std::io::Error> {
        let parsed_expresssion = crate::parse_expression(object_type_expression.as_ref())?;

        let mut role_names = vec![];

        for part in parsed_expresssion.parts {
            if let crate::ExpressionPart::Role(role) = part {
                role_names.push(role.role_name.inner());
            }
        }

        let mut ote_roles: Vec<Role> = vec![];

        if role_names.len() != self.roles.len() {
            panic!("Number of roles in object type expression does not match number of roles in claim type expression");
        }

        for role_name in role_names.iter() {
            let claim_role = self.roles.iter().find(|v| v.role_name == *role_name);
            let unicity_role = ote_roles.iter().find(|v| v.role_name == *role_name);

            if unicity_role.is_some() {
                panic!("Role already added to unicity");
            }

            if let Some(role) = claim_role {
                ote_roles.push(role.clone());
            } else {
                panic!("Role not found in roles");
            }
        }

        self.object_type_expression = Some(object_type_expression);

        Ok(())
    }
}

impl Display for InnerClaimType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "ClaimType: \n\tname: `{}` \n\tcte: `{}` \n\tote: `{:?}` \n\troles: `{:?}` \n\nunicities: `{:#?}`\n",
            self.name,
            self.claim_type_expression,
            self.object_type_expression,
            self.roles,
            self.unicities
        )
    }
}

#[derive(Clone, Debug)]
pub enum RoleType {
    LabelType(LabelType),
    ClaimType(ClaimType),
}

impl RoleType {
    pub fn variant(&self) -> String {
        match self {
            RoleType::LabelType(_) => "LabelType".to_string(),
            RoleType::ClaimType(_) => "ClaimType".to_string(),
        }
    }

    pub fn name(&self) -> String {
        match self {
            RoleType::LabelType(v) => v.name().to_string(),
            RoleType::ClaimType(v) => v.name(),
        }
    }

    pub fn id(&self) -> Uuid {
        match self {
            RoleType::LabelType(v) => v.id(),
            RoleType::ClaimType(v) => v.id(),
        }
    }
}

#[derive(Clone, Debug)]
pub struct Role {
    id: Uuid,
    pub role_name: String,
    pub role_type: RoleType,
}

impl Role {
    pub fn new(role_name: String, role_type: RoleType) -> Self {
        Self {
            id: Uuid::new_v4(),
            role_name,
            role_type,
        }
    }

    pub fn id(&self) -> Uuid {
        self.id
    }

    pub fn role_name(&self) -> String {
        self.role_name.clone()
    }

    pub fn role_type(&self) -> RoleType {
        self.role_type.clone()
    }

    pub fn is_annotion(&self) -> bool {
        self.role_type.name().as_str() == crate::RESERVED_ROLE_ANNOTATION
    }
}

impl std::hash::Hash for Role {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}

impl PartialEq for Role {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl Eq for Role {}

#[derive(Clone, Debug)]
pub struct Unicity {
    pub roles: Vec<Role>,
}

impl Unicity {
    pub fn new(roles: Vec<Role>) -> Self {
        Self { roles }
    }

    pub fn roles(&self) -> Vec<Role> {
        self.roles.clone()
    }
}

#[cfg(test)]
mod test {
    use crate::{ClaimDataItem, PrimitiveType};

    use super::*;
    use anyhow::{Ok, Result};

    #[test]
    fn test_claim_type() -> Result<()> {
        let primitive_type_uuid =
            PrimitiveType::new("uuid", |value| value.parse().ok().map(ClaimDataItem::Uuid));
        let primitive_type_string = PrimitiveType::new("string", |value| {
            value.parse().ok().map(ClaimDataItem::String)
        });

        let roles = vec![
            Role::new(
                "Persoon".to_string(),
                RoleType::LabelType(LabelType::new("Persoon", primitive_type_uuid)),
            ),
            Role::new(
                "Gebouw".to_string(),
                RoleType::LabelType(LabelType::new("Gebouw", primitive_type_string)),
            ),
        ];
        let claim_type = ClaimType::new(
            "claim_type".to_string(),
            "De eigenaar van <Gebouw:string> is het persoon <Persoon:uuid>".to_string(),
            roles,
        );

        let mut claim_type_lock = claim_type.inner.lock().unwrap();
        claim_type_lock.add_unicity(vec!["Persoon".to_string(), "Gebouw".to_string()])?;

        println!("{:#?}", claim_type);

        Ok(())
    }
}
