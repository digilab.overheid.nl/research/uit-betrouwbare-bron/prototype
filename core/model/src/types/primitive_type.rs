use std::{
    fmt::{Display, Formatter},
    sync::Arc,
};

use crate::ClaimDataItem;

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub struct PrimitiveType {
    pub(crate) inner: Arc<InnerPrimitiveType>,
}

#[derive(Debug, Hash, PartialEq, Eq)]
pub struct InnerPrimitiveType {
    pub name: String,
    parser: ParseFunction,
}

pub type ParseFunction = fn(&str) -> Option<ClaimDataItem>;

impl PrimitiveType {
    pub fn new<T>(name: T, parser: ParseFunction) -> Self
    where
        T: Into<String>,
    {
        PrimitiveType {
            inner: Arc::new(InnerPrimitiveType {
                name: name.into(),
                parser,
            }),
        }
    }

    pub fn name(&self) -> &str {
        &self.inner.name
    }

    pub fn parse(&self, value: &str) -> Option<ClaimDataItem> {
        (self.inner.parser)(value)
    }
}

impl Display for PrimitiveType {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "Primitive Type: {}", self.name())
    }
}
