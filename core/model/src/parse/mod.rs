use pest::Parser;
use pest_derive::Parser;

#[derive(Parser)]
#[grammar = "./parse/grammar.pest"]
struct ExpressionParse;

#[derive(Clone, Debug)]
pub struct Expression {
    pub parts: Vec<ExpressionPart>,
}

#[derive(Clone, Debug, PartialEq)]
pub enum ExpressionPart {
    Text(ExpressionText),
    Role(ExpressionRole),
}

#[derive(Clone, Debug, PartialEq)]
pub struct ExpressionRole {
    pub role_name: ExpressionText,
    pub role_type: Option<ExpressionText>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct ExpressionText(String);
impl ExpressionText {
    fn new(text: &str) -> Self {
        // TODO: cleanup parser to remove need for this
        ExpressionText(
            text.replace("\\\\", "\\")
                .replace("\\>", ">")
                .replace("\\<", "<")
                .replace("\\:", ":"),
        )
    }

    pub fn inner(&self) -> String {
        self.0.clone()
    }
}

pub fn parse_expression(expression_str: &str) -> Result<Expression, std::io::Error> {
    let mut expression = Expression { parts: Vec::new() };

    let pairs = ExpressionParse::parse(Rule::expression, expression_str)
        .unwrap_or_else(|e| panic!("{}", e));

    for pair in pairs {
        match pair.as_rule() {
            Rule::text => {
                expression
                    .parts
                    .push(ExpressionPart::Text(ExpressionText::new(pair.as_str())));
            }
            Rule::role => {
                let mut role_pair = pair.into_inner();

                let role_name = ExpressionText::new(role_pair.next().unwrap().as_str());
                let role_type = role_pair.next().map(|r| ExpressionText::new(r.as_str()));

                expression.parts.push(ExpressionPart::Role(ExpressionRole {
                    role_name,
                    role_type,
                }));
            }
            Rule::EOI => {}
            rule => {
                panic!("Unknown rule: {:?}", rule);
            }
        }
    }

    Ok(expression)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parse_expression_1() {
        let exp = r#"De <lengte:grootheid> is \> dan <meter:eenheid>"#;

        let res = parse_expression(exp).unwrap();

        println!("{:#?}", res);
        assert_eq!(res.parts.len(), 4);

        assert_eq!(
            res.parts[0],
            ExpressionPart::Text(ExpressionText::new("De "))
        );
        assert_eq!(
            res.parts[1],
            ExpressionPart::Role(ExpressionRole {
                role_name: ExpressionText::new("lengte"),
                role_type: Some(ExpressionText::new("grootheid")),
            })
        );
        assert_eq!(
            res.parts[2],
            ExpressionPart::Text(ExpressionText::new("is > dan"))
        );
        assert_eq!(
            res.parts[3],
            ExpressionPart::Role(ExpressionRole {
                role_name: ExpressionText::new("meter"),
                role_type: Some(ExpressionText::new("eenheid")),
            })
        );
    }

    #[test]
    fn test_parse_expression_2() {
        let exp = r#"De oppervlak van een <Vierkant:Vorm> is \>= 2 * <lengte:meter>"#;

        let res = parse_expression(exp).unwrap();

        println!("{:#?}", res);
        assert_eq!(res.parts.len(), 4);

        assert_eq!(
            res.parts[0],
            ExpressionPart::Text(ExpressionText::new("De oppervlak van een "))
        );
        assert_eq!(
            res.parts[1],
            ExpressionPart::Role(ExpressionRole {
                role_name: ExpressionText::new("Vierkant"),
                role_type: Some(ExpressionText::new("Vorm")),
            })
        );
        assert_eq!(
            res.parts[2],
            ExpressionPart::Text(ExpressionText::new("is >= 2 *"))
        );
        assert_eq!(
            res.parts[3],
            ExpressionPart::Role(ExpressionRole {
                role_name: ExpressionText::new("lengte"),
                role_type: Some(ExpressionText::new("meter")),
            })
        );
    }

    #[test]
    fn test_parse_expression_3() {
        let exp =
            r#"De oppervlak: van een <Vier\:k\\ant:V\>orm><Henk> is \>= dan 2 * <lengte:meter>"#;

        let res = parse_expression(exp).unwrap();

        println!("{:#?}", res);
        assert_eq!(res.parts.len(), 5);

        assert_eq!(
            res.parts[0],
            ExpressionPart::Text(ExpressionText::new("De oppervlak: van een "))
        );
        assert_eq!(
            res.parts[1],
            ExpressionPart::Role(ExpressionRole {
                role_name: ExpressionText::new("Vier:k\\ant"),
                role_type: Some(ExpressionText::new("V\\>orm")),
            })
        );

        assert_eq!(
            res.parts[2],
            ExpressionPart::Role(ExpressionRole {
                role_name: ExpressionText::new("Henk"),
                role_type: None,
            })
        );
        assert_eq!(
            res.parts[3],
            ExpressionPart::Text(ExpressionText::new("is >= dan 2 *"))
        );
        assert_eq!(
            res.parts[4],
            ExpressionPart::Role(ExpressionRole {
                role_name: ExpressionText::new("lengte"),
                role_type: Some(ExpressionText::new("meter")),
            })
        );
    }
}
