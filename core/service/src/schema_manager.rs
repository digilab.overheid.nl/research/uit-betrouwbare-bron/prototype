use std::{
    fmt::Display,
    sync::{Arc, Mutex},
};

use crate::schema::Schema;

#[derive(Default, Clone)]
pub struct SchemaManager {
    pub schemas: Arc<Mutex<Vec<Schema>>>,
}

impl SchemaManager {
    pub fn register_schema(&self, schema: Schema) {
        self.schemas.lock().unwrap().push(schema);
    }

    pub fn get_schema(&self, name: &str) -> Option<Schema> {
        self.schemas
            .lock()
            .unwrap()
            .iter()
            .find(|schema| schema.name() == name)
            .cloned()
    }
}

impl Display for SchemaManager {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for schema in self.schemas.lock().unwrap().iter() {
            writeln!(f, "{}", schema)?;
        }

        Ok(())
    }
}
