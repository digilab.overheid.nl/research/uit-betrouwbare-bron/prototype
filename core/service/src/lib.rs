pub mod api;
pub mod bootstrap;
pub mod schema;
pub mod schema_manager;

#[cfg(test)]
mod test {
    use std::collections::HashMap;

    use anyhow::Result;

    use core_model::PrimitiveType;
    use storage_inmem::InMemStore;

    use crate::{api::api_add, schema::Schema, schema_manager::SchemaManager};

    #[test]
    fn test_engine() -> Result<()> {
        let storage = InMemStore::new_arc();

        let schema = Schema::new(storage, "default")?;

        let primitive_type_uuid = schema.get_primitive_type("uuid").unwrap();
        let primitive_type_string = schema.get_primitive_type("string").unwrap();

        schema.add_label_type("id", &primitive_type_uuid)?;
        schema.add_label_type("voornaam", &primitive_type_string)?;

        let unknown_primitive = PrimitiveType::new("unknown_primitive", |_value| None);

        let res = schema.add_label_type("lengte", &unknown_primitive);
        assert!(res.is_err());

        schema.add_primitive_type(unknown_primitive.clone())?;
        let res = schema.add_label_type("lengte", &unknown_primitive);
        assert!(res.is_ok());

        println!("{}", schema);
        Ok(())
    }

    #[tokio::test]
    async fn test_acync() -> Result<()> {
        let storage = InMemStore::new_arc();

        let schema_manager = SchemaManager::default();

        let schema1 = Schema::new(storage.clone(), "s1")?;
        schema_manager.register_schema(schema1.clone());

        let unknown_primitive = PrimitiveType::new("unknown_primitive", |_value| None);

        async fn add_primitive_type(schema: &Schema, primitive_type: PrimitiveType) -> Result<()> {
            schema.add_primitive_type(primitive_type)?;
            Ok(())
        }

        async fn add_label(schema: &Schema, primitive_type: &PrimitiveType) -> Result<()> {
            schema.add_label_type("lengte", primitive_type)?;
            Ok(())
        }

        add_primitive_type(&schema1, unknown_primitive.clone()).await?;

        add_label(&schema1, &unknown_primitive).await?;

        println!("{}", schema_manager);

        Ok(())
    }

    #[test]
    fn test_claim() -> Result<()> {
        println!("Starting");

        println!("Setting up schema");
        // Assume the schema setup code is not accessible during normal operations
        let schema = {
            let storage = InMemStore::new_arc();
            let schema = Schema::new(storage, "test_schema")?;

            let primitive_type_uuid = schema.get_primitive_type("uuid").unwrap();
            let primitive_type_string = schema.get_primitive_type("string").unwrap();
            let primitive_type_integer = schema.get_primitive_type("integer").unwrap();
            let primitive_type_datetime = schema.get_primitive_type("datetime").unwrap();

            // Meta claim types
            schema.add_label_type("claim_id", &primitive_type_uuid)?;
            schema.add_label_type("datetime", &primitive_type_datetime)?;

            // schema.add_label_type("$claim", &PRIMITIVE_TYPE_UUID)?;
            schema.add_claim_type("$claim", "claim <claim_id> exists")?;

            schema.add_claim_type("Annotation/ExpiredAt", "<$claim> expired at <datetime>")?;

            // Domain claim types
            schema.add_label_type("persoon_id", &primitive_type_integer)?;
            schema.add_label_type("voornaam", &primitive_type_string)?;

            schema.add_claim_type("Persoon", "Persoon <persoon_id> bestaat")?;
            schema.add_claim_type("Persoon/voornaam", "<Persoon> heeft <voornaam>")?;
            schema
        };

        let claim_type = "Persoon";
        let claim_data = HashMap::from([("persoon_id", "1")]);
        let resp = api_add(&schema, claim_type, claim_data)?;
        let persoon_claim_id = resp.to_string();

        let claim_type = "Persoon/voornaam";
        let claim_data = HashMap::from([("voornaam", "Henk"), ("Persoon", &persoon_claim_id)]);

        let resp = api_add(&schema, claim_type, claim_data)?;
        let voornaam_claim_id = resp.to_string();

        let annotation_type = "Annotation/ExpiredAt";
        let now = chrono::Utc::now().to_rfc3339();
        let annotation_data =
            HashMap::from([("$claim", voornaam_claim_id.as_str()), ("datetime", &now)]);

        let resp = api_add(&schema, annotation_type, annotation_data)?;
        let voornaam_annotation_id = resp.to_string();

        println!("voornaam annotation id: {}", voornaam_annotation_id);

        let annotation_type = "Annotation/ExpiredAt";
        let now = chrono::Utc::now().to_rfc3339();
        let annotation_data = HashMap::from([
            ("$claim", voornaam_annotation_id.as_str()),
            ("datetime", &now),
        ]);

        let resp = api_add(&schema, annotation_type, annotation_data)?;
        let annotation_annotation_id = resp.to_string();

        println!("annoration annotation id: {}", annotation_annotation_id);

        Ok(())
    }
}

// Twijfel: 'Domein' Annotation Type: <$claim> wordt betwist door <claimant>
// Valid until: 'Systeem' Annotation Type: <$baseclaim> is geldig tot <date>
//

// Test Henk
//
// Register at: 20-06-2024 10:51
// Claimant: Thom
// Valid from: 19-06-2024

// Command: "Command <id> exists."
// Command/Registered at: "<Command> was registered at <timestamp>"
//      #Registered at
// Command/Claimant: "<Command> was claimed by <?>"
//      #Claimant

// Effect: "Effect <id> exists."
// Effect/Valid from: "<Effect> is valid from <date>"
//      #ValidFrom
// Effect/Command: "<Effect> is part of <Command>"
//      ??? ->
// ... brondocument / rechtgrond / toelichting

// Registration Effect: "<$claim> was registered as part of <Effect>"
// Kind: Registration

// Command = De opdracht die binnengekomen is op de API van het register
//      ? Is het echt 1 moment, of kan dit verspreid zijn over meerdere calls
//      ! Er is tenminste een moment waarop we tot verwerking overgaan = COMMAND!
//      Een register kan prima meerdere soorten API's (Rest, GraphQL, ...)
//      Verschillende granulariteit (Akte, vs Rechtfeit ...)

// Effect / Event
//      Bij complexere commands worden deze uiteengerafeld in meerdere effecten (=events)
//      Bij simpele commands is dit 1:1 (Dus command = effect)

// Relatie effect / claims?
//      Effect kan uit meerdere claims bestaan

// Engine / Schema?
// Schema.Add_dimension("Registration", DiminsionType::TimeLine(Timestamp))
// Schema.Add_dimension("Valid", DiminsionType::TimeLine(Date))
// Schema.Add_dimension("Claimant", DiminsionType::String)
// Schema.Add_dimension("Certainty", DiminsionType::Enum("Certain", "Uncertain"))

// "Commands" uit het gemeentelijk domein
// Schema.Add_dimension("Registration_Gemeente", DiminsionType::TimeLine(Timestamp))
// Schema.Add_dimension("Claimant", DiminsionType::String)
// Schema.Add_dimension("Certainty", DiminsionType::Enum("Certain", "Uncertain"))
// Schema.Add_dimension("Valid_Akte", DiminsionType::TimeLine(Date))
// Schema.Add_dimension("Effect", DiminsionType::Uuid)
// Schema.Add_dimension("Command", DiminsionType::Uuid)

// "Commands" door synchronisatie / maken van leesmodel
// Schema.Add_dimension("Registration_LV_Woz", DiminsionType::TimeLine(Timestamp))

// ?
// Schema.Add_dimension("Valid_Rechter", DiminsionType::TimeLine(Date))
