use core_model::{ClaimDataItem, PrimitiveType};
use storage_common::DataStoreArc;

use crate::{schema::Schema, schema_manager::SchemaManager};

pub fn bootstrap(
    storage: DataStoreArc,
    schema_manager: &SchemaManager,
    schema_name: &str,
) -> Result<Schema, std::io::Error> {
    let schema = Schema::new(storage, schema_name)?;

    schema_manager.register_schema(schema.clone());

    let primitive_type_enum = PrimitiveType::new("enum_primitive_types", |value| match value {
        "uuid" | "string" | "integer" => Some(ClaimDataItem::String(value.to_string())),
        _ => None,
    });

    let must_should_enum = PrimitiveType::new("enum_must_should", |value| match value {
        "must" | "should" => Some(ClaimDataItem::String(value.to_string())),
        _ => None,
    });

    schema.add_primitive_type(primitive_type_enum.clone())?;
    schema.add_primitive_type(must_should_enum.clone())?;

    let primitive_type_uuid = schema.get_primitive_type("uuid").unwrap();
    let primitive_type_string = schema.get_primitive_type("string").unwrap();
    let primitive_type_integer = schema.get_primitive_type("integer").unwrap();

    schema.add_label_type("id", &primitive_type_uuid)?;
    schema.add_label_type("name", &primitive_type_string)?;
    schema.add_label_type("expression", &primitive_type_string)?;
    schema.add_label_type("sequence number", &primitive_type_integer)?;
    schema.add_label_type("primitive type", &primitive_type_enum)?;
    schema.add_label_type("must_should", &must_should_enum)?;
    // enum: must, should
    // Zou mooi zijn als we een waardelijst bij een labeltype zouden kunnen opgeven.
    // Kan wat mij betreft in eerste instantie gewoon als strings die naar het primitieve type vertaald kunnen worden
    // (of je moet direct al een vector van de primitive types willen maken en de conversie bij de creatie doen, is natuurlijk wel veel veiliger)

    // -- Schema

    schema.add_claim_type("Schema", "Schema <id> exists")?;
    schema.add_claim_type_ote("Schema", "schema <id>")?;
    schema.add_claim_type_unicity("Schema", vec!["id"])?;

    schema.add_claim_type("Schema / name", "<Schema> is called <name>")?;
    schema.add_claim_type_unicity("Schema / name", vec!["Schema"])?;
    schema.add_claim_type_unicity("Schema / name", vec!["name"])?;

    // // -- Label type

    schema.add_claim_type("Label type", "Label type <id> exists")?;
    schema.add_claim_type_ote("Label type", "label type <id>")?;
    schema.add_claim_type_unicity("Label type", vec!["id"])?;

    schema.add_claim_type("Label type / name", "<Label type> is called <name>")?;
    schema.add_claim_type_unicity("Label type / name", vec!["Label type"])?;

    schema.add_claim_type(
        "Label type / primitive",
        "<Label type> is implemented as <primitive type>",
    )?;
    schema.add_claim_type_unicity("Label type / primitive", vec!["Label type"])?;

    schema.add_claim_type("Schema / Label type", "<Schema> contains <Label type>")?;
    schema.add_claim_type_unicity("Schema / Label type", vec!["Label type"])?;

    // // -- Claim type

    schema.add_claim_type("Claim type", "Claim type <id> exists")?;
    schema.add_claim_type_ote("Claim type", "claim type <id>")?;
    schema.add_claim_type_unicity("Claim type", vec!["id"])?;

    schema.add_claim_type("Claim type / name", "<Claim type> is called <name>")?;
    schema.add_claim_type_unicity("Claim type / name", vec!["Claim type"])?;

    schema.add_claim_type(
        "Claim type / expression",
        "<Claim type> can be verbalised by \"<expression>\"",
    )?;
    schema.add_claim_type_unicity("Claim type / expression", vec!["Claim type"])?;

    schema.add_claim_type("Claim type / nested expression", "<Claim type> can be verbalised as \"<nested expression:expression>\" when substituted in other expressions")?;
    schema.add_claim_type_unicity("Claim type / nested expression", vec!["Claim type"])?;

    schema.add_claim_type("Schema / Claim type", "<Schema> contains <Claim type>")?;
    schema.add_claim_type_unicity("Schema / Claim type", vec!["Claim type"])?;

    // // -- Role

    schema.add_claim_type("Role", "Role <id> exists")?;
    schema.add_claim_type_ote("Role", "role <id>")?;

    schema.add_claim_type("Role / name", "<Role> is called <name>")?;
    schema.add_claim_type_unicity("Role / name", vec!["Role"])?;

    schema.add_claim_type("Role / part of", "<Role> is part of <part of:Claim type>")?;
    schema.add_claim_type_unicity("Role / part of", vec!["part of", "Role"])?;

    schema.add_claim_type(
        "Role / played by claim type",
        "<Role> is played by <played by claim type:Claim type>",
    )?;
    schema.add_claim_type_unicity("Role / played by claim type", vec!["Role"])?;

    schema.add_claim_type(
        "Role / played by label type",
        "<Role> is played by <played by label type:Label type>",
    )?;
    schema.add_claim_type_unicity("Role / played by label type", vec!["Role"])?;

    // // -- Discuss? Alternative: Introduce subtype that combines Label type and Claim type

    // // -- Verbalisation 'borrowed' from T. Halpin
    schema.add_claim_type("Role / Totality", "Every instance in the population of the type of <Role> <population total:must_should> play that role")?;
    schema.add_claim_type_unicity("Role / Totality", vec!["Role"])?;

    // // -- Unicity
    // // --- Ugly, sets are not supported, so we have to introduce an artificial id

    schema.add_claim_type("Unicity", "Unicity <id> exists")?;
    schema.add_claim_type_ote("Unicity", "unicity <id>")?;
    schema.add_claim_type_unicity("Unicity", vec!["id"])?;

    schema.add_claim_type(
        "Unicity / part of",
        "<Claim type> can be uniquely identified by <Unicity>",
    )?;
    schema.add_claim_type_unicity("Unicity / part of", vec!["Unicity"])?;

    schema.add_claim_type(
        "Unicity / Role",
        "<Unicity> contains <Role> at position <sequence number>",
    )?;

    schema.add_claim_type_unicity("Unicity / Role", vec!["Unicity", "Role"])?;
    schema.add_claim_type_unicity("Unicity / Role", vec!["Unicity", "sequence number"])?;

    Ok(schema)
}

#[cfg(test)]
mod test {

    use crate::schema_manager::SchemaManager;

    use super::*;
    use anyhow::Result;
    use storage_inmem::InMemStore;

    #[test]
    fn bootstrap_test() -> Result<()> {
        let schema_manager = SchemaManager::default();
        let storage = InMemStore::new_arc();

        let schema = bootstrap(storage, &schema_manager, "ace")?;

        println!("{:#?}", schema);

        // assert!(false);
        Ok(())
    }

    #[test]
    fn bootstrap_render_test() -> Result<()> {
        // Render the schema as a mermaid flowchart

        let schema_manager = SchemaManager::default();
        let storage = InMemStore::new_arc();

        let schema = bootstrap(storage, &schema_manager, "ace")?;

        let data = schema.render_to_mermaid()?;
        std::fs::write("docs/bootstrap_schema.md", data).expect("Unable to write file");
        Ok(())
    }
}
