use std::collections::HashMap;

use uuid::Uuid;

use core_model::{Annotation, BaseClaim, Claim, ClaimDataItem, RoleType, RESERVED_ROLE_ANNOTATION};

use crate::schema::Schema;

#[tracing::instrument(name = "api_add", skip(schema, api_type, api_input), level = "info")]
pub fn api_add<T, U>(
    schema: &Schema,
    api_type: &str,
    api_input: HashMap<T, U>,
) -> Result<Uuid, std::io::Error>
where
    T: AsRef<str>,
    U: AsRef<str>,
{
    let claim_type = schema.find_claim_type(api_type)?.unwrap();

    let api_input: HashMap<&str, &str> = api_input
        .iter()
        .map(|(k, v)| (k.as_ref(), v.as_ref()))
        .collect();

    tracing::info!("api_input {} {:#?}", api_type, api_input);

    let mut is_annoration = false;
    for role in &claim_type.roles() {
        if role.is_annotion() {
            is_annoration = true;
        }
    }

    if is_annoration {
        api_add_annotation(schema, api_type, api_input)
    } else {
        api_add_claim(schema, api_type, api_input)
    }
}

#[tracing::instrument(name = "api_add_claim", skip(schema), level = "info")]
fn api_add_claim(
    schema: &Schema,
    api_type: &str,
    api_input: HashMap<&str, &str>,
) -> Result<Uuid, std::io::Error> {
    let claim_type = schema.find_claim_type(api_type)?.unwrap();
    let claim_type_roles = claim_type.roles();

    let claim = Claim::new(Uuid::new_v4(), claim_type);

    for role in claim_type_roles {
        let api_data_item = api_input
            .get(role.role_name.as_str())
            .unwrap_or_else(|| panic!("Role {:?} not found in input", &role.role_name));
        let data_item = match &role.role_type {
            RoleType::LabelType(value) => value
                .inner()
                .primitive_type
                .parse(api_data_item)
                .unwrap_or_else(|| {
                    panic!(
                        "Could not parse data item: `{}` for `{}`",
                        api_data_item, value
                    )
                }),
            RoleType::ClaimType(_) => {
                let id = Uuid::parse_str(api_data_item).expect("Invalid UUID");
                ClaimDataItem::Uuid(id)
            }
        };

        claim.set(role.clone(), data_item)
    }

    let claim_id = claim.id();

    schema.add_base_claim(BaseClaim::Claim(claim))?;

    Ok(claim_id)
}

#[tracing::instrument(name = "api_add_annotation", skip(schema), level = "info")]
fn api_add_annotation(
    schema: &Schema,
    api_type: &str,
    api_input: HashMap<&str, &str>,
) -> Result<Uuid, std::io::Error> {
    let annotation_type = schema.find_claim_type(api_type)?.unwrap();
    let annotation_roles = annotation_type.roles();

    let annotated_claim_id = Uuid::parse_str(
        api_input
            .get(RESERVED_ROLE_ANNOTATION)
            .expect("Annotated claim id missing from input"),
    )
    .expect("Invalid UUID");

    let annotated_claim = schema
        .storage()
        .find_claim(&annotated_claim_id)?
        .ok_or_else(|| {
            std::io::Error::new(std::io::ErrorKind::Other, "Could not find annotated claim")
        })?;

    let annotation = Annotation::new(Uuid::new_v4(), annotation_type, annotated_claim);

    for role in annotation_roles {
        let api_data_item = api_input
            .get(role.role_name.as_str())
            .unwrap_or_else(|| panic!("Role {:?} not found in input", &role.role_name));
        let data_item = match &role.role_type {
            RoleType::LabelType(value) => value
                .inner()
                .primitive_type
                .parse(api_data_item)
                .unwrap_or_else(|| {
                    panic!(
                        "Could not parse data item: `{}` for `{}`",
                        api_data_item, value
                    )
                }),
            RoleType::ClaimType(_) => {
                let id = Uuid::parse_str(api_data_item).expect("Invalid UUID");
                ClaimDataItem::Uuid(id)
            }
        };

        annotation.set(role.clone(), data_item)
    }

    let annotation_id = annotation.id();

    schema.add_base_claim(BaseClaim::Annotation(annotation))?;

    Ok(annotation_id)
}
