use core_model::RoleType;

use super::Schema;
use std::fmt::Write as _;

use std::hash::{DefaultHasher, Hash, Hasher};

// Prevent schema changing by using hashed name instead of uuid
fn calculate_hash<T: Hash>(t: &T) -> u64 {
    let mut s = DefaultHasher::new();
    t.hash(&mut s);
    s.finish()
}

impl Schema {
    pub fn render_to_mermaid(&self) -> Result<String, std::io::Error> {
        // Render the schema as a mermaid flowchart

        let schema = self;

        let name = schema.name();
        let primitives = schema.primitive_types()?;

        let labels = schema.label_types();
        let claims = schema.claim_types();

        let mut data = String::new();

        writeln!(&mut data, "```mermaid")
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;
        writeln!(&mut data, "---\ntitle: {} schema\n---", name)
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;

        writeln!(&mut data, "flowchart LR")
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;
        writeln!(&mut data, "classDef hidden display:none;")
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;
        writeln!(
            &mut data,
            "classDef labelClass stroke-width:2px,stroke-dasharray: 5 5;"
        )
        .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;

        // Primitives
        writeln!(&mut data, "rank_primitive:::hidden")
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;

        for primitive in primitives {
            writeln!(
                &mut data,
                "{}{{{{{}}}}}",
                primitive.name(),
                primitive.name()
            )
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;
            writeln!(&mut data, "{} ~~~ rank_primitive", primitive.name())
                .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;
        }

        // Label types

        writeln!(&mut data, "rank_label:::hidden")
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;
        for label in labels {
            let name = label.inner().name.clone();
            let id = calculate_hash(&format!("label:{}", name));
            let primitive_name = label.inner().primitive_type.name();
            writeln!(&mut data, "{}:::labelClass", id)
                .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;
            writeln!(&mut data, "{}(({}))", id, name)
                .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;
            writeln!(&mut data, "{} ---> {}", id, primitive_name)
                .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;
            writeln!(&mut data, "{} ~~~ rank_label", id)
                .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;
        }

        // Claim types
        //
        writeln!(&mut data, "rank_claim:::hidden")
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;
        for claimtype in claims {
            let inner = claimtype.inner().lock().unwrap();
            let name = inner.name.clone();
            let id = calculate_hash(&format!("claim:{}", name));
            writeln!(&mut data, "{}({})", id, name)
                .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;
            writeln!(&mut data, "{} ~~~ rank_claim", id)
                .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;

            let roles = inner.roles.clone();
            for role in roles {
                let role_type = role.role_type;

                match role_type {
                    RoleType::LabelType(label) => {
                        let label_inner = label.inner();
                        let label_id = calculate_hash(&format!("label:{}", &label_inner.name));
                        writeln!(&mut data, "{} ---> {}", id, label_id).map_err(|e| {
                            std::io::Error::new(std::io::ErrorKind::Other, e.to_string())
                        })?;
                    }
                    RoleType::ClaimType(claim) => {
                        let claim_inner = claim.inner().lock().unwrap();
                        let claim_id = calculate_hash(&format!("claim:{}", &claim_inner.name));
                        writeln!(&mut data, "{} ----> {}", id, claim_id).map_err(|e| {
                            std::io::Error::new(std::io::ErrorKind::Other, e.to_string())
                        })?;
                    }
                };
            }
        }

        writeln!(&mut data, "```")
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;

        Ok(data)
    }
}
