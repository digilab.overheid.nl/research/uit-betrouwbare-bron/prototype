use core_model::{LabelType, PrimitiveType};

use super::Schema;

impl Schema {
    pub fn label_types(&self) -> Vec<LabelType> {
        self.storage().get_label_types().unwrap_or_else(|e| {
            tracing::error!("Failed to get label types: {}", e);
            vec![]
        })
    }

    pub fn find_label_type(&self, name: &str) -> Result<Option<LabelType>, std::io::Error> {
        self.storage().get_label_type(name)
    }

    pub fn add_label_type<T>(
        &self,
        name: T,
        primitive_type: &PrimitiveType,
    ) -> Result<(), std::io::Error>
    where
        T: Into<String>,
    {
        let name = name.into();

        // Check if primitive type is known
        let primitive_type = self
            .storage()
            .find_primitive_type(primitive_type.name())?
            .ok_or_else(|| {
                std::io::Error::new(
                    std::io::ErrorKind::Other,
                    format!("Unknown primitive type: {}", primitive_type.name()),
                )
            })?;

        let label_type = LabelType::new(name, primitive_type.clone());

        self.storage().add_label_type(&label_type)?;

        Ok(())
    }
}
