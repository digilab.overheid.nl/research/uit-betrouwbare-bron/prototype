use std::{
    fmt::{Debug, Display, Formatter},
    sync::Arc,
};

use core_model::{
    types::Role, Annotation, BaseClaim, Claim, ClaimDataItem, ClaimLike, Expression, RoleType,
};
use storage_common::DataStoreArc;

mod claim_types;
mod inner;
mod label_types;
mod primitives;
mod render;
mod roles;

use inner::InnerSchema;
use uuid::Uuid;

#[derive(Clone, Debug)]
pub struct Schema {
    inner: Arc<InnerSchema>,
}

impl Display for Schema {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "{}", self.inner())
    }
}

impl Schema {
    pub fn new<T>(storage: DataStoreArc, name: T) -> Result<Self, std::io::Error>
    where
        T: Into<String>,
    {
        let schema = Schema {
            inner: Arc::new(InnerSchema::new(name.into(), storage)),
        };

        schema.storage().load_state()?;
        Ok(schema)
    }

    pub fn id(&self) -> Uuid {
        self.inner().id
    }

    pub fn name(&self) -> String {
        self.inner().name.clone()
    }

    pub fn storage(&self) -> &DataStoreArc {
        &self.inner().storage
    }

    pub fn add_base_claim(&self, claim: BaseClaim) -> Result<(), std::io::Error> {
        match &claim {
            BaseClaim::Claim(claim) => self.check_claim(claim)?,
            BaseClaim::Annotation(annotation) => self.check_annotation(annotation)?,
        };

        self.storage()
            .add_claim(&claim)
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;

        Ok(())
    }

    fn inner(&self) -> &Arc<InnerSchema> {
        &self.inner
    }

    fn check_claim(&self, claim: &Claim) -> Result<(), std::io::Error> {
        let claimtype_roles = {
            &claim
                .inner()
                .claim_type
                .inner()
                .lock()
                .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?
                .roles
                .clone()
        };

        self.check_data(claimtype_roles, claim)?;

        Ok(())
    }

    fn check_annotation(&self, annotation: &Annotation) -> Result<(), std::io::Error> {
        let claimtype_roles = {
            &annotation
                .inner()
                .claim_type
                .inner()
                .lock()
                .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?
                .roles
                .clone()
        };

        self.check_data(claimtype_roles, annotation)?;
        Ok(())
    }

    pub fn get_roles_from_expression(
        &self,
        expression: Expression,
    ) -> Result<Vec<Role>, std::io::Error> {
        let mut roles = vec![];

        for part in expression.clone().parts {
            if let core_model::ExpressionPart::Role(role) = part {
                let role_name = role.role_name.inner();
                let role_type = role.role_type.map(|v| v.inner());

                let role = self.storage().get_role(role_name, role_type)?;
                roles.push(role);
            }
        }

        Ok(roles)
    }

    /// Check the data of a `ClaimLike` object against the roles of a `ClaimType`
    fn check_data(
        &self,
        claim_type_roles: &[Role],
        data: impl ClaimLike,
    ) -> Result<(), std::io::Error> {
        for claimtype_role in claim_type_roles {
            tracing::debug!(message = "Role check start", claimtype_role.role_name);

            let data = data.get_data(claimtype_role).ok_or(std::io::Error::new(
                std::io::ErrorKind::Other,
                format!("Missing data for role: {}", claimtype_role.role_name),
            ))?;

            // If the role is a claim type, check that a matching claim exists, and that it is of the correct type
            if matches!(&claimtype_role.role_type, RoleType::ClaimType(_)) {
                // A claim type reference can only be an uuid
                let data_id = if let ClaimDataItem::Uuid(data_id) = data {
                    data_id
                } else {
                    return Err(std::io::Error::new(
                        std::io::ErrorKind::Other,
                        format!(
                            "Invalid data for role: `{}`, data `{:?}`",
                            claimtype_role.role_name, data
                        ),
                    ));
                };

                // Find the referenced claim
                let referenced_claim = self.storage().find_claim(&data_id)?;

                // Check if the referenced claim exists
                let name = match referenced_claim {
                    Some(BaseClaim::Claim(claim)) => claim.inner().claim_type.name(),
                    Some(BaseClaim::Annotation(annotation)) => annotation.inner().claim_type.name(),
                    None => {
                        return Err(std::io::Error::new(
                            std::io::ErrorKind::Other,
                            format!(
                                "Invalid referenced claim for role: `{}`, data `{:?}`",
                                claimtype_role.role_name, referenced_claim
                            ),
                        ))
                    }
                };

                // Check if the referenced claim is of the correct type
                // Allow `RESERVED_ROLE_ANNOTATION` as a special case for annotations
                if name != claimtype_role.role_name && !claimtype_role.is_annotion() {
                    return Err(std::io::Error::new(
                        std::io::ErrorKind::Other,
                        format!(
                            "Invalid claim type found for role: `{}`, data `{:?}`",
                            claimtype_role.role_name, data
                        ),
                    ));
                }

                tracing::debug!(message = "Role check OK", claimtype_role.role_name, ?data,);
            }
        }
        Ok(())
    }
}
