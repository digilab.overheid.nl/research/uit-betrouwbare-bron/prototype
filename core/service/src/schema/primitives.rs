use core_model::PrimitiveType;

use super::Schema;

impl Schema {
    pub fn primitive_types(&self) -> Result<Vec<PrimitiveType>, std::io::Error> {
        self.storage().find_primitive_types()
    }

    pub fn add_primitive_type(&self, primitive_type: PrimitiveType) -> Result<(), std::io::Error> {
        self.storage().add_primitive_type(primitive_type)
    }

    pub fn get_primitive_type<T>(&self, name: T) -> Result<PrimitiveType, std::io::Error>
    where
        T: AsRef<str>,
    {
        let primitive_types = self
            .storage()
            .find_primitive_type(name.as_ref())?
            .ok_or_else(|| {
                std::io::Error::new(
                    std::io::ErrorKind::NotFound,
                    format!("Primitive Type {} not found", name.as_ref()),
                )
            })?;

        Ok(primitive_types)
    }
}
