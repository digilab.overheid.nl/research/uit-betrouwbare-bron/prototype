use core_model::Role;

use super::Schema;

impl Schema {
    pub fn roles(&self) -> Vec<Role> {
        self.storage().get_roles().unwrap_or_else(|e| {
            tracing::error!("Failed to get roles: {}", e);
            vec![]
        })
    }
}
