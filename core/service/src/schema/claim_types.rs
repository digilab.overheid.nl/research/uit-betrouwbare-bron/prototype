use core_model::types::ClaimType;

use super::Schema;

impl Schema {
    pub fn claim_types(&self) -> Vec<ClaimType> {
        self.storage().get_claim_types().unwrap_or_else(|e| {
            tracing::error!("Failed to get claim types: {}", e);
            vec![]
        })
    }

    pub fn add_claim_type<T, U>(
        &self,
        name: T,
        claim_type_expression: U,
    ) -> Result<(), std::io::Error>
    where
        T: AsRef<str>,
        U: AsRef<str>,
    {
        let parsed_expresssion = core_model::parse_expression(claim_type_expression.as_ref())?;
        let roles = self.get_roles_from_expression(parsed_expresssion)?;

        let claim_type = ClaimType::new(
            name.as_ref().to_string(),
            claim_type_expression.as_ref().to_string(),
            roles,
        );

        self.storage().add_claim_type(&claim_type)?;

        Ok(())
    }

    pub fn find_claim_type(&self, name: &str) -> Result<Option<ClaimType>, std::io::Error> {
        self.storage().get_claim_type(name)
    }

    pub fn add_claim_type_unicity<T, U>(
        &self,
        name: T,
        role_names: Vec<U>,
    ) -> Result<(), std::io::Error>
    where
        T: AsRef<str>,
        U: AsRef<str>,
    {
        let claim_type = self.set_claim_type_unicity(&name, role_names)?;

        self.storage()
            .update_claim_type(name.as_ref(), &claim_type)?;

        Ok(())
    }
    pub fn set_claim_type_unicity<T, U>(
        &self,
        name: &T,
        role_names: Vec<U>,
    ) -> Result<ClaimType, std::io::Error>
    where
        T: AsRef<str>,
        U: AsRef<str>,
    {
        let claim_type = self.find_claim_type(name.as_ref())?;

        let claim_type = match claim_type {
            Some(claim_type) => claim_type,
            None => {
                return Err(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    format!("Unknown claim type: {}", name.as_ref()),
                ));
            }
        };

        let unicity = role_names
            .into_iter()
            .map(|v| v.as_ref().to_string())
            .collect();
        claim_type.set_unicity(unicity)?;

        Ok(claim_type)
    }

    pub fn add_claim_type_ote<T, U>(
        &self,
        name: T,
        object_type_expression: U,
    ) -> Result<(), std::io::Error>
    where
        T: AsRef<str>,
        U: AsRef<str>,
    {
        let claim_type = self.set_claim_type_ote(&name, object_type_expression)?;

        self.storage()
            .update_claim_type(name.as_ref(), &claim_type)?;

        Ok(())
    }

    pub fn set_claim_type_ote<T, U>(
        &self,
        name: &T,
        object_type_expression: U,
    ) -> Result<ClaimType, std::io::Error>
    where
        T: AsRef<str>,
        U: AsRef<str>,
    {
        let claim_type = self.find_claim_type(name.as_ref())?;

        let claim_type = match claim_type {
            Some(claim_type) => claim_type,
            None => {
                return Err(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    format!("Unknown claim type: {}", name.as_ref()),
                ));
            }
        };

        claim_type.set_object_type_expression(object_type_expression.as_ref().to_string())?;

        Ok(claim_type)
    }
}
