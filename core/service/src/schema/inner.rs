use std::fmt::{Debug, Display, Formatter};

use storage_common::DataStoreArc;
use uuid::Uuid;

pub struct InnerSchema {
    pub(crate) id: Uuid,
    pub(crate) name: String,
    pub(crate) storage: DataStoreArc,
    // pub(crate) primitive_types: Mutex<Vec<PrimitiveType>>, // Settings: Key / Value map
}

impl InnerSchema {
    pub fn new(name: String, storage: DataStoreArc) -> Self {
        InnerSchema {
            id: Uuid::new_v4(),
            name,
            storage,
        }
    }
}

impl Debug for InnerSchema {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("InnerSchema")
            .field("name", &self.name)
            .finish()
        // .field("primitive_types", &self.primitive_types)
        // .field("storage", &self.storage)
    }
}

impl Display for InnerSchema {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        writeln!(f, "Schema: `{}`", self.name)?;

        Ok(())
    }
}
