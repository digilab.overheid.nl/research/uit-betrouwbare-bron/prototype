# ACE (Atomic claim engine)

<!-- ![ace](./Documents/ace.svg) -->
<img alt="alt_text" width="400px" background-color="white" src="./Documents/ace2.svg" />

## Overview

ACE (Atomic claim engine) is a project that aims to provide a powerful and efficient engine for handling data using the claim sourcing paradigma.

## Running the project

TODO: describe, as concise as possible, how to use the project.

## Developer documentation

If you would like to contribute to this project, consult the [`CONTRIBUTING.md`](_docs/CONTRIBUTING.md) file.

## Deployment

### Requirements

- Postgres server

### Installation

To install ACE, follow these steps:

1. Clone the repository
2. Navigate to the project directory
3. Install the dependencies:
    ```bash
    rustup install stable
    ```
4. Build the binary
    ```bash
    cargo build --bin ace_server_gql
    ```
5. Run the binary
    ```
    ./target/debug/ace_server_gql
    ```

## Todo

- use time-rs instead of chrono?
- Add implementation for operations

## License

Copyright © VNG Realisatie 2024

[Licensed under the EUPLv1.2](LICENSE)

[You can find more information about the license here](https://commission.europa.eu/content/european-union-public-licence_en).
