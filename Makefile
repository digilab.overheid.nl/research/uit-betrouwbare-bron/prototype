-include ./local_*/Makefile

PROJECT_NAME := ace
CLUSTER_EXISTS := $$(k3d cluster list $(PROJECT_NAME) --no-headers | wc -l | xargs)
ROOT_DIR := ${PWD}
SKAFFOLD := skaffold
APP_VERSION := $$(git describe --always --dirty=-modified)

# to get the correct KUBECONFIG env var:
# - If no KUBECONFIG has been set, or KUBECONFIG is empty, keep using the default.
# - If KUBECONFIG is a single file, keep using that file.
# - If KUBECONFIG contains multiple files, k3d will be confused.
#   - If the default config (ie. "$HOME/.kube/config") is contained in KUBECONFIG, we use that.
#     Note that this will incorrectly find "$HOME/.kube/config foo" because of limitations in make pattern matching, so don't use spaces ;)
#   - If it is not, we give up, and k3d won't be able to automatically create or delete the kubectl context.
DEFAULT_KUBECONFIG := $(if $(findstring :,${KUBECONFIG}),$(if $(filter ${HOME}/.kube/config,$(subst :, ,${KUBECONFIG})),${HOME}/.kube/config,${KUBECONFIG}),${KUBECONFIG})
KUBECONFIG=$(DEFAULT_KUBECONFIG)

.PHONY: k3d
k3d:
	@if [ $(CLUSTER_EXISTS) -eq 0 ]; then \
		k3d cluster create --config=deploy/k3d/config.yaml; \
		sleep 10; \
		kubectl -n kube-system wait --for=condition=ready pod -l job-name=helm-install-cloudnative-pg; \
		kubectl -n kube-system wait --for=condition=ready=false pod -l job-name=helm-install-cloudnative-pg; \
	else \
		k3d cluster start "$(PROJECT_NAME)"; \
	fi
	kubectl config use-context "k3d-$(PROJECT_NAME)"

.PHONY: dev
dev: k3d
	APP_VERSION=$(APP_VERSION) "$(SKAFFOLD)" dev --filename=.conf/skaffold.yaml --cleanup=false

.PHONY: stop
stop:
	k3d cluster stop "$(PROJECT_NAME)"

.PHONY: clean
clean:
	k3d cluster delete "$(PROJECT_NAME)"

.PHONY: lint
lint:
	cargo fmt --all -- --check
	cargo clippy --all-targets --all-features -- -D warnings

.PHONY: test
test:
	kubectl -n ace exec -it deployments/backend-dpl -- cargo test --all-targets

CASE ?= 000_
CASE_DIR:="$(shell find _docs/cases -type d -name "${CASE}*" | head -n 1)"
.PHONY: case
case:
	@if [ -z "${CASE_DIR}" ]; then \
		echo "No case resembling ${CASE}* found"; \
		false ; \
	fi
	@echo "Running case: ${CASE_DIR}"
	@cargo run \
		--bin case_client \
		-- \
        --schema-file "${CASE_DIR}/schema.gql" \
        --data-file "${CASE_DIR}/data.gql" \
		--action populate

 .PHONY: watch
 watch:
	APP_DATASTORE_TYPE="file" RUST_LOG="debug" cargo watch -x 'run --bin ace_server_gql'

# BASE_URL?="http://claim-backend-127.0.0.1.nip.io:8080"
BASE_URL?="localhost:8000"

.PHONY: group
group:
	@curl \
	${BASE_URL}/test/group \
	| jq --sort-keys
