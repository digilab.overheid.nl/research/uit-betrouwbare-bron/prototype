use postgres_types::ToSql;
use uuid::Uuid;

use core_model::{BaseClaim, Claim, ClaimDataItem, RoleType};
use storage_common::{error::DataStoreError, DBClaimQueryParams};

use crate::claim_type::{
    column_name_from_role_name, table_name_from_claim_type_name, PGSQL_TABLENAME_RE,
};

pub async fn add_claim<'a>(
    conn: &deadpool_postgres::Transaction<'a>,
    new_claim: &BaseClaim,
) -> Result<(), std::io::Error> {
    let (sql, sql_params) = generate_insert_sql_for_claim(new_claim)?;
    let stmt = conn
        .prepare(&sql)
        .await
        .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e))?;
    let _ = conn
        .execute(
            &stmt,
            &sql_params
                .iter()
                .map(|b| b.as_ref())
                .collect::<Vec<&(dyn ToSql + Sync)>>(),
        )
        .await
        .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e))?;

    Ok(()) // RH: WIP:
}

#[allow(dead_code)]
pub async fn find_claims<'a>(
    _conn: &deadpool_postgres::Transaction<'a>,
    _query: DBClaimQueryParams,
) -> Result<Vec<Claim>, DataStoreError> {
    todo!();
}

#[allow(dead_code)]
pub async fn find_open_claims<'a>(
    _conn: &deadpool_postgres::Transaction<'a>,
    _claim_type: &uuid::Uuid,
    _claim_subject: &uuid::Uuid,
) -> Result<Vec<Uuid>, DataStoreError> {
    todo!();
}

#[allow(dead_code)]
pub async fn reset<'a>(_conn: &deadpool_postgres::Transaction<'a>) -> Result<(), DataStoreError> {
    todo!();
}

// Returns a tuple of sql and parameters.
fn generate_insert_sql_for_claim(
    claim: &BaseClaim,
) -> Result<(String, Vec<Box<dyn ToSql + Sync>>), std::io::Error> {
    let table_name = table_name_from_claim_type_name(&claim.claim_type().name());
    // ATTN: This guarantees that no SQL injection can take place, because we only allow ascii alphanumerics and underscores.
    if !PGSQL_TABLENAME_RE.is_match(&table_name) {
        return Err(std::io::Error::new(
            std::io::ErrorKind::Other,
            format!(
                "Claim type name '{}' results in invalid table name '{}'",
                claim.claim_type().name(),
                table_name
            ),
        ));
    }
    let id = claim.id();
    let mut insert_sql = format!("INSERT INTO {table_name} (id,registration_start");
    let mut values_sql = " VALUES ($1,transaction_timestamp()".to_string();
    let mut sql_params: Vec<Box<dyn ToSql + Sync>> = vec![Box::new(id)];
    for (idx, role) in claim.claim_type().roles().iter().enumerate() {
        insert_sql.push(',');
        insert_sql.push_str(&column_name_from_role_name(&role.role_name, idx));
        values_sql.push_str(&format!(",${param_idx}", param_idx = idx + 2));
        let role_value = claim
            .data()
            .get(role)
            .ok_or(std::io::Error::new(
                std::io::ErrorKind::Other,
                "data missing role item",
            ))?
            .clone();
        match role.role_type() {
            RoleType::ClaimType(_) => {
                if let ClaimDataItem::Uuid(role_uuid) = role_value {
                    sql_params.push(Box::new(role_uuid));
                } else {
                    return Err(std::io::Error::new(
                        std::io::ErrorKind::Other,
                        "role referencing a claim must be a UUID",
                    ));
                }
            }
            RoleType::LabelType(_) => {
                sql_params.push(Box::new(serde_json::to_value(role_value.to_string())?));
            }
        };
    }
    insert_sql.push(')');
    values_sql.push(')');

    Ok((insert_sql + &values_sql, sql_params))
}
