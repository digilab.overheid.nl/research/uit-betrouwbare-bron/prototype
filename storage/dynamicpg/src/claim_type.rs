use std::sync::LazyLock;

use ahash::RandomState;
use radix_fmt::radix_36;
use regex::Regex;

use core_model::{ClaimType, RoleType};
use storage_common::error::DataStoreError;

// Regex for unambiguously valid table names for PostgreSQL.
// Note that this is more strict than <https://www.postgresql.org/docs/current/sql-syntax-lexical.html#SQL-SYNTAX-IDENTIFIERS>.
pub static PGSQL_TABLENAME_RE: LazyLock<Regex> =
    LazyLock::new(|| Regex::new(r"^[a-z_][a-z_0-9]*$").unwrap());

pub async fn add_claim_type<'a>(
    conn: &deadpool_postgres::Transaction<'a>,
    new_claim_type: &ClaimType,
) -> Result<(), std::io::Error> {
    for ddl in generate_claims_table_ddl_for_claim_type(new_claim_type)? {
        let stmt = conn
            .prepare(&ddl)
            .await
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e))?;
        let _ = conn
            .execute(&stmt, &[])
            .await
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e))?;
    }
    // RH: WIP: TODO: handle errors
    // RH: WIP: TODO: if successful, add claimtype to claimtype table?
    Ok(())
}

#[allow(dead_code)]
pub async fn expire_claim_type<'a>(
    _conn: &deadpool_postgres::Transaction<'a>,
    _claim_type_id: &uuid::Uuid,
    _valid_end: chrono::DateTime<chrono::Utc>,
) -> Result<(), DataStoreError> {
    todo!();
}

#[allow(dead_code)]
pub async fn find_claim_type<'a, T: AsRef<str>>(
    _conn: &deadpool_postgres::Transaction<'a>,
    _name: T,
) -> Result<Option<ClaimType>, DataStoreError> {
    todo!();
}

pub fn generate_claims_table_ddl_for_claim_type(
    new_claim_type: &ClaimType,
) -> Result<Vec<String>, std::io::Error> {
    let table_name = table_name_from_claim_type_name(&new_claim_type.name());
    // ATTN: This guarantees that no SQL injection can take place, because we only allow ascii alphanumerics and underscores.
    if !PGSQL_TABLENAME_RE.is_match(&table_name) {
        return Err(std::io::Error::new(
            std::io::ErrorKind::Other,
            format!(
                "Claim type name '{}' results in invalid table name '{}'",
                new_claim_type.name(),
                table_name
            ),
        ));
    }
    if new_claim_type.roles().len() > 999 {
        // The way we create safe column names might conceivably break on extremely long role names and huge numbers of roles.
        return Err(std::io::Error::new(
            std::io::ErrorKind::Other,
            format!(
                "Claim type has too many roles: {}, 999 allowed",
                new_claim_type.roles().len()
            ),
        ));
    }
    // Claim tables contain roles that are label types as literal (json-encoded) data, and roles that are claim types as foreign keys.
    // RH: TODO: is it desirable to keep everything as a JSONB `data` field as well?
    let mut create_table_sql = format!(
        "CREATE TABLE {table_name} (\
        id uuid PRIMARY KEY,\
        registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),\
    ",
        table_name = table_name,
    );
    let mut sql_stmts = vec![format!(
        "CREATE INDEX {table_name}_registration_start ON {table_name} (registration_start)",
        table_name = table_name,
    )];
    for (idx, role) in new_claim_type.roles().iter().enumerate() {
        let column_name = column_name_from_role_name(&role.role_name, idx);
        match role.role_type {
            RoleType::ClaimType(_) => {
                create_table_sql.push_str(&format!("{column_name} uuid NOT NULL,",));
                sql_stmts.push(format!(
                    "CREATE INDEX {table_name}_{column_name} ON {table_name} ({column_name})",
                ));
            }
            RoleType::LabelType(_) => {
                create_table_sql.push_str(&format!("{column_name} jsonb NOT NULL,",));
                sql_stmts.push(format!(
                    "CREATE INDEX {table_name}_{column_name} ON {table_name} USING GIN ({column_name})",
                ));
            }
        }
    }
    create_table_sql.pop(); // drop the trailing comma
    create_table_sql.push(')');
    sql_stmts.insert(0, create_table_sql);

    Ok(sql_stmts)
}

const PGSQL_NAMEDATALEN: usize = 64;
const TABLENAME_HASH_LEN: usize = 13;
const MAX_TABLENAME_LEN: usize = PGSQL_NAMEDATALEN - 1;
const TABLENAME_INFIX: &str = "_claims_";
// table name starts with a sluggified claim type name, followed by `_claims_` and a hash of the original name.
const MAX_TABLENAME_SLUG_LEN: usize =
    MAX_TABLENAME_LEN - TABLENAME_HASH_LEN - TABLENAME_INFIX.len();

// This creates table names that are safe to use without escaping or quotes, and are unique and descriptive.
// We'll assume claim type names are mostly ascii, but can deal with diacritics or non-latin unicode.
// (We lose the descriptiveness of the table name in the latter case).
pub fn table_name_from_claim_type_name(name: &str) -> String {
    let mut res = String::with_capacity(MAX_TABLENAME_LEN);
    if let Ok(mut v) = slugify(name) {
        v.truncate(MAX_TABLENAME_SLUG_LEN);
        res.push_str(&v);
    }
    // RH: ATTN: TODO: note that a malicious user might craft claim types names that slugify to the same string, and have the same hash,
    // to cause collisions. I chose this hash for speed and brevity, but the actual seeds might need to be configurable secrets in prod.
    let hash_builder = RandomState::with_seeds(122333444455555, 3141592653589793238, 31337, 42);
    let hash = hash_builder.hash_one(name);

    res.push_str(&format!(
        "{}{:0>13}",
        TABLENAME_INFIX,
        radix_36(hash).to_string()
    ));
    res
}

const MAX_COLUMNNAME_LEN: usize = PGSQL_NAMEDATALEN - 1;
const COLUMNNAME_INFIX: &str = "_role";
// column name starts with a sluggified role name, followed by `_role` and a sequence number (max 999).
const MAX_COLUMNNAME_SLUG_LEN: usize = MAX_COLUMNNAME_LEN - COLUMNNAME_INFIX.len() - 3;

// This creates table column names that are safe to use without escaping or quotes, and are unique and descriptive.
// We'll assume claim type names are mostly ascii, but can deal with diacritics or non-latin unicode.
// (We lose the descriptiveness of the table name in the latter case).
pub fn column_name_from_role_name(name: &str, role_index: usize) -> String {
    let mut res = String::with_capacity(MAX_COLUMNNAME_LEN);
    if let Ok(mut v) = slugify(name) {
        v.truncate(MAX_COLUMNNAME_SLUG_LEN);
        res.push_str(&v);
    }
    res.push_str(COLUMNNAME_INFIX);
    res.push_str(&role_index.to_string());
    res
}

// ATTN: See https://gitlab.com/digilab.overheid.nl/research/uit-betrouwbare-bron/backlog/-/issues/107 for a more permanent solution.
fn slugify(name: &str) -> Result<String, &str> {
    let mut res = String::with_capacity(name.len());
    for c in name.chars() {
        if res.is_empty() {
            // First char can only be alphabetic, no digits.
            if c.is_ascii_alphabetic() {
                res.push(c.to_ascii_lowercase());
            }
        } else {
            // Subsequent chars can be alphanumeric.
            if c.is_ascii_alphanumeric() {
                res.push(c.to_ascii_lowercase());
            }
        }
    }
    if res.is_empty() {
        return Err("Zero-length slug");
    }
    Ok(res)
}

#[cfg(test)]
mod tests {
    use core_model::{ClaimType, LabelType, Role};

    use super::*;

    #[test]
    fn test_generate_claims_table_ddl_for_meta_claim_types() {
        let primitive_type_uuid = core_model::PrimitiveType::new("uuid", |value| {
            value.parse().ok().map(core_model::ClaimDataItem::Uuid)
        });

        // Arrange
        let label_type_ct = ClaimType::new(
            "Label type".to_string(),
            "Label type <id> exists".to_string(),
            vec![Role::new(
                "id".to_string(),
                RoleType::LabelType(LabelType::new("id", primitive_type_uuid)),
            )],
        );
        // Act
        let result = generate_claims_table_ddl_for_claim_type(&label_type_ct);
        // Assert
        match result {
            Ok(value) => assert_eq!(value, vec![
                "CREATE TABLE labeltype_claims_09druuatlmvlz (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),id_role0 jsonb NOT NULL)",
                "CREATE INDEX labeltype_claims_09druuatlmvlz_registration_start ON labeltype_claims_09druuatlmvlz (registration_start)",
                "CREATE INDEX labeltype_claims_09druuatlmvlz_id_role0 ON labeltype_claims_09druuatlmvlz USING GIN (id_role0)",
            ]),
            Err(e) => panic!("Expected Ok variant but got Err: {}", e),
        }
    }

    #[test]
    fn test_generate_claims_table_ddl_for_claim_type() {
        let primitive_type_uuid = core_model::PrimitiveType::new("uuid", |value| {
            value.parse().ok().map(core_model::ClaimDataItem::Uuid)
        });
        let primitive_type_string = core_model::PrimitiveType::new("string", |value| {
            value.parse().ok().map(core_model::ClaimDataItem::String)
        });

        // Arrange
        let persoon_claim_type = ClaimType::new(
            "Persoon".to_string(),
            "Persoon <persoon_id> bestaat".to_string(),
            vec![Role::new(
                "persoon_id".to_string(),
                RoleType::LabelType(LabelType::new("persoon_id", primitive_type_uuid)),
            )],
        );
        let voornaam_claim_type = ClaimType::new(
            "voornaam".to_string(),
            "Persoon <Persoon> heeft als voornaam <Voornaam>".to_string(),
            vec![
                Role::new(
                    "Persoon".to_string(),
                    RoleType::ClaimType(persoon_claim_type),
                ),
                Role::new(
                    "Voornaam".to_string(),
                    RoleType::LabelType(LabelType::new("Voornaam", primitive_type_string)),
                ),
            ],
        );
        // Act
        let result = generate_claims_table_ddl_for_claim_type(&voornaam_claim_type);
        // Assert
        match result {
            Ok(value) => assert_eq!(value, vec![
                "CREATE TABLE voornaam_claims_2v6mfrcv71cnq (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),persoon_role0 uuid NOT NULL,voornaam_role1 jsonb NOT NULL)",
                "CREATE INDEX voornaam_claims_2v6mfrcv71cnq_registration_start ON voornaam_claims_2v6mfrcv71cnq (registration_start)",
                "CREATE INDEX voornaam_claims_2v6mfrcv71cnq_persoon_role0 ON voornaam_claims_2v6mfrcv71cnq (persoon_role0)",
                "CREATE INDEX voornaam_claims_2v6mfrcv71cnq_voornaam_role1 ON voornaam_claims_2v6mfrcv71cnq USING GIN (voornaam_role1)",
            ]),
            Err(e) => panic!("Expected Ok variant but got Err: {}", e),
        }
    }

    #[test]
    fn test_table_name_from_claim_type_name() {
        assert_eq!(
            table_name_from_claim_type_name("a"),
            String::from("a_claims_0u19v4fwhxx5x")
        );
        assert_eq!(
            table_name_from_claim_type_name("f0o"),
            String::from("f0o_claims_33w4ij4l9bga8")
        );
        assert_eq!(
            table_name_from_claim_type_name("F0O"),
            String::from("f0o_claims_3f6yhjvexnspz")
        );
        assert_eq!(
            table_name_from_claim_type_name(" f0o"),
            String::from("f0o_claims_2fygwd7ha9fjo")
        );
        assert_eq!(
            table_name_from_claim_type_name("f0o "),
            String::from("f0o_claims_12iqeqjorot7n")
        );
        assert_eq!(
            table_name_from_claim_type_name("f0 o"),
            String::from("f0o_claims_154ktwqjhos9z")
        );
        assert_eq!(
            table_name_from_claim_type_name("f0_o"),
            String::from("f0o_claims_2v00mwfy9481h")
        );
        assert_eq!(
            table_name_from_claim_type_name("f/0 o"),
            String::from("f0o_claims_12kxc6y7by6le")
        );
        assert_eq!(
            table_name_from_claim_type_name("0foo"),
            String::from("foo_claims_2v1bnfpxmas9v")
        );
        assert_eq!(
            table_name_from_claim_type_name(
                "Foo the whole freaking Bar right in the freaking Baz etcetera"
            ),
            String::from("foothewholefreakingbarrightinthefreakingba_claims_3vj00si0rrro5")
        );

        assert_eq!(
            table_name_from_claim_type_name(""),
            String::from("_claims_1ofq03sxivsu0")
        );
        assert_eq!(
            table_name_from_claim_type_name(" "),
            String::from("_claims_1w01fymfiv3vp")
        );
        assert_eq!(
            table_name_from_claim_type_name("/"),
            String::from("_claims_05090q7xf1zs6")
        );
        assert_eq!(
            table_name_from_claim_type_name("-"),
            String::from("_claims_3uqqdy21yomil")
        );
        assert_eq!(
            table_name_from_claim_type_name("_"),
            String::from("_claims_19evwuidtjh13")
        );
        assert_eq!(
            table_name_from_claim_type_name("__"),
            String::from("_claims_2uprqgivulcv9")
        );
        assert_eq!(
            table_name_from_claim_type_name("0"),
            String::from("_claims_0v5163hz8ptax")
        );
    }

    #[test]
    fn test_column_name_from_role_name() {
        assert_eq!(column_name_from_role_name("a", 0), String::from("a_role0"));
        assert_eq!(
            column_name_from_role_name("f0o", 1),
            String::from("f0o_role1"),
        );
        assert_eq!(
            column_name_from_role_name(
                "Foo the whole freaking Bar right in the freaking Baz etcetera etc and so on",
                999,
            ),
            String::from("foothewholefreakingbarrightinthefreakingbazetceteraetca_role999")
        );
    }

    #[test]
    fn test_slugify() {
        assert_eq!(slugify("a"), Ok(String::from("a")));
        assert_eq!(slugify("f0o"), Ok(String::from("f0o")));
        assert_eq!(slugify("F0O"), Ok(String::from("f0o")));
        assert_eq!(slugify(" f0o"), Ok(String::from("f0o")));
        assert_eq!(slugify("f0o "), Ok(String::from("f0o")));
        assert_eq!(slugify("f0 o"), Ok(String::from("f0o")));
        assert_eq!(slugify("f0_o"), Ok(String::from("f0o")));
        assert_eq!(slugify("f/0 o"), Ok(String::from("f0o")));
        assert_eq!(slugify("0foo"), Ok(String::from("foo")));

        assert!(slugify("").is_err());
        assert!(slugify(" ").is_err());
        assert!(slugify("/").is_err());
        assert!(slugify("-").is_err());
        assert!(slugify("_").is_err());
        assert!(slugify("__").is_err());
        assert!(slugify("0").is_err());
    }
}
