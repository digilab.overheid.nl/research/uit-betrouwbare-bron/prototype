mod bootstrap;
mod claim;
mod claim_type;

use std::{future::Future, sync::Arc};

use deadpool_postgres::{Client, Config, Pool, Transaction};
use tokio::{
    runtime::{Handle, Runtime},
    task,
};
use tokio_postgres::NoTls;
use uuid::Uuid;

use claim::add_claim;
use claim_type::add_claim_type;
use core_model::{BaseClaim, ClaimType, LabelType, Role};
use storage_common::error::DataStoreError;
use storage_common::{DataStore, DataStoreArc};

pub struct DynamicPGStore {
    pool: Pool,
}

impl DynamicPGStore {
    pub fn new_arc(database_url: &str) -> DataStoreArc {
        let mut cfg = Config::new();
        cfg.url = Some(database_url.to_string());
        let pool = cfg
            .create_pool(Some(deadpool_postgres::Runtime::Tokio1), NoTls)
            .unwrap_or_else(|_| panic!("Could not create db pool with url {}", database_url,));
        let data_store = DynamicPGStore { pool };
        data_store
            .bootstrap()
            .unwrap_or_else(|e| panic!("Could not bootstrap data store {}", e));
        Arc::new(data_store)
    }

    // Helper to allow calling async code from sync code, regardless if the current runtime is async.
    // RH: ATTN: TODO: This helper function is here because we're mixing sync and async code.
    // Rethink this. Either a sync db pool, or make the DataStore trait async?
    fn block_on<F: Future>(&self, future: F) -> F::Output {
        match Handle::try_current() {
            Ok(h) => task::block_in_place(|| h.block_on(future)),
            Err(_) => Runtime::new().unwrap().block_on(future),
        }
    }

    // RH: ATTN: TODO: This helper function is here because we're mixing sync and async code.
    // Rethink this. Either a sync db pool, or make the DataStore trait async?
    fn get_conn(&self) -> Result<Client, std::io::Error> {
        self.block_on(self.pool.get())
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e))
    }

    // RH: ATTN: TODO: This helper function is here because we're mixing sync and async code.
    // Rethink this. Either a sync db pool, or make the DataStore trait async?
    // RH: ATTN: TODO: This needs the connection, because I cannot figure out the borrow checker on this one.
    fn get_transaction<'a>(&'a self, conn: &'a mut Client) -> Result<Transaction, std::io::Error> {
        self.block_on(conn.transaction())
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e))
    }
}

type Result<T, E = DataStoreError> = std::result::Result<T, E>;

impl DataStore for DynamicPGStore {
    fn name(&self) -> &str {
        "DynamicPGStore"
    }

    fn reset(&self) -> Result<(), std::io::Error> {
        todo!()
    }

    fn add_label_type(&self, _lt: &LabelType) -> Result<(), std::io::Error> {
        todo!()
    }

    fn get_label_type(&self, _name: &str) -> Result<Option<LabelType>, std::io::Error> {
        todo!()
    }

    fn get_label_types(&self) -> Result<Vec<LabelType>, std::io::Error> {
        todo!()
    }

    fn add_claim_type(&self, ct: &ClaimType) -> Result<(), std::io::Error> {
        tracing::info!("Adding claim type: {}", ct);
        let mut conn = self.get_conn()?;
        let tr = self.get_transaction(&mut conn)?;
        self.block_on(add_claim_type(&tr, ct))
    }

    fn update_claim_type(&self, _name: &str, _ct: &ClaimType) -> Result<(), std::io::Error> {
        todo!()
    }

    fn get_claim_type(&self, _name: &str) -> Result<Option<ClaimType>, std::io::Error> {
        todo!()
    }

    fn get_claim_types(&self) -> Result<Vec<ClaimType>, std::io::Error> {
        // RH: WIP:
        Ok(vec![])
    }

    fn add_claim(&self, claim: &BaseClaim) -> Result<(), std::io::Error> {
        tracing::debug!("Adding claim: {}", claim);
        let mut conn = self.get_conn()?;
        let tr = self.get_transaction(&mut conn)?;
        self.block_on(add_claim(&tr, claim))?;
        self.block_on(tr.commit())
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e))
    }

    fn find_claim(&self, _id: &Uuid) -> Result<Option<BaseClaim>, std::io::Error> {
        todo!()
    }

    fn find_claims(&self, _claim_type: &str) -> Result<Vec<BaseClaim>, std::io::Error> {
        todo!()
    }

    fn load_state(&self) -> Result<(), std::io::Error> {
        // RH: WIP:
        Ok(())
    }

    fn find_primitive_type(
        &self,
        _name: &str,
    ) -> std::result::Result<Option<core_model::PrimitiveType>, std::io::Error> {
        todo!()
    }

    fn find_primitive_types(
        &self,
    ) -> std::result::Result<Vec<core_model::PrimitiveType>, std::io::Error> {
        todo!()
    }

    fn add_primitive_type(
        &self,
        _primitive: core_model::PrimitiveType,
    ) -> std::result::Result<(), std::io::Error> {
        todo!()
    }

    fn get_role(
        &self,
        _role_name: String,
        _role_type: Option<String>,
    ) -> std::result::Result<core_model::Role, std::io::Error> {
        todo!()
    }

    fn get_roles(&self) -> Result<Vec<Role>, std::io::Error> {
        todo!()
    }
}
