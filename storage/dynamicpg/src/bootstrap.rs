use std::collections::{HashMap, HashSet};

use tokio_postgres::error::SqlState;
use uuid::Uuid;

use core_model::{BaseClaim, Claim, ClaimDataItem, ClaimType, RoleType};
use core_service::{bootstrap::bootstrap, schema::Schema, schema_manager::SchemaManager};
use storage_common::DataStore;
use storage_inmem::InMemStore;

use crate::{claim_type::generate_claims_table_ddl_for_claim_type, DynamicPGStore};

impl DynamicPGStore {
    pub fn bootstrap(&self) -> Result<(), std::io::Error> {
        tracing::info!("Bootstrapping meta-schema");
        let mut conn = self.get_conn()?;
        let tr = self.get_transaction(&mut conn)?;
        let mem_data_store = InMemStore::new_arc();
        let schema_manager = SchemaManager::default();
        let schema = bootstrap(mem_data_store, &schema_manager, "ace")?;
        for ddl in generate_bootstrap_ddl(&schema)? {
            let stmt = self
                .block_on(tr.prepare(&ddl))
                .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e))?;
            match self.block_on(tr.execute(&stmt, &[])) {
                Ok(_) => {}
                Err(e) => {
                    if let Some(db_error) = e.as_db_error() {
                        if db_error.code() == &SqlState::DUPLICATE_TABLE {
                            tracing::info!("Meta-schema appears to be bootstrapped already");
                            return Ok(());
                        }
                    }
                    return Err(std::io::Error::new(std::io::ErrorKind::Other, e));
                }
            }
        }
        self.block_on(tr.commit())
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e))?;

        // RH: TODO: these claims should probably be moved to the datastore-agnostic bootstrap in core_service, and here we should just "copy" all claims from inmem to self?
        let schema_meta_claim_ids_by_schema_id = self.bootstrap_schemas(&schema)?;
        let labeltype_meta_claim_ids_by_labeltype_id =
            self.bootstrap_label_types(&schema, &schema_meta_claim_ids_by_schema_id)?;
        let claimtype_meta_claim_ids_by_claimtype_id =
            self.bootstrap_claim_types(&schema, &schema_meta_claim_ids_by_schema_id)?;
        self.bootstrap_roles(
            &schema,
            labeltype_meta_claim_ids_by_labeltype_id,
            claimtype_meta_claim_ids_by_claimtype_id,
        )?;

        Ok(())
    }

    fn bootstrap_claim_types(
        &self,
        schema: &Schema,
        schema_meta_claim_ids_by_schema_id: &HashMap<Uuid, Uuid>,
    ) -> Result<HashMap<Uuid, Uuid>, std::io::Error> {
        let claimtype_meta_ct = find_meta_claim_type(schema, "Claim type")?;
        check_claim_type_role_names(&claimtype_meta_ct, vec!["id"])?;
        let claimtype_id_meta_role = claimtype_meta_ct.find_role("id").unwrap();

        let claimtypename_meta_ct = find_meta_claim_type(schema, "Claim type / name")?;
        check_claim_type_role_names(&claimtypename_meta_ct, vec!["Claim type", "name"])?;
        let claimtypename_claimtype_meta_role =
            claimtypename_meta_ct.find_role("Claim type").unwrap();
        let claimtypename_name_meta_role = claimtypename_meta_ct.find_role("name").unwrap();

        let claimtypeexpression_meta_ct = find_meta_claim_type(schema, "Claim type / expression")?;
        check_claim_type_role_names(
            &claimtypeexpression_meta_ct,
            vec!["Claim type", "expression"],
        )?;
        let claimtypeexpression_claimtype_meta_role =
            claimtypeexpression_meta_ct.find_role("Claim type").unwrap();
        let claimtypeexpression_expression_meta_role =
            claimtypeexpression_meta_ct.find_role("expression").unwrap();

        let claimtypenestedexpression_meta_ct =
            find_meta_claim_type(schema, "Claim type / nested expression")?;
        check_claim_type_role_names(
            &claimtypenestedexpression_meta_ct,
            vec!["Claim type", "nested expression"],
        )?;
        let claimtypenestedexpression_claimtype_meta_role = claimtypenestedexpression_meta_ct
            .find_role("Claim type")
            .unwrap();
        let claimtypenestedexpression_expression_meta_role = claimtypenestedexpression_meta_ct
            .find_role("nested expression")
            .unwrap();

        let schemaclaimtype_meta_ct = find_meta_claim_type(schema, "Schema / Claim type")?;
        check_claim_type_role_names(&schemaclaimtype_meta_ct, vec!["Schema", "Claim type"])?;
        let schemaclaimtype_schema_meta_role = schemaclaimtype_meta_ct.find_role("Schema").unwrap();
        let schemaclaimtype_claimtype_meta_role =
            schemaclaimtype_meta_ct.find_role("Claim type").unwrap();

        let mut claimtype_meta_claim_ids_by_claimtype_id = HashMap::new();
        for claim_type in schema.claim_types() {
            let claimtype_meta_claim_id = Uuid::new_v4(); // The ID of the existential claim for the "Claim type" claim type itself
            claimtype_meta_claim_ids_by_claimtype_id
                .insert(claim_type.id(), claimtype_meta_claim_id);
            let claim = Claim::new(claimtype_meta_claim_id, claimtype_meta_ct.clone());
            claim.set(
                claimtype_id_meta_role.clone(),
                ClaimDataItem::Uuid(claim_type.id()),
            );
            self.add_claim(&BaseClaim::Claim(claim))?;

            let claim = Claim::new(Uuid::new_v4(), claimtypename_meta_ct.clone());
            claim.set(
                claimtypename_claimtype_meta_role.clone(),
                ClaimDataItem::Uuid(claimtype_meta_claim_id),
            );
            claim.set(
                claimtypename_name_meta_role.clone(),
                ClaimDataItem::String(claim_type.name()),
            );
            self.add_claim(&BaseClaim::Claim(claim))?;

            let claim = Claim::new(Uuid::new_v4(), claimtypeexpression_meta_ct.clone());
            claim.set(
                claimtypeexpression_claimtype_meta_role.clone(),
                ClaimDataItem::Uuid(claimtype_meta_claim_id),
            );
            claim.set(
                claimtypeexpression_expression_meta_role.clone(),
                ClaimDataItem::String(claim_type.claim_type_expression()),
            );
            self.add_claim(&BaseClaim::Claim(claim))?;

            if let Some(ote) = claim_type.object_type_expression() {
                let claim = Claim::new(Uuid::new_v4(), claimtypenestedexpression_meta_ct.clone());
                claim.set(
                    claimtypenestedexpression_claimtype_meta_role.clone(),
                    ClaimDataItem::Uuid(claimtype_meta_claim_id),
                );
                claim.set(
                    claimtypenestedexpression_expression_meta_role.clone(),
                    ClaimDataItem::String(ote),
                );
                self.add_claim(&BaseClaim::Claim(claim))?;
            }

            let claim = Claim::new(Uuid::new_v4(), schemaclaimtype_meta_ct.clone());
            claim.set(
                schemaclaimtype_schema_meta_role.clone(),
                ClaimDataItem::Uuid(schema_meta_claim_ids_by_schema_id[&schema.id()]),
            );
            claim.set(
                schemaclaimtype_claimtype_meta_role.clone(),
                ClaimDataItem::Uuid(claimtype_meta_claim_id),
            );
            self.add_claim(&BaseClaim::Claim(claim))?;
        }
        Ok(claimtype_meta_claim_ids_by_claimtype_id)
    }

    fn bootstrap_label_types(
        &self,
        schema: &Schema,
        schema_meta_claim_ids_by_schema_id: &HashMap<Uuid, Uuid>,
    ) -> Result<HashMap<Uuid, Uuid>, std::io::Error> {
        let labeltype_meta_ct = find_meta_claim_type(schema, "Label type")?;
        check_claim_type_role_names(&labeltype_meta_ct, vec!["id"])?;
        let labeltype_id_meta_role = labeltype_meta_ct.find_role("id").unwrap();

        let labeltypename_meta_ct = find_meta_claim_type(schema, "Label type / name")?;
        check_claim_type_role_names(&labeltypename_meta_ct, vec!["Label type", "name"])?;
        let labeltypename_labeltype_meta_role =
            labeltypename_meta_ct.find_role("Label type").unwrap();
        let labeltypename_name_meta_role = labeltypename_meta_ct.find_role("name").unwrap();

        let labeltypeprimitive_meta_ct = find_meta_claim_type(schema, "Label type / primitive")?;
        check_claim_type_role_names(
            &labeltypeprimitive_meta_ct,
            vec!["Label type", "primitive type"],
        )?;
        let labeltypeprimitive_labeltype_meta_role =
            labeltypeprimitive_meta_ct.find_role("Label type").unwrap();
        let labeltypeprimitive_primitivetype_meta_role = labeltypeprimitive_meta_ct
            .find_role("primitive type")
            .unwrap();

        let schemalabeltype_meta_ct = find_meta_claim_type(schema, "Schema / Label type")?;
        check_claim_type_role_names(&schemalabeltype_meta_ct, vec!["Schema", "Label type"])?;
        let schemalabeltype_schema_meta_role = schemalabeltype_meta_ct.find_role("Schema").unwrap();
        let schemalabeltype_labeltype_meta_role =
            schemalabeltype_meta_ct.find_role("Label type").unwrap();

        let mut labeltype_meta_claim_ids_by_labeltype_id = HashMap::new();
        for label_type in schema.label_types() {
            let labeltype_meta_claim_id = Uuid::new_v4(); // The ID of the existential claim for the "Label type" claim type
            labeltype_meta_claim_ids_by_labeltype_id
                .insert(label_type.id(), labeltype_meta_claim_id);
            let claim = Claim::new(labeltype_meta_claim_id, labeltype_meta_ct.clone());
            claim.set(
                labeltype_id_meta_role.clone(),
                ClaimDataItem::Uuid(label_type.id()),
            );
            self.add_claim(&BaseClaim::Claim(claim))?;

            let claim = Claim::new(Uuid::new_v4(), labeltypename_meta_ct.clone());
            claim.set(
                labeltypename_labeltype_meta_role.clone(),
                ClaimDataItem::Uuid(labeltype_meta_claim_id),
            );
            claim.set(
                labeltypename_name_meta_role.clone(),
                ClaimDataItem::String(label_type.name().to_string()),
            );
            self.add_claim(&BaseClaim::Claim(claim))?;

            let claim = Claim::new(Uuid::new_v4(), labeltypeprimitive_meta_ct.clone());
            claim.set(
                labeltypeprimitive_labeltype_meta_role.clone(),
                ClaimDataItem::Uuid(labeltype_meta_claim_id),
            );
            claim.set(
                labeltypeprimitive_primitivetype_meta_role.clone(),
                ClaimDataItem::String(label_type.primitive_type().name().to_string()),
            );
            self.add_claim(&BaseClaim::Claim(claim))?;

            let claim = Claim::new(Uuid::new_v4(), schemalabeltype_meta_ct.clone());
            claim.set(
                schemalabeltype_schema_meta_role.clone(),
                ClaimDataItem::Uuid(schema_meta_claim_ids_by_schema_id[&schema.id()]),
            );
            claim.set(
                schemalabeltype_labeltype_meta_role.clone(),
                ClaimDataItem::Uuid(labeltype_meta_claim_id),
            );
            self.add_claim(&BaseClaim::Claim(claim))?;
        }
        Ok(labeltype_meta_claim_ids_by_labeltype_id)
    }

    fn bootstrap_roles(
        &self,
        schema: &Schema,
        labeltype_meta_claim_ids_by_labeltype_id: HashMap<Uuid, Uuid>,
        claimtype_meta_claim_ids_by_claimtype_id: HashMap<Uuid, Uuid>,
    ) -> Result<HashMap<Uuid, Uuid>, std::io::Error> {
        let role_meta_ct = find_meta_claim_type(schema, "Role")?;
        check_claim_type_role_names(&role_meta_ct, vec!["id"])?;
        let role_id_meta_role = role_meta_ct.find_role("id").unwrap();

        let rolename_meta_ct = find_meta_claim_type(schema, "Role / name")?;
        check_claim_type_role_names(&rolename_meta_ct, vec!["Role", "name"])?;
        let rolename_role_meta_role = rolename_meta_ct.find_role("Role").unwrap();
        let rolename_name_meta_role = rolename_meta_ct.find_role("name").unwrap();

        let roleplayedbyclaimtype_meta_ct =
            find_meta_claim_type(schema, "Role / played by claim type")?;
        check_claim_type_role_names(
            &roleplayedbyclaimtype_meta_ct,
            vec!["Role", "played by claim type"],
        )?;
        let roleplayedbyclaimtype_role_meta_role =
            roleplayedbyclaimtype_meta_ct.find_role("Role").unwrap();
        let roleplayedbyclaimtype_claimtype_meta_role = roleplayedbyclaimtype_meta_ct
            .find_role("played by claim type")
            .unwrap();

        let roleplayedbylabeltype_meta_ct =
            find_meta_claim_type(schema, "Role / played by label type")?;
        check_claim_type_role_names(
            &roleplayedbylabeltype_meta_ct,
            vec!["Role", "played by label type"],
        )?;
        let roleplayedbylabeltype_role_meta_role =
            roleplayedbylabeltype_meta_ct.find_role("Role").unwrap();
        let roleplayedbylabeltype_labeltype_meta_role = roleplayedbylabeltype_meta_ct
            .find_role("played by label type")
            .unwrap();

        let rolepartof_meta_ct = find_meta_claim_type(schema, "Role / part of")?;
        check_claim_type_role_names(&rolepartof_meta_ct, vec!["Role", "part of"])?;
        let rolepartof_role_meta_role = rolepartof_meta_ct.find_role("Role").unwrap();
        let rolepartof_claimtype_meta_role = rolepartof_meta_ct.find_role("part of").unwrap();

        // TODO: Role / Totality (not implemented in core_model yet, only in bootstrap)

        let mut role_meta_claim_ids_by_role_id = HashMap::new();
        for role in schema.roles() {
            let role_meta_claim_id = Uuid::new_v4(); // The ID of the existential claim for the "Role" claim type
            role_meta_claim_ids_by_role_id.insert(role.id(), role_meta_claim_id);
            let claim = Claim::new(role_meta_claim_id, role_meta_ct.clone());
            claim.set(role_id_meta_role.clone(), ClaimDataItem::Uuid(role.id()));
            self.add_claim(&BaseClaim::Claim(claim))?;

            let claim = Claim::new(Uuid::new_v4(), rolename_meta_ct.clone());
            claim.set(
                rolename_role_meta_role.clone(),
                ClaimDataItem::Uuid(role_meta_claim_id),
            );
            claim.set(
                rolename_name_meta_role.clone(),
                ClaimDataItem::String(role.role_name()),
            );
            self.add_claim(&BaseClaim::Claim(claim))?;

            match role.role_type {
                RoleType::ClaimType(ref claim_type) => {
                    let claim = Claim::new(Uuid::new_v4(), roleplayedbyclaimtype_meta_ct.clone());
                    claim.set(
                        roleplayedbyclaimtype_role_meta_role.clone(),
                        ClaimDataItem::Uuid(role_meta_claim_id),
                    );
                    claim.set(
                        roleplayedbyclaimtype_claimtype_meta_role.clone(),
                        ClaimDataItem::Uuid(
                            claimtype_meta_claim_ids_by_claimtype_id[&claim_type.id()],
                        ),
                    );
                    self.add_claim(&BaseClaim::Claim(claim))?;
                }
                RoleType::LabelType(ref label_type) => {
                    let claim = Claim::new(Uuid::new_v4(), roleplayedbylabeltype_meta_ct.clone());
                    claim.set(
                        roleplayedbylabeltype_role_meta_role.clone(),
                        ClaimDataItem::Uuid(role_meta_claim_id),
                    );
                    claim.set(
                        roleplayedbylabeltype_labeltype_meta_role.clone(),
                        ClaimDataItem::Uuid(
                            labeltype_meta_claim_ids_by_labeltype_id[&label_type.id()],
                        ),
                    );
                    self.add_claim(&BaseClaim::Claim(claim))?;
                }
            }
        }
        for claim_type in schema.claim_types() {
            for role in claim_type.roles() {
                let claim = Claim::new(Uuid::new_v4(), rolepartof_meta_ct.clone());
                claim.set(
                    rolepartof_role_meta_role.clone(),
                    ClaimDataItem::Uuid(role_meta_claim_ids_by_role_id[&role.id()]),
                );
                claim.set(
                    rolepartof_claimtype_meta_role.clone(),
                    ClaimDataItem::Uuid(claimtype_meta_claim_ids_by_claimtype_id[&claim_type.id()]),
                );
                self.add_claim(&BaseClaim::Claim(claim))?;
            }
        }
        Ok(role_meta_claim_ids_by_role_id)
    }

    fn bootstrap_schemas(&self, schema: &Schema) -> Result<HashMap<Uuid, Uuid>, std::io::Error> {
        let schema_meta_ct = find_meta_claim_type(schema, "Schema")?;
        check_claim_type_role_names(&schema_meta_ct, vec!["id"])?;
        let schema_id_meta_role = schema_meta_ct.find_role("id").unwrap();

        let schemaname_meta_ct = find_meta_claim_type(schema, "Schema / name")?;
        check_claim_type_role_names(&schemaname_meta_ct, vec!["Schema", "name"])?;
        let schemaname_schema_meta_role = schemaname_meta_ct.find_role("Schema").unwrap();
        let schemaname_name_meta_role = schemaname_meta_ct.find_role("name").unwrap();

        let mut schema_meta_claim_ids_by_schema_id = HashMap::new();

        let schema_meta_claim_id = Uuid::new_v4(); // The ID of the existential claim for the "Schema" claim type
        schema_meta_claim_ids_by_schema_id.insert(schema.id(), schema_meta_claim_id);
        let claim = Claim::new(schema_meta_claim_id, schema_meta_ct.clone());
        claim.set(
            schema_id_meta_role.clone(),
            ClaimDataItem::Uuid(schema.id()),
        );
        self.add_claim(&BaseClaim::Claim(claim))?;

        let claim = Claim::new(Uuid::new_v4(), schemaname_meta_ct.clone());
        claim.set(
            schemaname_schema_meta_role.clone(),
            ClaimDataItem::Uuid(schema_meta_claim_id),
        );
        claim.set(
            schemaname_name_meta_role.clone(),
            ClaimDataItem::String(schema.name()),
        );
        self.add_claim(&BaseClaim::Claim(claim))?;
        Ok(schema_meta_claim_ids_by_schema_id)
    }
}

// This creates the DDL for the meta-schema itself.
fn generate_bootstrap_ddl(schema: &Schema) -> Result<Vec<String>, std::io::Error> {
    let mut sql_stmts: Vec<String> = vec![];
    for claim_type in schema.claim_types() {
        sql_stmts.append(&mut generate_claims_table_ddl_for_claim_type(&claim_type)?);
    }
    Ok(sql_stmts)
}

// Helper to check if a claim type contains the expected roles. This is mostly to guarantee that any meta-model change without changing this bootstrap fails fast.
fn check_claim_type_role_names(
    claim_type: &ClaimType,
    role_names: Vec<&str>,
) -> Result<(), std::io::Error> {
    let ct_roles_set: HashSet<String> = claim_type.roles().iter().map(|r| r.role_name()).collect();
    if ct_roles_set.len() != role_names.len() {
        return Err(std::io::Error::new(
            std::io::ErrorKind::Other,
            format!(
                "Meta-schema for '{ct_name}' has unexpected number of roles ({cnt})",
                ct_name = claim_type.name(),
                cnt = ct_roles_set.len(),
            ),
        ));
    }
    for role_name in role_names {
        if !ct_roles_set.contains(role_name) {
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                format!(
                    "Meta-schema for '{ct_name}' does not have expected role '{role_name}'",
                    ct_name = claim_type.name(),
                ),
            ));
        }
    }
    Ok(())
}

// Helper to get a claim type including error handling.
fn find_meta_claim_type(schema: &Schema, name: &str) -> Result<ClaimType, std::io::Error> {
    schema.find_claim_type(name)?.ok_or(std::io::Error::new(
        std::io::ErrorKind::Other,
        format!("Meta-schema missing claim type '{name}'"),
    ))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_generate_bootstrap_ddl() {
        // Arrange
        let data_store = InMemStore::new_arc();
        let schema_manager = SchemaManager::default();
        let schema = bootstrap(data_store, &schema_manager, "ace").unwrap();
        // Act
        let result = generate_bootstrap_ddl(&schema);
        // Assert
        match result {
            Ok(value) => assert_eq!(value, vec![
                "CREATE TABLE schema_claims_0rgakdffqtowh (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),id_role0 jsonb NOT NULL)",
                "CREATE INDEX schema_claims_0rgakdffqtowh_registration_start ON schema_claims_0rgakdffqtowh (registration_start)",
                "CREATE INDEX schema_claims_0rgakdffqtowh_id_role0 ON schema_claims_0rgakdffqtowh USING GIN (id_role0)",
                "CREATE TABLE schemaname_claims_0lgno0aevhfwq (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),schema_role0 uuid NOT NULL,name_role1 jsonb NOT NULL)",
                "CREATE INDEX schemaname_claims_0lgno0aevhfwq_registration_start ON schemaname_claims_0lgno0aevhfwq (registration_start)",
                "CREATE INDEX schemaname_claims_0lgno0aevhfwq_schema_role0 ON schemaname_claims_0lgno0aevhfwq (schema_role0)",
                "CREATE INDEX schemaname_claims_0lgno0aevhfwq_name_role1 ON schemaname_claims_0lgno0aevhfwq USING GIN (name_role1)",
                "CREATE TABLE labeltype_claims_09druuatlmvlz (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),id_role0 jsonb NOT NULL)",
                "CREATE INDEX labeltype_claims_09druuatlmvlz_registration_start ON labeltype_claims_09druuatlmvlz (registration_start)",
                "CREATE INDEX labeltype_claims_09druuatlmvlz_id_role0 ON labeltype_claims_09druuatlmvlz USING GIN (id_role0)",
                "CREATE TABLE labeltypename_claims_3fwig62wgz59a (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),labeltype_role0 uuid NOT NULL,name_role1 jsonb NOT NULL)",
                "CREATE INDEX labeltypename_claims_3fwig62wgz59a_registration_start ON labeltypename_claims_3fwig62wgz59a (registration_start)",
                "CREATE INDEX labeltypename_claims_3fwig62wgz59a_labeltype_role0 ON labeltypename_claims_3fwig62wgz59a (labeltype_role0)",
                "CREATE INDEX labeltypename_claims_3fwig62wgz59a_name_role1 ON labeltypename_claims_3fwig62wgz59a USING GIN (name_role1)",
                "CREATE TABLE labeltypeprimitive_claims_1an1n8tz4lhu1 (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),labeltype_role0 uuid NOT NULL,primitivetype_role1 jsonb NOT NULL)",
                "CREATE INDEX labeltypeprimitive_claims_1an1n8tz4lhu1_registration_start ON labeltypeprimitive_claims_1an1n8tz4lhu1 (registration_start)",
                "CREATE INDEX labeltypeprimitive_claims_1an1n8tz4lhu1_labeltype_role0 ON labeltypeprimitive_claims_1an1n8tz4lhu1 (labeltype_role0)",
                "CREATE INDEX labeltypeprimitive_claims_1an1n8tz4lhu1_primitivetype_role1 ON labeltypeprimitive_claims_1an1n8tz4lhu1 USING GIN (primitivetype_role1)",
                "CREATE TABLE schemalabeltype_claims_09lwnhwck0hae (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),schema_role0 uuid NOT NULL,labeltype_role1 uuid NOT NULL)",
                "CREATE INDEX schemalabeltype_claims_09lwnhwck0hae_registration_start ON schemalabeltype_claims_09lwnhwck0hae (registration_start)",
                "CREATE INDEX schemalabeltype_claims_09lwnhwck0hae_schema_role0 ON schemalabeltype_claims_09lwnhwck0hae (schema_role0)",
                "CREATE INDEX schemalabeltype_claims_09lwnhwck0hae_labeltype_role1 ON schemalabeltype_claims_09lwnhwck0hae (labeltype_role1)",
                "CREATE TABLE claimtype_claims_006pzzfey6x7u (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),id_role0 jsonb NOT NULL)",
                "CREATE INDEX claimtype_claims_006pzzfey6x7u_registration_start ON claimtype_claims_006pzzfey6x7u (registration_start)",
                "CREATE INDEX claimtype_claims_006pzzfey6x7u_id_role0 ON claimtype_claims_006pzzfey6x7u USING GIN (id_role0)",
                "CREATE TABLE claimtypename_claims_230sg02awdzz9 (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),claimtype_role0 uuid NOT NULL,name_role1 jsonb NOT NULL)",
                "CREATE INDEX claimtypename_claims_230sg02awdzz9_registration_start ON claimtypename_claims_230sg02awdzz9 (registration_start)",
                "CREATE INDEX claimtypename_claims_230sg02awdzz9_claimtype_role0 ON claimtypename_claims_230sg02awdzz9 (claimtype_role0)",
                "CREATE INDEX claimtypename_claims_230sg02awdzz9_name_role1 ON claimtypename_claims_230sg02awdzz9 USING GIN (name_role1)",
                "CREATE TABLE claimtypeexpression_claims_1pfg7ngkzq2wf (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),claimtype_role0 uuid NOT NULL,expression_role1 jsonb NOT NULL)",
                "CREATE INDEX claimtypeexpression_claims_1pfg7ngkzq2wf_registration_start ON claimtypeexpression_claims_1pfg7ngkzq2wf (registration_start)",
                "CREATE INDEX claimtypeexpression_claims_1pfg7ngkzq2wf_claimtype_role0 ON claimtypeexpression_claims_1pfg7ngkzq2wf (claimtype_role0)",
                "CREATE INDEX claimtypeexpression_claims_1pfg7ngkzq2wf_expression_role1 ON claimtypeexpression_claims_1pfg7ngkzq2wf USING GIN (expression_role1)",
                "CREATE TABLE claimtypenestedexpression_claims_3h32pnrf30mg5 (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),claimtype_role0 uuid NOT NULL,nestedexpression_role1 jsonb NOT NULL)",
                "CREATE INDEX claimtypenestedexpression_claims_3h32pnrf30mg5_registration_start ON claimtypenestedexpression_claims_3h32pnrf30mg5 (registration_start)",
                "CREATE INDEX claimtypenestedexpression_claims_3h32pnrf30mg5_claimtype_role0 ON claimtypenestedexpression_claims_3h32pnrf30mg5 (claimtype_role0)",
                "CREATE INDEX claimtypenestedexpression_claims_3h32pnrf30mg5_nestedexpression_role1 ON claimtypenestedexpression_claims_3h32pnrf30mg5 USING GIN (nestedexpression_role1)",
                "CREATE TABLE schemaclaimtype_claims_1e148w8efllbs (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),schema_role0 uuid NOT NULL,claimtype_role1 uuid NOT NULL)",
                "CREATE INDEX schemaclaimtype_claims_1e148w8efllbs_registration_start ON schemaclaimtype_claims_1e148w8efllbs (registration_start)",
                "CREATE INDEX schemaclaimtype_claims_1e148w8efllbs_schema_role0 ON schemaclaimtype_claims_1e148w8efllbs (schema_role0)",
                "CREATE INDEX schemaclaimtype_claims_1e148w8efllbs_claimtype_role1 ON schemaclaimtype_claims_1e148w8efllbs (claimtype_role1)",
                "CREATE TABLE role_claims_1q7b345sy8911 (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),id_role0 jsonb NOT NULL)",
                "CREATE INDEX role_claims_1q7b345sy8911_registration_start ON role_claims_1q7b345sy8911 (registration_start)",
                "CREATE INDEX role_claims_1q7b345sy8911_id_role0 ON role_claims_1q7b345sy8911 USING GIN (id_role0)",
                "CREATE TABLE rolename_claims_2b3ognhyinb5h (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),role_role0 uuid NOT NULL,name_role1 jsonb NOT NULL)",
                "CREATE INDEX rolename_claims_2b3ognhyinb5h_registration_start ON rolename_claims_2b3ognhyinb5h (registration_start)",
                "CREATE INDEX rolename_claims_2b3ognhyinb5h_role_role0 ON rolename_claims_2b3ognhyinb5h (role_role0)",
                "CREATE INDEX rolename_claims_2b3ognhyinb5h_name_role1 ON rolename_claims_2b3ognhyinb5h USING GIN (name_role1)",
                "CREATE TABLE rolepartof_claims_3bl1ogpeam1xt (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),role_role0 uuid NOT NULL,partof_role1 uuid NOT NULL)",
                "CREATE INDEX rolepartof_claims_3bl1ogpeam1xt_registration_start ON rolepartof_claims_3bl1ogpeam1xt (registration_start)",
                "CREATE INDEX rolepartof_claims_3bl1ogpeam1xt_role_role0 ON rolepartof_claims_3bl1ogpeam1xt (role_role0)",
                "CREATE INDEX rolepartof_claims_3bl1ogpeam1xt_partof_role1 ON rolepartof_claims_3bl1ogpeam1xt (partof_role1)",
                "CREATE TABLE roleplayedbyclaimtype_claims_0p5421rzvkuti (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),role_role0 uuid NOT NULL,playedbyclaimtype_role1 uuid NOT NULL)",
                "CREATE INDEX roleplayedbyclaimtype_claims_0p5421rzvkuti_registration_start ON roleplayedbyclaimtype_claims_0p5421rzvkuti (registration_start)",
                "CREATE INDEX roleplayedbyclaimtype_claims_0p5421rzvkuti_role_role0 ON roleplayedbyclaimtype_claims_0p5421rzvkuti (role_role0)",
                "CREATE INDEX roleplayedbyclaimtype_claims_0p5421rzvkuti_playedbyclaimtype_role1 ON roleplayedbyclaimtype_claims_0p5421rzvkuti (playedbyclaimtype_role1)",
                "CREATE TABLE roleplayedbylabeltype_claims_09eoghrl0rakr (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),role_role0 uuid NOT NULL,playedbylabeltype_role1 uuid NOT NULL)",
                "CREATE INDEX roleplayedbylabeltype_claims_09eoghrl0rakr_registration_start ON roleplayedbylabeltype_claims_09eoghrl0rakr (registration_start)",
                "CREATE INDEX roleplayedbylabeltype_claims_09eoghrl0rakr_role_role0 ON roleplayedbylabeltype_claims_09eoghrl0rakr (role_role0)",
                "CREATE INDEX roleplayedbylabeltype_claims_09eoghrl0rakr_playedbylabeltype_role1 ON roleplayedbylabeltype_claims_09eoghrl0rakr (playedbylabeltype_role1)",
                "CREATE TABLE roletotality_claims_195yl0dd0snx6 (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),role_role0 uuid NOT NULL,populationtotal_role1 jsonb NOT NULL)",
                "CREATE INDEX roletotality_claims_195yl0dd0snx6_registration_start ON roletotality_claims_195yl0dd0snx6 (registration_start)",
                "CREATE INDEX roletotality_claims_195yl0dd0snx6_role_role0 ON roletotality_claims_195yl0dd0snx6 (role_role0)",
                "CREATE INDEX roletotality_claims_195yl0dd0snx6_populationtotal_role1 ON roletotality_claims_195yl0dd0snx6 USING GIN (populationtotal_role1)",
                "CREATE TABLE unicity_claims_074s7vdl72og4 (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),id_role0 jsonb NOT NULL)",
                "CREATE INDEX unicity_claims_074s7vdl72og4_registration_start ON unicity_claims_074s7vdl72og4 (registration_start)",
                "CREATE INDEX unicity_claims_074s7vdl72og4_id_role0 ON unicity_claims_074s7vdl72og4 USING GIN (id_role0)",
                "CREATE TABLE unicitypartof_claims_3bu2rbxrxkt52 (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),claimtype_role0 uuid NOT NULL,unicity_role1 uuid NOT NULL)",
                "CREATE INDEX unicitypartof_claims_3bu2rbxrxkt52_registration_start ON unicitypartof_claims_3bu2rbxrxkt52 (registration_start)",
                "CREATE INDEX unicitypartof_claims_3bu2rbxrxkt52_claimtype_role0 ON unicitypartof_claims_3bu2rbxrxkt52 (claimtype_role0)",
                "CREATE INDEX unicitypartof_claims_3bu2rbxrxkt52_unicity_role1 ON unicitypartof_claims_3bu2rbxrxkt52 (unicity_role1)",
                "CREATE TABLE unicityrole_claims_2uiizrpj92lbd (id uuid PRIMARY KEY,registration_start TIMESTAMPTZ NOT NULL DEFAULT NOW(),unicity_role0 uuid NOT NULL,role_role1 uuid NOT NULL,sequencenumber_role2 jsonb NOT NULL)",
                "CREATE INDEX unicityrole_claims_2uiizrpj92lbd_registration_start ON unicityrole_claims_2uiizrpj92lbd (registration_start)",
                "CREATE INDEX unicityrole_claims_2uiizrpj92lbd_unicity_role0 ON unicityrole_claims_2uiizrpj92lbd (unicity_role0)",
                "CREATE INDEX unicityrole_claims_2uiizrpj92lbd_role_role1 ON unicityrole_claims_2uiizrpj92lbd (role_role1)",
                "CREATE INDEX unicityrole_claims_2uiizrpj92lbd_sequencenumber_role2 ON unicityrole_claims_2uiizrpj92lbd USING GIN (sequencenumber_role2)",
            ]),
            Err(e) => panic!("Expected Ok variant but got Err: {}", e),
        }
    }
}
