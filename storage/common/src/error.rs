use snafu::prelude::*;
use snafu::Location;

#[derive(Snafu, Debug)]
#[snafu(visibility(pub))]
pub enum DataStoreError {
    #[snafu(display("migration error: {}", source))]
    Migrate {
        source: std::io::Error,
        #[snafu(implicit)]
        location: Location,
    },
    #[snafu(display("connection error: {}", source))]
    Connect {
        source: std::io::Error,
        #[snafu(implicit)]
        location: Location,
    },
    #[snafu(display("insert error: {}", source))]
    Insert {
        source: std::io::Error,
        #[snafu(implicit)]
        location: Location,
    },
    #[snafu(display("delete error: {}", source))]
    Delete {
        source: std::io::Error,
        #[snafu(implicit)]
        location: Location,
    },
    #[snafu(display("find open error: {}", source))]
    FindOpen {
        source: std::io::Error,
        #[snafu(implicit)]
        location: Location,
    },
    #[snafu(display("find error: {}", source))]
    Find {
        source: std::io::Error,
        #[snafu(implicit)]
        location: Location,
    },
    #[snafu(display("transaction error: {}", source))]
    Transaction {
        source: std::io::Error,
        #[snafu(implicit)]
        location: Location,
    },
    #[snafu(display("conversion error: {}", message))]
    Conversion {
        message: &'static str,
        #[snafu(implicit)]
        location: Location,
    },
    #[snafu(display("parse error: {}", source))]
    Parse {
        source: std::io::Error,
        #[snafu(implicit)]
        location: Location,
    },
    #[snafu(display("dynamic schema creation error: {}", source))]
    DynamicSchemaCreate {
        source: std::io::Error,
        #[snafu(implicit)]
        location: Location,
    },
    #[snafu(display("unknown data store error"))]
    Unknown {
        #[snafu(implicit)]
        location: Location,
    },
    #[snafu(display("not found error"))]
    NotFound {
        #[snafu(implicit)]
        location: Location,
    },
}
