use std::sync::Arc;

use core_model::{BaseClaim, ClaimType, LabelType, PrimitiveType, Role};

pub mod error;

pub trait DataStore {
    fn name(&self) -> &str;
    fn reset(&self) -> Result<(), std::io::Error>;

    fn add_primitive_type(&self, primitive: PrimitiveType) -> Result<(), std::io::Error>;
    fn find_primitive_type(&self, name: &str) -> Result<Option<PrimitiveType>, std::io::Error>;
    fn find_primitive_types(&self) -> Result<Vec<PrimitiveType>, std::io::Error>;

    fn get_role(
        &self,
        role_name: String,
        role_type: Option<String>,
    ) -> Result<Role, std::io::Error>;
    fn get_roles(&self) -> Result<Vec<Role>, std::io::Error>;

    fn add_label_type(&self, lt: &LabelType) -> Result<(), std::io::Error>;
    fn get_label_type(&self, name: &str) -> Result<Option<LabelType>, std::io::Error>;
    fn get_label_types(&self) -> Result<Vec<LabelType>, std::io::Error>;

    fn add_claim_type(&self, ct: &ClaimType) -> Result<(), std::io::Error>;
    fn update_claim_type(&self, name: &str, ct: &ClaimType) -> Result<(), std::io::Error>;
    fn get_claim_type(&self, name: &str) -> Result<Option<ClaimType>, std::io::Error>;
    fn get_claim_types(&self) -> Result<Vec<ClaimType>, std::io::Error>;

    fn add_claim(&self, claim: &BaseClaim) -> Result<(), std::io::Error>;
    fn find_claim(&self, id: &uuid::Uuid) -> Result<Option<BaseClaim>, std::io::Error>;
    fn find_claims(&self, claim_type: &str) -> Result<Vec<BaseClaim>, std::io::Error>;

    fn load_state(&self) -> Result<(), std::io::Error>;
}

pub type DataStoreArc = Arc<dyn DataStore + Send + Sync + 'static>;

pub struct DBClaimQueryParams {}
pub struct NewDBClaim {
    pub registration_start: chrono::DateTime<chrono::Utc>,
}
