use std::{
    fmt::{self, Display, Formatter},
    fs::{File, OpenOptions},
    io::{Read as _, Write},
    path::PathBuf,
    sync::{Arc, Mutex},
};

use serde::{Deserialize, Serialize};
use uuid::Uuid;

use core_model::{
    Annotation, BaseClaim, Claim, ClaimDataItem, ClaimType, LabelType, Role, RoleType,
};
use storage_common::{DataStore, DataStoreArc};
use storage_inmem::InMemStore;

pub struct FileStore {
    inner: Arc<Mutex<File>>,
    inmem: InMemStore,
}

impl FileStore {
    pub fn new_arc(file_location: &PathBuf) -> DataStoreArc {
        let file = OpenOptions::new()
            .create(true)
            .read(true)
            .append(true)
            .open(file_location)
            .unwrap_or_else(|_| {
                panic!(
                    "Could not open file at location {:?}",
                    file_location.to_str()
                )
            });

        Arc::new(FileStore {
            inner: Arc::new(Mutex::new(file)),
            inmem: InMemStore::new(),
        })
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
struct RoleData {
    role_name: String,
    role_type_variant: String,
    role_type_name: String,
    primitive_type: Option<String>,
    role_data: String,
}
#[derive(Clone, Debug, Serialize, Deserialize)]
struct ClaimData {
    id: uuid::Uuid,
    variant: String,
    claim_type: String,
    subject: Option<Uuid>,
    data: Vec<RoleData>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
enum StoredItemType {
    LabelType(String),
    ClaimType(String),
    ObjectTypeExpression(String),
    Unicity(String),
    Claim(ClaimData),
}

impl StoredItemType {
    fn name(&self) -> String {
        match self {
            StoredItemType::LabelType(_) => "LabelType".to_string(),
            StoredItemType::ClaimType(_) => "ClaimType".to_string(),
            StoredItemType::ObjectTypeExpression(_) => "ObjectTypeExpression".to_string(),
            StoredItemType::Unicity(_) => "Unicity".to_string(),
            StoredItemType::Claim(_) => "Claim".to_string(),
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
struct StoredItem {
    item_name: String,
    item_type: StoredItemType,
}

impl Display for StoredItem {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{} -> {}", self.item_type.name(), self.item_name)
    }
}

impl DataStore for FileStore {
    fn name(&self) -> &str {
        "FileStore"
    }

    fn reset(&self) -> Result<(), std::io::Error> {
        self.inner.lock().unwrap().set_len(0)?;
        self.inmem.reset()?;
        Ok(())
    }

    fn find_claim(&self, id: &uuid::Uuid) -> Result<Option<BaseClaim>, std::io::Error> {
        self.inmem.find_claim(id)
    }

    fn find_claims(&self, claim_type: &str) -> Result<Vec<BaseClaim>, std::io::Error> {
        self.inmem.find_claims(claim_type)
    }

    fn add_claim(&self, claim: &BaseClaim) -> Result<(), std::io::Error> {
        self.inmem.add_claim(claim)?;

        // Store in file
        let role_data = claim
            .data()
            .into_iter()
            .map(|(k, v)| {
                let primitive_type = if let RoleType::LabelType(l) = k.role_type() {
                    Some(l.primitive_type().name().to_string())
                } else {
                    None
                };

                RoleData {
                    role_name: k.role_name.clone(),
                    role_type_variant: k.role_type().variant().to_string(),
                    role_type_name: k.role_type().name().to_string(),
                    primitive_type,
                    role_data: v.to_string(),
                }
            })
            .collect::<Vec<RoleData>>();

        let id = claim.id();
        let ct = claim.claim_type().name();
        let variant = claim.variant();
        let subject = if let BaseClaim::Annotation(a) = claim {
            Some(a.subject().id())
        } else {
            None
        };

        let item = StoredItem {
            item_name: format!("Claim-{}", id),
            item_type: StoredItemType::Claim(ClaimData {
                id,
                variant: variant.to_string(),
                claim_type: ct.to_string(),
                subject,
                data: role_data,
            }),
        };

        write_item(&mut self.inner.lock().unwrap(), item)?;

        Ok(())
    }

    fn add_label_type(&self, lt: &LabelType) -> Result<(), std::io::Error> {
        let label_type = lt.name();
        let primitive_type = lt.primitive_type();

        tracing::info!("Adding label type: {} {}", label_type, primitive_type);

        self.inmem.add_label_type(lt)?;

        // Store in file
        let item = StoredItem {
            item_type: StoredItemType::LabelType(primitive_type.name().to_string()),
            item_name: label_type.to_string(),
        };

        write_item(&mut self.inner.lock().unwrap(), item)?;

        Ok(())
    }

    fn get_label_type(&self, name: &str) -> Result<Option<LabelType>, std::io::Error> {
        self.inmem.get_label_type(name)
    }

    fn get_label_types(&self) -> Result<Vec<LabelType>, std::io::Error> {
        self.inmem.get_label_types()
    }

    fn add_claim_type(&self, ct: &ClaimType) -> Result<(), std::io::Error> {
        let claim_type = ct.name();
        let cte = ct.inner().lock().unwrap().claim_type_expression.clone();

        self.inmem.add_claim_type(ct)?;

        // Store in file
        let item = StoredItem {
            item_type: StoredItemType::ClaimType(cte.to_string()),
            item_name: claim_type.to_string(),
        };

        write_item(&mut self.inner.lock().unwrap(), item)?;

        Ok(())
    }

    fn update_claim_type(&self, name: &str, ct_update: &ClaimType) -> Result<(), std::io::Error> {
        self.inmem.update_claim_type(name, ct_update)?;

        // Store in file
        let ote = ct_update.object_type_expression();

        if let Some(ote) = ote {
            let item = StoredItem {
                item_type: StoredItemType::ObjectTypeExpression(ote),
                item_name: name.to_string(),
            };
            write_item(&mut self.inner.lock().unwrap(), item)?;
        }

        let unicities = ct_update
            .unicity()
            .iter()
            .map(|v| {
                v.roles()
                    .iter()
                    .map(|v| v.role_name.clone())
                    .collect::<Vec<String>>()
            })
            .collect::<Vec<_>>();

        for unicity in unicities {
            let item = StoredItem {
                item_type: StoredItemType::Unicity(serde_json::to_string(&unicity)?),
                item_name: name.to_string(),
            };
            write_item(&mut self.inner.lock().unwrap(), item)?;
        }

        Ok(())
    }

    fn get_claim_type(&self, name: &str) -> Result<Option<ClaimType>, std::io::Error> {
        self.inmem.get_claim_type(name)
    }

    fn get_claim_types(&self) -> Result<Vec<ClaimType>, std::io::Error> {
        self.inmem.get_claim_types()
    }

    fn load_state(&self) -> Result<(), std::io::Error> {
        tracing::info!("Loading state from file");

        let mut contents = String::new();
        {
            let mut inner = self.inner.lock().unwrap();
            inner.read_to_string(&mut contents)?;
        }

        for line in contents.lines() {
            let item: StoredItem = serde_json::from_str(line).unwrap();

            tracing::debug!("Loading item: {}", item);

            match item.item_type {
                StoredItemType::LabelType(primitive_type) => {
                    let pt = self.find_primitive_type(&primitive_type).unwrap().unwrap();
                    self.inmem
                        .add_label_type(&LabelType::new(item.item_name, pt))?;
                }
                StoredItemType::ClaimType(ote) => {
                    let claim_type_expression = ote;
                    let parsed_expresssion =
                        core_model::parse_expression(claim_type_expression.as_ref())?;

                    let mut roles = vec![];

                    for part in parsed_expresssion.clone().parts {
                        if let core_model::ExpressionPart::Role(role) = part {
                            let role_name = role.role_name;
                            let role_type = role.role_type;

                            let role =
                                self.get_role(role_name.inner(), role_type.map(|v| v.inner()))?;
                            roles.push(role);
                        }
                    }

                    let claim_type = ClaimType::new(
                        item.item_name.to_string(),
                        claim_type_expression.to_string(),
                        roles,
                    );

                    self.inmem.add_claim_type(&claim_type)?;
                }
                StoredItemType::ObjectTypeExpression(ote) => {
                    let ct = self.get_claim_type(&item.item_name)?.unwrap();
                    ct.set_object_type_expression(ote)?;
                    self.inmem.update_claim_type(&item.item_name, &ct)?;
                }
                StoredItemType::Unicity(unicity) => {
                    let unicity: Vec<String> = serde_json::from_str(&unicity)?;
                    let ct = self.get_claim_type(&item.item_name)?.unwrap();
                    ct.set_unicity(unicity)?;
                    self.inmem.update_claim_type(&item.item_name, &ct)?;
                }
                StoredItemType::Claim(claim) => {
                    let claim_type = self.get_claim_type(&claim.claim_type)?.unwrap();

                    let base_claim = if claim.variant == "Claim" {
                        let new_claim = Claim::new(claim.id, claim_type.clone());

                        for role_data in claim.data {
                            let role = claim_type.find_role(&role_data.role_name).unwrap();

                            match role.role_type() {
                                RoleType::LabelType(_lt) => {
                                    let ptn = role_data.primitive_type.unwrap();
                                    let pt = self.find_primitive_type(&ptn).unwrap().unwrap();
                                    let d = pt.parse(role_data.role_data.as_str()).unwrap();
                                    new_claim.inner().data.lock().unwrap().insert(role, d);
                                }
                                RoleType::ClaimType(_ct) => {
                                    let claim_id = role_data.role_data.parse::<Uuid>().unwrap();
                                    let c = ClaimDataItem::Uuid(claim_id);

                                    new_claim.inner().data.lock().unwrap().insert(role, c);
                                }
                            }
                        }
                        BaseClaim::Claim(new_claim)
                    } else {
                        let sub = self
                            .inmem
                            .find_claim(&claim.subject.unwrap())
                            .unwrap()
                            .unwrap();
                        let annotation = Annotation::new(claim.id, claim_type, sub);
                        BaseClaim::Annotation(annotation)
                    };

                    self.inmem.add_claim(&base_claim)?;
                }
            }
        }

        Ok(())
    }

    fn find_primitive_type(
        &self,
        name: &str,
    ) -> Result<Option<core_model::PrimitiveType>, std::io::Error> {
        self.inmem.find_primitive_type(name)
    }

    fn find_primitive_types(&self) -> Result<Vec<core_model::PrimitiveType>, std::io::Error> {
        self.inmem.find_primitive_types()
    }

    fn add_primitive_type(
        &self,
        primitive: core_model::PrimitiveType,
    ) -> Result<(), std::io::Error> {
        self.inmem.add_primitive_type(primitive)
    }

    fn get_role(
        &self,
        role_name: String,
        role_type: Option<String>,
    ) -> Result<core_model::Role, std::io::Error> {
        self.inmem.get_role(role_name, role_type)
    }

    fn get_roles(&self) -> Result<Vec<Role>, std::io::Error> {
        self.inmem.get_roles()
    }
}

fn write_item(file: &mut File, item: StoredItem) -> Result<(), std::io::Error> {
    let data = serde_json::to_string(&item).unwrap();

    file.write_all(data.as_bytes())?;
    file.write_all(b"\n")?;

    Ok(())
}
