use std::{
    collections::HashMap,
    sync::{Arc, Mutex},
};

use uuid::Uuid;

use core_model::{
    BaseClaim, ClaimDataItem, ClaimType, LabelType, PrimitiveType, Role, RoleType,
    RESERVED_ROLE_ANNOTATION,
};
use storage_common::{DataStore, DataStoreArc};

#[derive(Clone, Debug, Default)]
pub struct InMemStore {
    claims: Arc<Mutex<HashMap<Uuid, BaseClaim>>>,
    label_types: Arc<Mutex<Vec<LabelType>>>,
    claim_types: Arc<Mutex<Vec<ClaimType>>>,
    primitive_types: Arc<Mutex<Vec<PrimitiveType>>>,
    roles: Arc<Mutex<Vec<Role>>>,
}

impl InMemStore {
    pub fn new() -> Self {
        let mut primitives = Vec::new();

        primitives.push(PrimitiveType::new("uuid", |value| {
            value.parse().ok().map(ClaimDataItem::Uuid)
        }));

        primitives.push(PrimitiveType::new("integer", |value| {
            value.parse().ok().map(ClaimDataItem::Integer)
        }));

        primitives.push(PrimitiveType::new("string", |value| {
            value.parse().ok().map(ClaimDataItem::String)
        }));

        primitives.push(PrimitiveType::new("bool", |value| {
            value.parse().ok().map(ClaimDataItem::Boolean)
        }));

        primitives.push(PrimitiveType::new("date", |value| {
            value.parse().ok().map(ClaimDataItem::Date)
        }));

        primitives.push(PrimitiveType::new("datetime", |value| {
            value.parse().ok().map(ClaimDataItem::DateTime)
        }));

        primitives.push(PrimitiveType::new("decimal", |value| {
            value.parse().ok().map(ClaimDataItem::Float)
        }));

        InMemStore {
            claims: Default::default(),
            label_types: Default::default(),
            claim_types: Default::default(),
            primitive_types: Arc::new(Mutex::new(primitives)),
            roles: Default::default(),
        }
    }

    pub fn new_arc() -> DataStoreArc {
        Arc::new(Self::new())
    }
}

impl DataStore for InMemStore {
    fn name(&self) -> &str {
        "InMemStore"
    }

    fn reset(&self) -> Result<(), std::io::Error> {
        let mut claims = self.claims.lock().unwrap();
        let mut label_types = self.label_types.lock().unwrap();
        let mut claim_types = self.claim_types.lock().unwrap();

        claims.clear();
        label_types.clear();
        claim_types.clear();

        Ok(())
    }

    fn add_claim(&self, claim: &BaseClaim) -> Result<(), std::io::Error> {
        let mut inner = self.claims.lock().unwrap();
        inner.insert(claim.id(), claim.clone());

        Ok(())
    }

    fn find_claim(&self, id: &uuid::Uuid) -> Result<Option<BaseClaim>, std::io::Error> {
        let inner = self.claims.lock().unwrap();
        Ok(inner.get(id).cloned())
    }

    fn find_claims(&self, claim_type: &str) -> Result<Vec<BaseClaim>, std::io::Error> {
        let inner = self.claims.lock().unwrap();

        let claims = inner
            .values()
            .filter(|c| c.claim_type().name() == claim_type)
            .cloned()
            .collect::<Vec<BaseClaim>>();

        Ok(claims)
    }
    fn add_label_type(&self, lt: &LabelType) -> Result<(), std::io::Error> {
        let mut lt_store = self.label_types.lock().unwrap();
        lt_store.push(lt.clone());

        Ok(())
    }

    fn get_label_type(&self, name: &str) -> Result<Option<LabelType>, std::io::Error> {
        let res = self
            .label_types
            .lock()
            .unwrap()
            .iter()
            .find(|lt| lt.name() == name)
            .cloned();
        Ok(res)
    }

    fn get_label_types(&self) -> Result<Vec<LabelType>, std::io::Error> {
        let res = self.label_types.lock().unwrap().clone();
        Ok(res)
    }

    fn add_claim_type(&self, ct: &ClaimType) -> Result<(), std::io::Error> {
        let claim_type = ct.name();

        let mut ct_store = self.claim_types.lock().unwrap();
        if ct_store.iter().any(|ct| ct.name() == claim_type) {
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                format!("Claim type already exists: {}", claim_type),
            ));
        }

        ct_store.push(ct.clone());
        Ok(())
    }

    fn update_claim_type(&self, name: &str, ct_update: &ClaimType) -> Result<(), std::io::Error> {
        let mut ct_store = self.claim_types.lock().unwrap();
        let ct = ct_store.iter_mut().find(|ct| ct.name() == name);

        if let Some(ct) = ct {
            *ct = ct_update.clone();
        }

        Ok(())
    }

    fn get_claim_type(&self, name: &str) -> Result<Option<ClaimType>, std::io::Error> {
        let res = self
            .claim_types
            .lock()
            .unwrap()
            .iter()
            .find(|ct| ct.name() == name)
            .cloned();
        Ok(res)
    }

    fn get_claim_types(&self) -> Result<Vec<ClaimType>, std::io::Error> {
        let res = self.claim_types.lock().unwrap().clone();
        Ok(res)
    }

    fn load_state(&self) -> Result<(), std::io::Error> {
        Ok(())
    }

    fn find_primitive_type(
        &self,
        name: &str,
    ) -> Result<Option<core_model::PrimitiveType>, std::io::Error> {
        let primitives = self.primitive_types.lock().unwrap();
        let res = primitives.iter().find(|pt| pt.name() == name).cloned();

        Ok(res)
    }

    fn find_primitive_types(&self) -> Result<Vec<PrimitiveType>, std::io::Error> {
        let res = self.primitive_types.lock().unwrap().clone();
        Ok(res)
    }

    fn add_primitive_type(&self, primitive: PrimitiveType) -> Result<(), std::io::Error> {
        let mut primitives = self.primitive_types.lock().unwrap();
        primitives.push(primitive);
        Ok(())
    }

    fn get_role(
        &self,
        role_name: String,
        role_type: Option<String>,
    ) -> Result<Role, std::io::Error> {
        let role = self
            .roles
            .lock()
            .unwrap()
            .iter()
            .find(|r| r.role_name == role_name)
            .cloned();

        if let Some(role) = role {
            return Ok(role);
        }

        // If the role is not found, make it
        let claim_type_name = role_type.unwrap_or(role_name.clone());

        let label_role = self.get_label_type(&claim_type_name)?;
        let claim_role = self.get_claim_type(&claim_type_name)?;

        let role = if let Some(label_role) = label_role {
            Role::new(role_name.to_string(), RoleType::LabelType(label_role))
        } else if let Some(claim_role) = claim_role {
            Role::new(role_name.to_string(), RoleType::ClaimType(claim_role))
        } else if claim_type_name == RESERVED_ROLE_ANNOTATION {
            Role::new(
                role_name.to_string(),
                RoleType::LabelType(LabelType::new(
                    RESERVED_ROLE_ANNOTATION,
                    self.find_primitive_type("uuid").unwrap().unwrap(),
                )),
            )
        } else {
            tracing::error!(
                "Role could not be built name: `{}` id: `{}`",
                role_name,
                claim_type_name
            );
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                format!(
                    "Role could not be built name: `{}` id: `{}`",
                    role_name, claim_type_name
                ),
            ));
        };

        self.roles.lock().unwrap().push(role.clone());

        Ok(role)
    }

    fn get_roles(&self) -> Result<Vec<Role>, std::io::Error> {
        Ok(self.roles.lock().unwrap().clone())
    }
}
