Feature: claim types

  Background:
    Given a subject s1
    And a claimant c1

  Rule: Claims should be submitted with a valid claimType

    Scenario: Adding a claim with a unkown claimtype
      Given a claimType
        | id  | valid_start | valid_end | schema                                                                    |
        | ct1 | now         | none      | { "type": "object", "properties": { "voornamen": { "type": "string" } } } |
      And a claim
        | id  | subject | claimant | kind      | type | data                   | associated_claim | registration_start | valid_start |
        | cl1 | s1      | c1       | Assertion | ct1  | { "voornamen": "Jan" } | None             | now                | None        |
      When claim cl1 has been sent
      Then claim cl1 response should not be ok

    Scenario: Adding a claim with a known claimtype
      Given a claimType
        | id  | valid_start | valid_end | schema                                                                    |
        | ct2 | now         | none      | { "type": "object", "properties": { "voornamen": { "type": "string" } } } |
      And a claim
        | id  | subject | claimant | kind      | type | data                   | associated_claim | registration_start | valid_start |
        | cl2 | s1      | c1       | Assertion | ct2  | { "voornamen": "Jan" } | None             | now                | None        |
      When claimType ct2 has been sent
      Then claimType ct2 response should be ok
      When claim cl2 has been sent
      Then claim cl2 response should be ok
