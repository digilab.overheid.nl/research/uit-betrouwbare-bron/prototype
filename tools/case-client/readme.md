# README for case-client

This README provides information on the format and conventions used in the `case-client` tool.

The tool proves the functions:
- `reset`:    reset the register
- `setup`:    reset the register and setup it's schema
- `populate`: reset the register, setup it's schema, and populate it with data

## Reset
Remove all data and the schema from the register

## Setup
Set the schema for the register

### Example
```graphql
mutation setup {
  # Meta claim types
  lt01: addLabelType(label_name: "buildingID", primitive_name: "string")
  lt02: addLabelType(label_name: "addressID", primitive_name: "string")

  # Domain claim types
  ct01: addClaimType(claim_type_name: "Building", cte: "Building <buildingID> exists")
  ct02: addClaimType(claim_type_name: "Address", cte: "Address <addressID> exists")
  ct03: addClaimType(claim_type_name: "Building/Address", cte: "<Building> is located at <Address>")

  # Nested expression templates
  m01: addObjectTypeExpression(claim_type_name: "Building", ote: "building <buildingID>")
  m02: addObjectTypeExpression(claim_type_name: "Address", ote: "address <addressID>")
  
  # Unicities
  m03: addUnicity(claim_type_name: "Building/Address", unicity: ["Building"])
```

## Population
Add data to the schema

### Action Format
Actions in the `case-client` tool follow the format:
`<id>: <query>`
- Duplicate ids are not allowed.
- Ids used to reference in other queries will be prefixed with a `$` sign.

### Groups
Groups in the `case-client` tool are denoted by:
`# <name>`
and end with: `#-`

Groups are executed in the order they appear.

### Comments
Comments in the `case-client` tool start with `##` and are used for providing additional information or explanations.

### Example
```
# Buildings
b1: addBuilding(BuildingID: "b1")
b2: addBuilding(BuildingID: "b2")
b3: addBuilding(BuildingID: "b3")
#---

# Addresses
a1: addAddress(AddressID: "a1")
a2: addAddress(AddressID: "a2")
a3: addAddress(AddressID: "a3")
#---

# BuildingAddresses
ba1: addBuildingAddress(Building: "$b1", Address: "$a1")
ba2: addBuildingAddress(Building: "$b2", Address: "$a2")
ba3: addBuildingAddress(Building: "$b3", Address: "$a3")
#---
```