use anyhow::Result;
use tracing::instrument;

#[instrument(skip(base_url, schema_file))]
pub fn setup(base_url: &str, schema_file: &str) -> Result<()> {
    tracing::info!("Setting up");
    let schema_query = std::fs::read_to_string(schema_file)?;

    let setup_response = reqwest::blocking::Client::new()
        .post(base_url)
        .json(&serde_json::json!({
            "query": schema_query,
        }))
        .send()?
        .json::<serde_json::Value>()?;

    tracing::info!(
        message="Setup done",
        response=?setup_response
    );
    Ok(())
}
