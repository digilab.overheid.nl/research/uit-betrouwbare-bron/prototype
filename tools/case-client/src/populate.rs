use anyhow::{anyhow, Result};
use regex::Regex;
use std::{collections::HashMap, fmt::Display};
use tracing::instrument;
use uuid::Uuid;

use crate::MutationBuilder;

// Action format <id>: <query>
//  - Duplicate ids are not allowed
//  - Ids used to reference in other queries will be prefixed with a $ sign
// groups start with # <name> and end with #-
//  - groups are run in order
// comments start with ##

#[derive(Debug)]
struct Comment {
    text: String,
    file: String,
    line: usize,
}

impl Display for Comment {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} ({}:{})", self.text, self.file, self.line)
    }
}

#[derive(Debug)]
struct PopulationItem {
    id: String,
    action: String,
}

#[derive(Debug)]
struct PopulationGroup {
    name: Option<String>,
    actions: Vec<PopulationItem>,
}

impl PopulationGroup {
    fn set_name(&mut self, name: String) {
        if name.is_empty() {
            return;
        }

        let name = name.split(' ').fold(String::new(), |acc, item| {
            let start = item[0..1].to_uppercase();
            let rest = &item[1..];
            format!("{}{}{}", acc, start, rest)
        });

        self.name = Some(name);
    }

    fn name(&self) -> Result<String> {
        self.name.clone().ok_or(anyhow!("Group name not set"))
    }
}

#[derive(Debug)]
struct Populator {
    ids: HashMap<String, Option<Uuid>>,
    groups: Vec<PopulationGroup>,
    comments: Vec<Comment>,
}

impl Populator {
    fn replace(&self, item: &PopulationItem) -> Result<String> {
        let mut action = item.action.clone();
        let re = Regex::new(r#"\$([a-zA-Z0-9]*)"#)?;
        let captures = re.captures_iter(&item.action);
        for capture in captures {
            let key = capture
                .get(1)
                .ok_or(anyhow!("Could not get capture group"))?
                .as_str();
            let value = self.ids.get(key);

            let id = if let Some(Some(id)) = value {
                id.to_string()
            } else {
                return Err(anyhow!("Key {} not found", key));
            };

            action = action.replace(&format!("${key}"), &id);
        }

        Ok(action)
    }
}

#[instrument(skip(base_url, data_file))]
pub fn populate(base_url: &str, data_file: &str) -> Result<()> {
    let queries = std::fs::read_to_string(data_file)?;

    let mut populator = Populator {
        ids: HashMap::new(),
        groups: Vec::new(),
        comments: Vec::new(),
    };

    let mut current_group = PopulationGroup {
        name: None,
        actions: Vec::new(),
    };

    let mut line_number = 0;
    for line in queries.lines() {
        line_number += 1;

        if line.trim().is_empty() {
            continue;
        }

        match line[0..=1].as_ref() {
            "# " => {
                // Start group
                current_group.set_name(line[2..].to_string());
            }
            "#-" => {
                // End group
                populator.groups.push(current_group);
                current_group = PopulationGroup {
                    name: None,
                    actions: Vec::new(),
                };
            }
            "##" => {
                // Comment
                populator.comments.push(Comment {
                    text: line[2..].trim().to_string(),
                    file: data_file.to_string(),
                    line: line_number,
                });
            }
            _ => {
                // Add to group
                let mut parts = line.splitn(2, ": ");
                let id = parts
                    .next()
                    .ok_or(anyhow!("Could not get id from action"))?
                    .to_string();
                let action = parts
                    .next()
                    .ok_or(anyhow!("Could not get data from action"))?;

                let item = PopulationItem {
                    id: id.clone(),
                    action: action.to_string(),
                };

                let entry = populator.ids.entry(id.clone());
                match entry {
                    std::collections::hash_map::Entry::Occupied(_v) => {
                        panic!("Duplicate id {} on {}", id, line_number)
                    }
                    std::collections::hash_map::Entry::Vacant(v) => v.insert(None),
                };

                current_group.actions.push(item);
            }
        }
    }

    for group in &populator.groups {
        tracing::info!(message = "Starting group", group.name);
        let mut mutation = MutationBuilder::new(base_url, &group.name()?);

        for action in &group.actions {
            tracing::debug!(
                message = "Running action",
                id = action.id,
                action = action.action
            );
            let clean_action = populator.replace(action)?;
            mutation = mutation.add(&format!("{}: {}", action.id, clean_action));
        }

        let response = mutation.send()?;
        tracing::info!(
            message = "Group send response",
            name = group.name()?,
            ?response
        );
        for (key, id) in response.data {
            let entry = populator.ids.get(&key);
            if let Some(Some(_)) = entry {
                panic!("Duplicate id {}", key)
            }
            populator.ids.insert(key, Some(id));
        }
    }

    if !populator.comments.is_empty() {
        tracing::warn!("Comments found in population file:");
        for comment in &populator.comments {
            tracing::warn!("{}", comment);
        }
    }

    Ok(())
}
