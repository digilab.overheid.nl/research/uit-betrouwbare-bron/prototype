use anyhow::{anyhow, Result};
use serde::Deserialize;
use std::collections::HashMap;
use uuid::Uuid;

#[derive(Debug, Deserialize)]
pub struct GqlClaimAddResponse {
    pub data: HashMap<String, Uuid>,
}

pub struct MutationBuilder {
    base_url: String,
    name: String,
    items: Vec<String>,
}

impl MutationBuilder {
    pub fn new(base_url: &str, name: &str) -> Self {
        Self {
            base_url: base_url.to_string(),
            name: name.to_string(),
            items: Vec::new(),
        }
    }

    pub fn add(mut self, mutation: &str) -> Self {
        self.items.push(mutation.to_string().replace('\'', "\""));
        self
    }

    pub fn send(&self) -> Result<GqlClaimAddResponse> {
        let mutation = format!("mutation {} {{{}}}", self.name, self.items.join("\n"));
        let query = build_query(&mutation);
        let claims = add_claims(&self.base_url, query)
            .map_err(|e| anyhow!("Error during sending {}: {}", self.name, e))?;

        Ok(claims)
    }
}

pub fn build_query(mutation: &str) -> serde_json::Value {
    serde_json::json!({
        "query": mutation,
    })
}

pub fn add_claims(url: &str, query: serde_json::Value) -> Result<GqlClaimAddResponse> {
    let res = reqwest::blocking::Client::new()
        .post(url)
        .json(&query)
        .send()?;

    let data = res.bytes()?;

    let json = serde_json::from_slice::<GqlClaimAddResponse>(&data);

    match json {
        Ok(json) => Ok(json),
        Err(e) => Err(anyhow!(
            "Could not add claims: `{}`\n\t`{}`",
            e,
            String::from_utf8_lossy(&data)
        )),
    }
}
