use anyhow::{anyhow, Result};
use tracing::instrument;

use crate::build_query;

#[instrument(skip(base_url))]
pub fn restart(base_url: &str) -> Result<()> {
    let restart_query = build_query(
        r#"
    mutation restart{
        restart
    }
    "#,
    );

    let restart_response = reqwest::blocking::Client::new()
        .post(base_url)
        .json(&restart_query)
        .send()?
        .json::<serde_json::Value>()
        .map_err(|e| anyhow!("Could not restart {}", e))?;

    tracing::info!(
        message="restart done",
        response=?restart_response
    );
    Ok(())
}
