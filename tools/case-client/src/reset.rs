use anyhow::{anyhow, Result};
use tracing::instrument;

use crate::build_query;

#[instrument(skip(base_url))]
pub fn reset(base_url: &str) -> Result<()> {
    let reset_query = build_query(
        r#"
    mutation reset{
        reset
    }
    "#,
    );

    let reset_response = reqwest::blocking::Client::new()
        .post(base_url)
        .json(&reset_query)
        .send()?
        .json::<serde_json::Value>()
        .map_err(|e| anyhow!("Could not reset {}", e))?;

    tracing::info!(
        message="Reset done",
        response=?reset_response
    );
    Ok(())
}
