use anyhow::Result;
use clap::Parser;
use serde::Serialize;

mod populate;
mod reset;
mod restart;
mod setup;
mod util;

use populate::*;
use reset::*;
use restart::*;
use setup::*;
use tracing::level_filters::LevelFilter;
use tracing_subscriber::EnvFilter;
use util::*;

#[derive(clap::ValueEnum, Clone, Debug, Serialize)]
pub enum Action {
    Reset,
    Setup,
    Populate,
}

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    #[arg(short, long, env, value_enum)]
    action: Action,

    #[arg(short, long, env, default_value = "http://localhost:8000/gql/v0")]
    base_url: String,

    #[arg(
        short,
        long,
        env,
        default_value = "./_docs/cases/001_claim_grouping_api/schema.gql"
    )]
    schema_file: String,

    #[arg(
        short,
        long,
        env,
        default_value = "./_docs/cases/001_claim_grouping_api/data.gql"
    )]
    data_file: String,
}

fn main() -> Result<()> {
    let args = Args::parse();

    let filter = EnvFilter::builder()
        .with_default_directive(LevelFilter::INFO.into())
        .from_env_lossy();

    tracing_subscriber::fmt().with_env_filter(filter).init();

    tracing::info!(message = "Running with config", ?args);

    match args.action {
        Action::Reset => reset(&args.base_url)?,
        Action::Setup => {
            reset(&args.base_url)?;
            setup(&args.base_url, &args.schema_file)?;
            restart(&args.base_url)?;
        }
        Action::Populate => {
            reset(&args.base_url)?;
            setup(&args.base_url, &args.schema_file)?;
            restart(&args.base_url)?;
            populate(&args.base_url, &args.data_file)?;
        }
    }
    Ok(())
}
