# Contributing

## Setup your development environment

### Requirements

We use [asdf](https://asdf-vm.com/) for installing [required tools](.tool-versions).

Note that the postgres plugin requires some packages to be manually installed. See <https://github.com/smashedtoatoms/asdf-postgres?tab=readme-ov-file#dependencies>.

Also, pre-commit requires the Python sqlite3 extension, which needs `libsqlite3-dev` while compiling Python during `asdf install`.

For these reasons, I needed to do this, but YMMV:
```sh
sudo apt install libicu-dev libreadline-dev uuid-dev
sudo apt install libsqlite3-dev
```
TODO: This kind of defeats the purpose of asdf in my opinion, so we need to figure out if we really need it? Maybe just use a docker exec for the psql client?

```sh
asdf plugin add k3d
asdf plugin add kustomize
asdf plugin add postgres
asdf plugin add pre-commit
asdf plugin add python
asdf plugin add rust
asdf plugin add skaffold
asdf install
```

To keep the dependencies of sub-packages simple, we use [cargo-autoinherit](https://mainmatter.com/blog/2024/03/18/cargo-autoinherit/).
There's no asdf support for these yet. Instead, do this:
```sh
cargo install cargo-autoinherit
```

## Running locally

Running locally uses k3d, so we need a working docker environment, eg. [Docker Desktop](https://docs.docker.com/desktop/).

We use pre-commmit for providing a git hook:
```sh
pre-commit install --config .conf/.pre-commit-config.yaml
```

The first time you run the project, starting k3d can take a while. Wait for all pods to be completed or running:
```sh
make k3d
kubectl get pods -Aw
```

After that, you can start the webserver in dev-mode:
```sh
make dev
```

To add test fixtures:
```sh
make seed
```

If you need to access the dev database:
```sh
kubectl -n ace exec -it pods/db-1 -- psql -d claim_backend
```

If you need to fully clear & reset the database without doing a full k3d rebuild (eg. when testing the bootstrap):
```sh
kubectl -n ace exec pods/db-1 -- psql -d claim_backend -c 'drop schema claim_backend cascade;create schema authorization claim_backend'
```

If you need to access the dev database from outside k3d for some reason:
```sh
printf "%s:%s:%s:%s:%s\n" \
    "127.0.0.1" \
    "5432" \
    "$(kubectl -n ace get secrets -o json db-app | jq --raw-output '.data.dbname' | base64 -d)" \
    "$(kubectl -n ace get secrets -o json db-app | jq --raw-output '.data.username' | base64 -d)" \
    "$(kubectl -n ace get secrets -o json db-app | jq --raw-output '.data.password' | base64 -d)" \
    >> ~/.pgpass
chmod 0600 ~/.pgpass
psql --host=127.0.0.1 --username="$(kubectl -n ace get secrets -o json db-app | jq --raw-output '.data.username' | base64 -d)"
```

To run `cargo watch` locally, using PostgreSQL in k8s:
```sh
RUST_BACKTRACE=1 \
RUST_LOG=debug \
APP_DATASTORE_TYPE=dynamic-pg \
APP_POSTGRES_DSN=postgres://"$(kubectl -n ace get secrets -o json db-app | jq --raw-output '.data.username' | base64 -d)":"$(kubectl -n ace get secrets -o json db-app | jq --raw-output '.data.password' | base64 -d)"@127.0.0.1:5432/"$(kubectl -n ace get secrets -o json db-app | jq --raw-output '.data.dbname' | base64 -d)"?sslmode=disable \
cargo watch -x 'run --bin ace_server_gql'
```

### Testing

To run all automated tests: both unit and integration tests:
```sh
make test
```

To run a case against a running k3d deployment:
```sh
CASE=001_ BASE_URL=http://claim-backend-127.0.0.1.nip.io:8080/gql/v0 make case
```

### Example URLs

To see all claims:
```sh
curl 'http://claim-backend-127.0.0.1.nip.io:8080/v0/claim' | jq
```

To add a claimtype:
```sh
echo '{
    "input": {
        "name": "foo",
        "schema": {}
    }
}' | curl \
    -H 'Content-Type: application/json' \
    --data @- \
    "claim-backend-127.0.0.1.nip.io:8080/v0/claimtype" \
    | jq
```

### Example SQL for dynamicpg data store

```sql
SELECT ct.id AS claimtype_id, ct.id_role0
    , ctn.id AS claimtypename_id, ctn.name_role1
    , cte.id AS claimtypeexpression_id, cte.expression_role1
    FROM claim_backend.claimtype_claims_006pzzfey6x7u ct
    LEFT OUTER JOIN claim_backend.claimtypename_claims_230sg02awdzz9 ctn ON ctn.claimtype_role0 = ct.id
    LEFT OUTER JOIN claim_backend.claimtypeexpression_claims_1pfg7ngkzq2wf cte ON cte.claimtype_role0 = ct.id
;
```

## External documentation

TODO: Add useful domain-specific documentation links
