#!/bin/sh

set -e

cargo watch --why -x "run --bin $1"
