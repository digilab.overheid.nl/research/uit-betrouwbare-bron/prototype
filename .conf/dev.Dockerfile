####################################################################################################
## Builder
####################################################################################################
FROM rust:1.80.0 AS builder

RUN cargo install cargo-watch

WORKDIR /app

COPY .conf/dev.entrypoint.sh ./
COPY Cargo.* ./
COPY application/ ./application/
COPY core/ ./core/
COPY storage/ ./storage/
COPY tools/ ./tools/

ARG APP_VERSION=undefined
ENV APP_VERSION=${APP_VERSION}

ENTRYPOINT ["./dev.entrypoint.sh"]
CMD ["ace_server_gql"]
