use std::any::Any;

use anyhow::Result;
use async_graphql::http::GraphiQLSource;
use async_graphql_axum::GraphQL;
use axum::{
    body::Body, extract::State, http::Method, http::StatusCode, response::Html, response::Response,
    routing::get, Router,
};
use config::AppConfig;
use core_service::schema_manager::SchemaManager;
use tokio::{net::TcpListener, sync::broadcast};
use tower_http::{
    catch_panic::CatchPanicLayer,
    cors::{AllowOrigin, CorsLayer},
};
use tracing::level_filters::LevelFilter;
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt, EnvFilter};

mod config;
mod core;
mod gql;
mod group;

#[derive(Clone)]
struct AppState {
    config: AppConfig,
    schema_manager: SchemaManager,
    signal: broadcast::Sender<SignalAction>,
}

#[tokio::main]
async fn main() -> Result<()> {
    let config = AppConfig::load();

    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .init();

    tracing::info!(message = "Starting ACE app",);

    let schema_manager = core::build(&config)?;

    let (sender, _) = tokio::sync::broadcast::channel::<SignalAction>(1);
    let app_state = AppState {
        config,
        schema_manager,
        signal: sender,
    };

    loop {
        tokio::spawn(run_server(app_state.clone()));
        let mut receiver = app_state.signal.subscribe();

        match receiver.recv().await? {
            SignalAction::Shutdown => {
                tracing::warn!("Shutdown signal received!");
                break;
            }
            SignalAction::Restart => {
                tracing::warn!("Restart signal received!");
            }
        }
    }

    Ok(())
}

#[derive(Debug, Clone)]
enum SignalAction {
    Shutdown,
    Restart,
}

async fn run_server(app_state: AppState) -> Result<()> {
    tracing::info!("(Re)starting the server");
    tracing::info!("GraphiQL IDE: {}", &app_state.config.public_url);

    let gql_schema = gql::build(app_state.clone())
        .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;

    let receiver = app_state.signal.subscribe();
    let listener = TcpListener::bind(&app_state.config.listen_address).await?;

    let app = Router::new()
        .route(
            "/gql/v0",
            get(graphiql).post_service(GraphQL::new(gql_schema)),
        )
        .route("/test/mermaid", get(render_mermaid))
        .route("/test/group", get(group::handle_group))
        .with_state(app_state)
        .layer(CatchPanicLayer::custom(handle_panic))
        .layer(
            CorsLayer::new()
                .allow_origin(AllowOrigin::predicate(|_, _| true))
                .allow_methods([Method::GET, Method::POST]),
        );

    axum::serve(listener, app)
        .with_graceful_shutdown(listen_for_signal(receiver))
        .await?;
    Ok(())
}

async fn listen_for_signal(mut receiver: broadcast::Receiver<SignalAction>) {
    receiver.recv().await.unwrap();
}

async fn graphiql(
    State(state): State<AppState>,
) -> Result<Html<String>, (axum::http::StatusCode, String)> {
    Ok(Html(
        GraphiQLSource::build()
            .endpoint(&state.config.public_url)
            .finish(),
    ))
}

async fn render_mermaid(
    State(state): State<AppState>,
) -> Result<String, (axum::http::StatusCode, String)> {
    let schema = state
        .schema_manager
        .get_schema(&state.config.schema_name)
        .unwrap();
    let render = schema.render_to_mermaid().unwrap();
    Ok(render)
}

fn handle_panic(err: Box<dyn Any + Send + 'static>) -> Response {
    let message = if let Some(s) = err.downcast_ref::<String>() {
        s.clone()
    } else if let Some(s) = err.downcast_ref::<&str>() {
        s.to_string()
    } else {
        "Unknown panic message".to_string()
    };

    let body = serde_json::json!({
        "errors": {
            "message": message,
        }
    });

    tracing::error!(message = "Panic handled", error = message);

    Response::builder()
        .status(StatusCode::INTERNAL_SERVER_ERROR)
        .body(Body::from(body.to_string()))
        .unwrap()
}
