use std::{collections::HashMap, vec};

use core_model::{BaseClaim, ClaimType};
use serde::Serialize;
use uuid::Uuid;

#[derive(Debug, Clone)]
pub struct BaseClaimType(ClaimType);

impl BaseClaimType {
    pub fn new(claim_type: ClaimType) -> Self {
        Self(claim_type)
    }

    pub fn name(&self) -> String {
        self.0.name()
    }

    /// Object types are claimtypes that have an Object type expression
    pub fn is_object_type(&self) -> bool {
        self.0.object_type_expression().is_some()
    }

    /// A claimtype with 1 roles is considered unary
    pub fn is_unary(&self) -> bool {
        self.0.roles().len() == 1
    }
    /// A claimtype with 2 roles is considered binary
    pub fn is_binary(&self) -> bool {
        self.0.roles().len() == 2
    }

    pub fn roles(&self) -> Vec<core_model::Role> {
        self.0.roles().clone()
    }

    pub fn identity(&self) -> Vec<core_model::Role> {
        let id = self.0.inner().lock().unwrap().unicities.first().cloned();

        if let Some(id) = id {
            id.roles.clone()
        } else {
            if self.roles().iter().any(|v| v.is_annotion()) {
                let id_role = self
                    .roles()
                    .iter()
                    .find(|v| !v.is_annotion())
                    .unwrap()
                    .clone();
                return vec![id_role];
            }

            // TODO: How do we construct a default identity?
            let id_role = self
                .0
                .roles()
                .into_iter()
                .find(|v| v.role_type().variant() != "LabelType")
                .unwrap()
                .clone();
            vec![id_role]
        }
    }
}

#[derive(Debug, Serialize)]
pub struct Grouping {
    pub classes: HashMap<String, Class>,
}

impl Grouping {
    pub fn new() -> Self {
        Self {
            classes: HashMap::new(),
        }
    }

    pub fn add_class(&mut self, name: &str, base: BaseClaimType) {
        self.classes
            .insert(name.to_string(), Class::new(name, base));
    }

    pub fn get_class(&mut self, name: &str) -> Option<&mut Class> {
        self.classes.get_mut(name)
    }
}

#[derive(Debug, Clone, Serialize)]
pub struct Class {
    pub name: String,
    #[serde(skip)]
    pub base: BaseClaimType,
    /// The Index String is the role name of the containing class
    pub role_fields: HashMap<String, RoleField>,
    pub objects: Vec<Object>,
}

impl Class {
    pub fn new(name: &str, base: BaseClaimType) -> Self {
        Self {
            name: name.to_string(),
            base,
            role_fields: HashMap::new(),
            objects: Vec::new(),
        }
    }

    pub fn add_role(&mut self, name: &str, value: RoleField) {
        self.role_fields.insert(name.to_string(), value);
    }

    pub fn add_object(&mut self, obj: Object) {
        self.objects.push(obj);
    }

    pub fn name(&self) -> String {
        self.name.clone()
    }
}

#[derive(Debug, Clone, Serialize)]
pub struct Object {
    pub id: Uuid,
    #[serde(skip)]
    pub base: BaseClaim,
    pub name: String,
    pub data: HashMap<String, String>,
    pub meta: HashMap<String, String>,
}

impl Object {
    pub fn new(base: BaseClaim, name: &str) -> Self {
        Self {
            id: base.id(),
            base,
            name: name.to_string(),
            data: HashMap::new(),
            meta: HashMap::new(),
        }
    }

    pub fn add_field(&mut self, name: &str, value: String) {
        self.data.insert(name.to_string(), value);
    }
    pub fn add_meta(&mut self, name: &str, value: String) {
        self.meta.insert(name.to_string(), value);
    }
}

#[derive(Debug, Clone, Serialize)]
pub struct RoleField {
    pub role_name: String,
    pub role_type: RoleFieldType,
}

impl RoleField {
    pub fn role_name(&self) -> String {
        self.role_name.clone()
    }
}

#[derive(Debug, Clone, Serialize)]
pub enum RoleFieldType {
    Primitive(String),
    Reference(String),
    Existential(String),
}

impl RoleFieldType {
    pub fn name(&self) -> &str {
        match self {
            RoleFieldType::Primitive(_) => "Primitive",
            RoleFieldType::Reference(_) => "Reference",
            RoleFieldType::Existential(_) => "Existential",
        }
    }
}
