use std::collections::HashMap;

use anyhow::Ok;
use axum::extract::State;
use core_model::{BaseClaim, ClaimDataItem, RoleType};
use core_service::schema::Schema;

use crate::AppState;

mod base_claim_type;
use base_claim_type::{BaseClaimType, Class, Grouping, Object, RoleField, RoleFieldType};

pub async fn handle_group(
    State(state): State<AppState>,
) -> Result<String, (axum::http::StatusCode, String)> {
    let schema = state
        .schema_manager
        .get_schema(&state.config.schema_name)
        .unwrap();

    do_group(schema)
        .await
        .map_err(|e| (axum::http::StatusCode::INTERNAL_SERVER_ERROR, e.to_string()))
}

/// FCO-IM / NIAM grouping algorithm
/// Based on: https://github.com/Rateotg/ACE/blob/main/source/commands/ace.Commands.Algorithms.pas#L107
async fn do_group(schema: Schema) -> anyhow::Result<String> {
    let mut grouping = build_grouping(&schema).await?;
    group_data(&schema, &mut grouping).await?;

    let res = serde_json::to_string_pretty(&grouping)?;
    Ok(res)
}

async fn group_data(schema: &Schema, grouping: &mut Grouping) -> anyhow::Result<()> {
    // Create objects for the base classes
    for class in grouping.classes.values_mut() {
        let class_name = class.name();
        tracing::info!("grouping base class {}", class_name);

        let claims = schema.storage().find_claims(&class_name)?;
        for claim in claims {
            let object = Object::new(claim, &class_name);
            class.add_object(object);
        }
    }

    // Create objects that reference other objects
    for class in grouping.classes.values_mut() {
        let class_name = class.name();
        tracing::info!("grouping references {}", class_name);

        for obj in class.objects.iter_mut() {
            for (field_name, field) in &class.role_fields {
                if field.role_type.name() != "Reference" {
                    continue;
                }

                let base_roles = obj.base.roles();

                // Find the role that is being referenced
                let role = base_roles.iter().find(|r| r.role_name() == *field_name);
                let role = if let Some(role) = role {
                    role
                } else {
                    continue;
                };

                // Find the data matching the role
                let (_, data) = obj
                    .base
                    .data()
                    .into_iter()
                    .find(|(r, _)| r.role_name == role.role_name())
                    .unwrap();

                tracing::info!(
                    "adding reference: {} -> {} -> {}",
                    class_name,
                    field.role_name(),
                    data
                );
                obj.add_field(&field.role_name(), data.to_string());
                obj.add_meta(&field.role_name(), data.to_string());
            }
        }
    }

    tracing::info!("grouping data");
    // Create objects with data from LabelType claims
    for class in grouping.classes.values_mut() {
        let class_name = class.name();
        tracing::info!("grouping data {}", class_name);

        for (field_name, field) in &class.role_fields {
            if field.role_type.name() != "Primitive" {
                continue;
            }

            let claims = schema.storage().find_claims(field_name)?;

            for claim in claims {
                let id_data = claim
                    .data()
                    .into_iter()
                    .find(|(r, _)| r.role_name == class_name);

                let id_data = if let Some((_, id_data)) = id_data {
                    id_data
                } else {
                    tracing::warn!(
                        "No id data found for {} {} in {:#?}",
                        class_name,
                        field_name,
                        claim.data()
                    );
                    continue;
                };

                let id = if let ClaimDataItem::Uuid(id) = id_data {
                    id
                } else {
                    panic!("expected uuid");
                };

                let obj = class.objects.iter_mut().find(|obj| obj.id == id).unwrap();

                for (role, data_item) in claim.data() {
                    tracing::info!(
                        "adding data {} -> {} -> {}",
                        class_name,
                        role.role_name(),
                        data_item
                    );
                    obj.add_field(&role.role_name, data_item.to_string());
                    obj.add_meta(&field.role_name(), claim.id().to_string());
                }
            }
        }
    }

    for class in grouping.classes.values_mut() {
        let class_name = class.name();
        tracing::info!("grouping existentials {}", class_name);

        for (field_name, field) in &class.role_fields {
            if field.role_type.name() != "Existential" {
                continue;
            }

            let claims = schema.storage().find_claims(field_name)?;

            for claim in claims {
                let id = claim.id();

                let data_item = match &claim {
                    BaseClaim::Annotation(claim) => claim.subject().id().to_string(),
                    BaseClaim::Claim(claim) => {
                        claim.data().iter().next().unwrap().1.clone().to_string()
                    }
                };

                let obj = class.objects.iter_mut().find(|obj| obj.id == id).unwrap();

                tracing::info!(
                    "adding existential {} -> {} -> {}",
                    class_name,
                    field.role_name(),
                    data_item
                );

                obj.add_field(&field.role_name(), data_item);
                obj.add_meta(&field.role_name(), claim.id().to_string());
            }
        }
    }
    Ok(())
}

/// FCO-IM / NIAM grouping algorithm
/// Based on: https://github.com/Rateotg/ACE/blob/main/source/commands/ace.Commands.Algorithms.pas#L107
async fn build_grouping(schema: &Schema) -> anyhow::Result<Grouping> {
    let mut grouping = Grouping::new();

    // Step: Create a list with all ClaimTypes
    let claim_types = schema.storage().get_claim_types()?;

    // TODO: handle label types?

    let base_claim_types = claim_types
        .into_iter()
        .map(BaseClaimType::new)
        .collect::<Vec<_>>();

    // Step: Create Classes for all ClaimTypes that are ObjectTypes
    let base_claim_types = base_claim_types
        .into_iter()
        .filter(|v| {
            if v.is_object_type() {
                grouping.add_class(&v.name(), v.clone());
                let class = grouping.get_class(&v.name()).expect("class not found");

                if v.is_unary() {
                    let roles = v.roles();
                    let role = roles.first().unwrap();
                    let role_data = match role.role_type() {
                        RoleType::LabelType(l) => {
                            RoleFieldType::Existential(l.primitive_type().name().to_string())
                        }
                        RoleType::ClaimType(_c) => RoleFieldType::Reference(role.role_name()),
                    };

                    let role = RoleField {
                        role_name: role.role_name(),
                        role_type: role_data,
                    };

                    class.add_role(&v.name(), role);
                }

                false
            } else {
                true
            }
        })
        .collect::<Vec<BaseClaimType>>();

    // Step: Group remaining ClaimTypes (when possible)
    // Condition 1: ClaimType must be binary (2 roles)
    // Condition 2: Identity must span a single role
    // Condition 3: Identity role must be referencing a Class
    let base_claim_types = base_claim_types
        .into_iter()
        .filter(|v| {
            // Condition 1
            if !v.is_binary() {
                return true;
            }

            // Condition 2
            let ident = v.identity();
            if ident.len() != 1 {
                return true;
            }

            // Condition 3
            let ident_name = &ident.first().unwrap().role_name;
            let class = grouping.get_class(ident_name);
            if class.is_none() {
                return true;
            }

            // Conditions met
            let non_ident_role = v
                .roles()
                .into_iter()
                .find(|v| v.role_name != *ident_name)
                .unwrap();

            let class = class.unwrap();

            let role_data = match non_ident_role.role_type() {
                RoleType::LabelType(l) => {
                    RoleFieldType::Primitive(l.primitive_type().name().to_string())
                }
                RoleType::ClaimType(_c) => RoleFieldType::Reference(non_ident_role.role_name()),
            };

            let role = RoleField {
                role_name: non_ident_role.role_name.clone(),
                role_type: role_data,
            };
            class.add_role(&v.name(), role);

            false
        })
        .collect::<Vec<BaseClaimType>>();

    // Step: Create Classes for all remaining ClaimTypes
    base_claim_types.into_iter().for_each(|v| {
        grouping.add_class(&v.name(), v.clone());
    });

    // Step: Change references to ClaimTypes into references to their corresponding Classes.
    // A. Make sure Roles either reference a LabelType or a Class
    //      find classes where roles exist without a corresponding field?
    // B. Make sure SuperTypes reference their corresponding Class
    //      TODO: do we need to do this
    let classes = grouping
        .classes
        .into_iter()
        .map(|(class_name, mut class)| {
            // Filter out roles that are LabelTypes (Per condition A)
            let f_roles = class
                .base
                .roles()
                .into_iter()
                .filter(|v| {
                    if v.role_type().variant() != "LabelType" {
                        return true;
                    }
                    false
                })
                .collect::<Vec<_>>();

            // Check if there are roles that do not have a fields
            if class.role_fields.len() == f_roles.len() {
                return (class_name, class);
            }

            // find roles missing
            let fields = class.role_fields.keys().collect::<Vec<_>>();
            let diff = f_roles
                .into_iter()
                .filter(|v| !fields.contains(&&v.role_name))
                .collect::<Vec<_>>();

            // Create fields for missing roles
            diff.into_iter().for_each(|role| {
                let leaf_data = match role.role_type() {
                    RoleType::LabelType(l) => {
                        RoleFieldType::Primitive(l.primitive_type().name().to_string())
                    }
                    RoleType::ClaimType(_c) => RoleFieldType::Reference(role.role_name()),
                };

                let leaf = RoleField {
                    role_name: role.role_name(),
                    role_type: leaf_data,
                };

                class.add_role(&role.role_name(), leaf);
            });

            (class_name, class)
        })
        .collect::<HashMap<String, Class>>();

    grouping.classes = classes;

    Ok(grouping)
}
