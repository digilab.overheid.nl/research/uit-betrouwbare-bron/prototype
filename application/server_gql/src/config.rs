use std::path::PathBuf;

use clap::{Parser, ValueEnum};

#[derive(ValueEnum, Debug, Clone)]
pub enum StoreType {
    DynamicPG,
    File,
    InMem,
}

#[derive(Parser, Debug, Clone)]
#[command(version, about, long_about = None)]
pub struct AppConfig {
    #[arg(
        long,
        env = "APP_LISTEN_ADDRESS",
        default_value = "127.0.0.1:8000",
        long_help = "The address to bind the server to"
    )]
    pub listen_address: String,

    #[arg(
        long,
        env = "APP_PUBLIC_URL",
        default_value = "http://localhost:8000/gql/v0",
        long_help = "The public URL of the graphql server"
    )]
    pub public_url: String,

    #[arg(
        long,
        env = "APP_SCHEMA_NAME",
        default_value = "ace",
        long_help = "The default schema name to use"
    )]
    pub schema_name: String,

    #[arg(
        short,
        long,
        env = "APP_DATASTORE_TYPE",
        default_value = "in-mem",
        long_help = "The storage backend to use"
    )]
    pub datastore_type: StoreType,

    #[arg(
        long,
        env = "APP_POSTGRES_DSN",
        long_help = "Used for the `dynamic-pg` datastore type",
        default_value = "postgresql://postgres:postgres@localhost:5432/ace"
    )]
    pub postgres_dsn: String,

    #[arg(
        long,
        env = "APP_FILE_STORE_DATA_PATH",
        long_help = "Used for the `file` datastore type",
        default_value = "./file-store.json"
    )]
    pub file_store_data_path: PathBuf,
}

impl AppConfig {
    pub fn load() -> Self {
        Self::parse()
    }
}
