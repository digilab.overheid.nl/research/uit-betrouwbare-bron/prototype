use anyhow::Result;
use core_service::{schema::Schema, schema_manager::SchemaManager};
use storage_dynamicpg::DynamicPGStore;
use storage_file::FileStore;
use storage_inmem::InMemStore;

use crate::config::{AppConfig, StoreType};

pub fn build(config: &AppConfig) -> Result<SchemaManager> {
    let schema_manager = SchemaManager::default();

    println!("Config used: {:?}", config);

    let store = match config.datastore_type {
        StoreType::DynamicPG => DynamicPGStore::new_arc(&config.postgres_dsn),
        StoreType::File => FileStore::new_arc(&config.file_store_data_path),
        StoreType::InMem => InMemStore::new_arc(),
    };

    let schema = Schema::new(store, &config.schema_name)?;
    schema_manager.register_schema(schema.clone());

    Ok(schema_manager)
}
