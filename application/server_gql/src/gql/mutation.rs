use std::collections::HashMap;

use crate::{gql::RoleArg, AppState, SignalAction};

use super::name_clean;
use async_graphql::{dynamic::*, Error, Value};
use core_service::{api::api_add, schema::Schema as DomainSchema};

pub fn build(state: AppState, schema: DomainSchema, builder: SchemaBuilder) -> SchemaBuilder {
    let s1 = state.signal.clone();
    let s2 = state.signal.clone();

    let mut mutation = Object::new(super::MUTATION_NAME)
        .field(
            Field::new("reset", TypeRef::named_nn(TypeRef::BOOLEAN), move |ctx| {
                FieldFuture::new(async move {
                    let schema = ctx.data_unchecked::<DomainSchema>();

                    tracing::warn!("Resetting storage layer");
                    schema.storage().reset()?;

                    Ok(Some(Value::from(true)))
                })
            })
            .description("reset the storage layer"),
        )
        .field(
            Field::new(
                "restart",
                TypeRef::named_nn(TypeRef::BOOLEAN),
                move |_ctx| {
                    let chan = s1.clone();
                    FieldFuture::new(async move {
                        chan.send(SignalAction::Restart).unwrap();
                        Ok(Some(Value::from(true)))
                    })
                },
            )
            .description("restart the server"),
        )
        .field(
            Field::new(
                "shutdown",
                TypeRef::named_nn(TypeRef::BOOLEAN),
                move |_ctx| {
                    let chan = s2.clone();
                    FieldFuture::new(async move {
                        chan.send(SignalAction::Shutdown).unwrap();
                        Ok(Some(Value::from(true)))
                    })
                },
            )
            .description("stop the server"),
        )
        .field(
            Field::new(
                "addLabelType",
                TypeRef::named_nn(TypeRef::BOOLEAN),
                move |ctx| {
                    FieldFuture::new(async move {
                        let schema = ctx.data_unchecked::<DomainSchema>();

                        let label_type = ctx.args.try_get("label_name")?.string()?;
                        let primitive_type = ctx.args.try_get("primitive_name")?.string()?;

                        tracing::info!(message = "Adding label type", ?label_type, ?primitive_type);

                        let primitive_type =
                            schema.get_primitive_type(primitive_type).map_err(|e| {
                                async_graphql::Error::new(format!("Invalid primitive type {}", e))
                            })?;

                        schema.add_label_type(label_type, &primitive_type)?;

                        Ok(Some(Value::from(true)))
                    })
                },
            )
            .description("Add a label type to a schema")
            .argument(
                InputValue::new("label_name", TypeRef::named_nn(TypeRef::STRING))
                    .description("The name for the label type"),
            )
            .argument(
                InputValue::new("primitive_name", TypeRef::named_nn(TypeRef::STRING))
                    .description("The primitive type for the label type"),
            ),
        )
        .field(
            Field::new(
                "addClaimType",
                TypeRef::named_nn(TypeRef::BOOLEAN),
                move |ctx| {
                    FieldFuture::new(async move {
                        let schema = ctx.data_unchecked::<DomainSchema>();

                        let claim_type_name = ctx.args.try_get("claim_type_name")?.string()?;
                        let cte = ctx.args.try_get("cte")?.string()?;

                        schema.add_claim_type(claim_type_name, cte)?;

                        Ok(Some(Value::from(true)))
                    })
                },
            )
            .description("Add a label type to a schema")
            .argument(
                InputValue::new("claim_type_name", TypeRef::named_nn(TypeRef::STRING))
                    .description("The name for the claim type"),
            )
            .argument(
                InputValue::new("cte", TypeRef::named_nn(TypeRef::STRING))
                    .description("Claim type expression for the claim type"),
            ),
        )
        .field(
            Field::new(
                "addObjectTypeExpression",
                TypeRef::named_nn(TypeRef::BOOLEAN),
                move |ctx| {
                    FieldFuture::new(async move {
                        let schema = ctx.data_unchecked::<DomainSchema>();

                        let claim_type_name = ctx.args.try_get("claim_type_name")?.string()?;
                        let object_type_expression = ctx.args.try_get("ote")?.string()?;

                        schema.add_claim_type_ote(claim_type_name, object_type_expression)?;

                        Ok(Some(Value::from(true)))
                    })
                },
            )
            .description("Add a label type to a schema")
            .argument(
                InputValue::new("claim_type_name", TypeRef::named_nn(TypeRef::STRING))
                    .description("The name for the claim type"),
            )
            .argument(
                InputValue::new("ote", TypeRef::named_nn(TypeRef::STRING))
                    .description("Object type expression for the claim type"),
            ),
        )
        .field(
            Field::new(
                "addUnicity",
                TypeRef::named_nn(TypeRef::BOOLEAN),
                move |ctx| {
                    FieldFuture::new(async move {
                        let schema = ctx.data_unchecked::<DomainSchema>();

                        let claim_type_name = ctx.args.try_get("claim_type_name")?.string()?;
                        let unicity_list = ctx.args.try_get("unicity")?.list()?;
                        let unicity = unicity_list
                            .iter()
                            .map(|x| x.string().map(String::from))
                            .collect::<Result<Vec<String>, Error>>()?;

                        schema.add_claim_type_unicity(claim_type_name, unicity)?;

                        Ok(Some(Value::from(true)))
                    })
                },
            )
            .description("Add a label type to a schema")
            .argument(
                InputValue::new("claim_type_name", TypeRef::named_nn(TypeRef::STRING))
                    .description("The name for the claim type"),
            )
            .argument(
                InputValue::new("unicity", TypeRef::named_nn_list_nn(TypeRef::STRING))
                    .description("Unicity for the claim type"),
            ),
        );

    let claimtypes = schema.claim_types();
    for claimtype in claimtypes {
        let roles = claimtype.roles();
        let args: Vec<RoleArg> = RoleArg::new_set(&roles);

        let claimtype_name = claimtype.name();
        let claimtype_name_clean = name_clean(&claimtype.name());
        tracing::debug!(
            "Adding mutation add{} for claim type `{}`",
            claimtype_name_clean,
            claimtype_name
        );

        let closure_args = args.clone();
        let mut field = Field::new(
            format!("add{}", claimtype_name_clean),
            TypeRef::named_nn(TypeRef::ID),
            move |ctx| {
                let future_args = closure_args.clone();
                let future_name = claimtype_name.clone();
                FieldFuture::new(async move {
                    let schema = ctx.data_unchecked::<DomainSchema>();

                    let mut api_input: HashMap<String, String> = HashMap::new();

                    for arg in future_args {
                        let name = arg.role_name.to_string();
                        let value: String = arg.parse_value(&ctx.args)?;
                        api_input.insert(name, value.to_string());
                    }

                    let x = api_add(schema, &future_name, api_input)?;

                    Ok(Some(Value::from(x.to_string())))
                })
            },
        )
        .description(format!("Add a {} claim to a schema", claimtype.name()));

        for arg in args {
            field = field.argument(
                InputValue::new(arg.get_clean_name(), arg.get_type())
                    .description(format!("The {} for the claim", arg.role_name)),
            )
        }

        mutation = mutation.field(field)
    }

    builder.register(mutation)
}
