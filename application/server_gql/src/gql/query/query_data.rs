use async_graphql::{dynamic::*, Value};
use core_model::{BaseClaim, ClaimDataItem, ClaimLike, RESERVED_ROLE_ANNOTATION};

use crate::gql::{name_clean, RoleArg};
use core_service::schema::Schema as DomainSchema;

pub fn build(
    schema: DomainSchema,
    mut builder: SchemaBuilder,
    mut root_query: Object,
) -> (Object, SchemaBuilder) {
    let claimtypes = schema.claim_types();
    for claimtype in claimtypes {
        let roles = claimtype.roles();
        let args: Vec<RoleArg> = RoleArg::new_set(&roles);

        let claimtype_name = claimtype.name();
        let name = name_clean(&claimtype.name());

        // Build return type for the field from the args/roles
        let mut claim_data = Object::new(format!("{}Data", name));
        for arg in args {
            let field_name = arg.get_clean_name();

            // TODO: This is a hack to handle annotations
            let is_reference = arg.is_annotation();
            let is_manual_annotation = arg.role_name == RESERVED_ROLE_ANNOTATION;

            let field_type = if is_reference {
                TypeRef::named_nn(format!("{}ClaimWrapper", field_name))
            } else {
                arg.get_type()
            };

            let field = Field::new(field_name, field_type, move |ctx| {
                let arg_name = arg.role_name.clone();
                FieldFuture::new(async move {
                    let base_claim = ctx
                        .parent_value
                        .try_downcast_ref::<core_model::BaseClaim>()?;

                    let role = base_claim
                        .roles()
                        .into_iter()
                        .find(|v| v.role_name() == arg_name)
                        .unwrap();

                    tracing::debug!("Getting role {:?} for claim {:?}", role, is_reference);
                    if is_reference {
                        match base_claim {
                            BaseClaim::Annotation(a) => {
                                let sub = a.subject();
                                Ok(Some(FieldValue::owned_any(sub.id())))
                            }
                            BaseClaim::Claim(_c) => {
                                let role_data = base_claim.get_data(&role).unwrap();
                                let id = if let ClaimDataItem::Uuid(id) = role_data {
                                    id
                                } else {
                                    panic!("Not a uuid")
                                };
                                Ok(Some(FieldValue::owned_any(id)))
                            }
                        }
                    } else if is_manual_annotation {
                        if let BaseClaim::Annotation(a) = base_claim {
                            let sub = a.subject();
                            Ok(Some(FieldValue::value(sub.id().to_string())))
                        } else {
                            panic!("Not an annotation")
                        }
                    } else {
                        let value = base_claim.get_data(&role).unwrap();
                        Ok(Some(FieldValue::value(format!("{:?}", value))))
                    }
                })
            });
            claim_data = claim_data.field(field)
        }

        let claim_info = Object::new(format!("{}Claim", name))
            .field(Field::new("id", TypeRef::named(TypeRef::ID), |ctx| {
                FieldFuture::new(async move {
                    let base_claim = ctx
                        .parent_value
                        .try_downcast_ref::<core_model::BaseClaim>()?;

                    Ok(Some(Value::from(base_claim.id().to_string())))
                })
            }))
            .field(Field::new(
                "variant",
                TypeRef::named(TypeRef::STRING),
                |ctx| {
                    FieldFuture::new(async move {
                        let base_claim = ctx
                            .parent_value
                            .try_downcast_ref::<core_model::BaseClaim>()?;

                        Ok(Some(Value::from(base_claim.variant().to_string())))
                    })
                },
            ))
            .field(Field::new(
                "data",
                TypeRef::named_nn(claim_data.type_name()),
                move |ctx| {
                    FieldFuture::new(async move {
                        let base_claim = ctx
                            .parent_value
                            .try_downcast_ref::<core_model::BaseClaim>()
                            .unwrap()
                            .clone();

                        Ok(Some(FieldValue::owned_any(base_claim)))
                    })
                },
            ));

        let claim_wrapper = Object::new(format!("{}ClaimWrapper", name)).field(Field::new(
            "claim",
            TypeRef::named_nn(claim_info.type_name()),
            |ctx| {
                let schema = ctx.data_unchecked::<DomainSchema>();
                let id = ctx.parent_value.try_downcast_ref::<uuid::Uuid>().unwrap();

                tracing::debug!("Getting claim {:?}", id);
                let claim = schema.storage().find_claim(id).unwrap().unwrap().clone();

                FieldFuture::new(async move { Ok(Some(FieldValue::owned_any(claim))) })
            },
        ));

        tracing::debug!(
            "Adding query getClaims{} for claim type `{}`",
            name,
            claimtype_name
        );

        let field = Field::new(
            format!("getClaims{}", name),
            TypeRef::named_nn_list_nn(claim_wrapper.type_name()),
            move |ctx| {
                let schema = ctx.data_unchecked::<DomainSchema>();

                let claims = schema.storage().find_claims(&claimtype_name).unwrap();

                let claims = claims
                    .iter()
                    .map(|v| v.id())
                    .map(FieldValue::owned_any)
                    .collect::<Vec<_>>();
                FieldFuture::new(async move { Ok(Some(FieldValue::list(claims))) })
            },
        )
        .description(format!("Add a {} claim to a schema", claimtype.name()));

        builder = builder
            .register(claim_info)
            .register(claim_wrapper)
            .register(claim_data);
        root_query = root_query.field(field);
    }

    (root_query, builder)
}
