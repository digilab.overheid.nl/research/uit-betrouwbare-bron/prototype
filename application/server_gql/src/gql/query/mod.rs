use async_graphql::dynamic::*;

mod query_data;
mod query_schema;

pub fn build(schema: core_service::schema::Schema, mut builder: SchemaBuilder) -> SchemaBuilder {
    let mut root_query = Object::new(super::QUERY_NAME);

    (root_query, builder) = query_schema::build(schema.clone(), builder, root_query);
    (root_query, builder) = query_data::build(schema.clone(), builder, root_query);

    builder.register(root_query)
}
