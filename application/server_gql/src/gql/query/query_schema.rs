use async_graphql::{dynamic::*, Value};

const CLAIM_TYPE_INFO_NAME: &str = "ClaimType";
use core_service::schema::Schema as DomainSchema;
use storage_common::DataStoreArc;

pub fn build(
    _schema: DomainSchema,
    builder: SchemaBuilder,
    mut root_query: Object,
) -> (Object, SchemaBuilder) {
    let primitive_info = Object::new("PrimitiveType")
        .description("A primitive type for the data contained in a claim")
        .field(Field::new(
            "name",
            TypeRef::named_nn(TypeRef::STRING),
            |ctx| {
                FieldFuture::new(async {
                    let primitive_type = ctx
                        .parent_value
                        .try_downcast_ref::<core_model::PrimitiveType>()?;
                    Ok(Some(Value::from(&primitive_type.name().to_string())))
                })
            },
        ));

    let storage_info = Object::new("Storage")
        .description("A storage engine for storing data in a schema")
        .field(Field::new(
            "name",
            TypeRef::named_nn(TypeRef::STRING),
            |ctx| {
                FieldFuture::new(async {
                    let storage = ctx.parent_value.try_downcast_ref::<DataStoreArc>()?;
                    Ok(Some(Value::from(&storage.name().to_string())))
                })
            },
        ));

    let label_info = Object::new("LabelType")
        .description("A label type describing a claim in a schema")
        .field(Field::new(
            "id",
            TypeRef::named_nn(TypeRef::STRING),
            |ctx| {
                FieldFuture::new(async {
                    let label_type = ctx
                        .parent_value
                        .try_downcast_ref::<core_model::LabelType>()?;
                    Ok(Some(Value::from(&label_type.id().to_string())))
                })
            },
        ))
        .field(Field::new(
            "name",
            TypeRef::named_nn(TypeRef::STRING),
            |ctx| {
                FieldFuture::new(async {
                    let label_type = ctx
                        .parent_value
                        .try_downcast_ref::<core_model::LabelType>()?;
                    Ok(Some(Value::from(&label_type.name().to_string())))
                })
            },
        ))
        .field(Field::new(
            "primitiveType",
            TypeRef::named_nn(primitive_info.type_name()),
            |ctx| {
                FieldFuture::new(async {
                    let label = ctx
                        .parent_value
                        .try_downcast_ref::<core_model::LabelType>()?;
                    Ok(Some(FieldValue::owned_any(label.primitive_type().clone())))
                })
            },
        ));

    let role_type_e = Enum::new("RoleTypeEnum")
        .item(EnumItem::new("LabelType"))
        .item(EnumItem::new("ClaimType"));

    let role_type = Object::new("RoleType")
        .description("The type of role in a schema")
        .field(Field::new(
            "typeEnum",
            TypeRef::named_nn(role_type_e.type_name()),
            |ctx| {
                FieldFuture::new(async {
                    let role_type = ctx
                        .parent_value
                        .try_downcast_ref::<core_model::RoleType>()?;
                    let v = match role_type {
                        core_model::RoleType::LabelType(_) => FieldValue::value("LabelType"),
                        core_model::RoleType::ClaimType(_) => FieldValue::value("ClaimType"),
                    };

                    Ok(Some(v))
                })
            },
        ))
        .field(Field::new(
            "labelType",
            TypeRef::named(label_info.type_name()),
            |ctx| {
                FieldFuture::new(async {
                    let role_type = ctx
                        .parent_value
                        .try_downcast_ref::<core_model::RoleType>()?;
                    let v = match role_type {
                        core_model::RoleType::LabelType(v) => {
                            Some(FieldValue::owned_any(v.clone()))
                        }
                        _ => None,
                    };

                    Ok(v)
                })
            },
        ))
        .field(Field::new(
            "claimType",
            TypeRef::named(CLAIM_TYPE_INFO_NAME),
            |ctx| {
                FieldFuture::new(async {
                    let role_type = ctx
                        .parent_value
                        .try_downcast_ref::<core_model::RoleType>()?;
                    let v = match role_type {
                        core_model::RoleType::ClaimType(v) => {
                            Some(FieldValue::owned_any(v.clone()))
                        }
                        _ => None,
                    };

                    Ok(v)
                })
            },
        ));

    let role_info = Object::new("Role")
        .description("A role in a schema")
        .field(Field::new("id", TypeRef::named_nn(TypeRef::ID), |ctx| {
            FieldFuture::new(async {
                let role = ctx.parent_value.try_downcast_ref::<core_model::Role>()?;
                Ok(Some(Value::from(&role.id().to_string())))
            })
        }))
        .field(Field::new(
            "name",
            TypeRef::named_nn(TypeRef::STRING),
            |ctx| {
                FieldFuture::new(async {
                    let role = ctx.parent_value.try_downcast_ref::<core_model::Role>()?;
                    Ok(Some(Value::from(&role.role_name())))
                })
            },
        ))
        .field(Field::new(
            "type",
            TypeRef::named_nn(role_type.type_name()),
            |ctx| {
                FieldFuture::new(async {
                    let role = ctx.parent_value.try_downcast_ref::<core_model::Role>()?;
                    Ok(Some(FieldValue::owned_any(role.role_type())))
                })
            },
        ));

    let unicity_info = Object::new("Unicity")
        .description("A unicity constraint for a claim in a schema")
        .field(Field::new(
            "roles",
            TypeRef::named_nn_list_nn(role_info.type_name()),
            |ctx| {
                FieldFuture::new(async {
                    let unicity = ctx.parent_value.try_downcast_ref::<core_model::Unicity>()?;
                    let roles = unicity
                        .roles()
                        .iter()
                        .map(|v| FieldValue::owned_any(v.clone()))
                        .collect::<Vec<_>>();
                    Ok(Some(FieldValue::list(roles)))
                })
            },
        ));

    let claim_type_info = Object::new(CLAIM_TYPE_INFO_NAME)
        .description("A type of claim in a schema")
        .field(Field::new(
            "id",
            TypeRef::named_nn(TypeRef::STRING),
            |ctx| {
                FieldFuture::new(async {
                    let claim_type = ctx
                        .parent_value
                        .try_downcast_ref::<core_model::ClaimType>()?;
                    Ok(Some(Value::from(claim_type.id().to_string())))
                })
            },
        ))
        .field(Field::new(
            "name",
            TypeRef::named_nn(TypeRef::STRING),
            |ctx| {
                FieldFuture::new(async {
                    let claim_type = ctx
                        .parent_value
                        .try_downcast_ref::<core_model::ClaimType>()?;
                    Ok(Some(Value::from(&claim_type.name().to_string())))
                })
            },
        ))
        .field(Field::new(
            "roles",
            TypeRef::named_nn_list_nn(role_info.type_name()),
            |ctx| {
                FieldFuture::new(async {
                    let claim_type = ctx
                        .parent_value
                        .try_downcast_ref::<core_model::ClaimType>()?;
                    let roles = claim_type
                        .roles()
                        .iter()
                        .map(|v| FieldValue::owned_any(v.clone()))
                        .collect::<Vec<_>>();
                    Ok(Some(FieldValue::list(roles)))
                })
            },
        ))
        .field(Field::new(
            "cte",
            TypeRef::named_nn(TypeRef::STRING),
            |ctx| {
                FieldFuture::new(async {
                    let claim_type = ctx
                        .parent_value
                        .try_downcast_ref::<core_model::ClaimType>()?;
                    let cte = claim_type.claim_type_expression();
                    Ok(Some(Value::from(cte)))
                })
            },
        ))
        .field(Field::new("ote", TypeRef::named(TypeRef::STRING), |ctx| {
            FieldFuture::new(async {
                let claim_type = ctx
                    .parent_value
                    .try_downcast_ref::<core_model::ClaimType>()?;
                let ote = claim_type.object_type_expression().map(Value::from);
                Ok(ote)
            })
        }))
        .field(Field::new(
            "unicities",
            TypeRef::named_nn_list_nn(unicity_info.type_name()),
            |ctx| {
                FieldFuture::new(async {
                    let claim_type = ctx
                        .parent_value
                        .try_downcast_ref::<core_model::ClaimType>()?;
                    let unicity = claim_type
                        .unicity()
                        .iter()
                        .map(|v| FieldValue::owned_any(v.clone()))
                        .collect::<Vec<_>>();
                    Ok(Some(unicity))
                })
            },
        ));

    let schema_info = Object::new("Schema")
        .description("A schema for storing data")
        .field(Field::new(
            "name",
            TypeRef::named_nn(TypeRef::STRING),
            |ctx| {
                FieldFuture::new(async {
                    let schema = ctx.parent_value.try_downcast_ref::<DomainSchema>()?;
                    Ok(Some(Value::from(&schema.name().to_string())))
                })
            },
        ))
        .field(Field::new(
            "storage",
            TypeRef::named_nn(storage_info.type_name()),
            |ctx| {
                FieldFuture::new(async {
                    let engine = ctx.parent_value.try_downcast_ref::<DomainSchema>()?;
                    Ok(Some(FieldValue::owned_any(engine.storage().clone())))
                })
            },
        ))
        .field(Field::new(
            "primitiveTypes",
            TypeRef::named_nn_list_nn(primitive_info.type_name()),
            |ctx| {
                FieldFuture::new(async {
                    let engine = ctx.parent_value.try_downcast_ref::<DomainSchema>()?;
                    let primitive_types = engine
                        .primitive_types()?
                        .iter()
                        .cloned()
                        .map(|v| FieldValue::owned_any(v.clone()))
                        .collect::<Vec<_>>();
                    Ok(Some(FieldValue::list(primitive_types)))
                })
            },
        ))
        .field(Field::new(
            "labelTypes",
            TypeRef::named_nn_list_nn(label_info.type_name()),
            |ctx| {
                FieldFuture::new(async {
                    let schema = ctx.parent_value.try_downcast_ref::<DomainSchema>()?;
                    let label_types = schema
                        .label_types()
                        .iter()
                        .cloned()
                        .map(|v| FieldValue::owned_any(v.clone()))
                        .collect::<Vec<_>>();

                    Ok(Some(FieldValue::list(label_types)))
                })
            },
        ))
        .field(Field::new(
            "claimTypes",
            TypeRef::named_nn_list_nn(claim_type_info.type_name()),
            |ctx| {
                FieldFuture::new(async {
                    let schema = ctx.parent_value.try_downcast_ref::<DomainSchema>()?;
                    let label_types = schema
                        .claim_types()
                        .iter()
                        .cloned()
                        .map(|v| FieldValue::owned_any(v.clone()))
                        .collect::<Vec<_>>();

                    Ok(Some(FieldValue::list(label_types)))
                })
            },
        ));

    root_query = root_query
        .description("The root query object for the ACE GraphQL API")
        .field(Field::new(
            "activeSchema",
            TypeRef::named_nn(schema_info.type_name()),
            |ctx| {
                FieldFuture::new(async move {
                    let schema = ctx.data_unchecked::<DomainSchema>();

                    Ok(Some(FieldValue::owned_any(schema.clone())))
                })
            },
        ));

    let builder = builder
        .register(schema_info)
        .register(label_info)
        .register(role_info)
        .register(unicity_info)
        .register(role_type)
        .register(role_type_e)
        .register(claim_type_info)
        .register(storage_info)
        .register(primitive_info);

    (root_query, builder)
}
