use async_graphql::{dynamic::Schema as GqlSchema, dynamic::*};
use core_model::{Role, RESERVED_ROLE_ANNOTATION};

use crate::AppState;

mod mutation;
mod query;

const QUERY_NAME: &str = "SchemaQuery";
const MUTATION_NAME: &str = "SchemaMutation";

pub fn build(state: AppState) -> Result<GqlSchema, SchemaError> {
    let builder = GqlSchema::build(QUERY_NAME, Some(MUTATION_NAME), None);
    let builder = builder.limit_depth(20).limit_recursive_depth(20);

    let default_schema = state.config.schema_name.clone();
    let schema = state.schema_manager.get_schema(&default_schema).unwrap();

    let builder = query::build(schema.clone(), builder);
    let builder = mutation::build(state, schema.clone(), builder);

    tracing::info!("Schema built");
    builder.data(schema).finish()
}

fn name_clean(data: &str) -> String {
    data.replace(['\\', '/', '$'], " ")
        .split(' ')
        .map(|v| {
            // first char of string to upper
            let start = v.get(0..1).unwrap_or_default().to_uppercase();
            let end = v.get(1..).unwrap_or_default();
            format!("{}{}", start, end)
        })
        .collect::<Vec<String>>()
        .join("")
}

#[derive(Debug, Clone)]
pub(crate) struct RoleArg {
    role_name: String,
    role_type: String,
}

impl RoleArg {
    pub fn new(role: &Role) -> RoleArg {
        let name = role.role_name();
        let valuetype = match role.role_type() {
            core_model::RoleType::LabelType(v) => v.primitive_type().name().to_string(),
            core_model::RoleType::ClaimType(_) => RESERVED_ROLE_ANNOTATION.to_string(),
        };
        RoleArg {
            role_name: name.to_string(),
            role_type: valuetype.to_string(),
        }
    }

    pub fn new_set(roles: &[Role]) -> Vec<RoleArg> {
        let args: Vec<RoleArg> = roles.iter().map(RoleArg::new).collect();
        args
    }

    pub fn is_annotation(&self) -> bool {
        self.role_type == RESERVED_ROLE_ANNOTATION
    }

    pub fn get_clean_name(&self) -> String {
        name_clean(&self.role_name)
    }

    pub fn get_type(&self) -> TypeRef {
        match self.role_type.as_str() {
            "string" => TypeRef::named_nn(TypeRef::STRING),
            "bool" => TypeRef::named_nn(TypeRef::BOOLEAN),
            "integer" => TypeRef::named_nn(TypeRef::INT),
            "decimal" => TypeRef::named_nn(TypeRef::FLOAT),
            "uuid" => TypeRef::named_nn(TypeRef::ID),
            _ => TypeRef::named_nn(TypeRef::STRING),
        }
    }

    pub fn parse_value(&self, accessor: &ObjectAccessor) -> Result<String, async_graphql::Error> {
        let arg_name = name_clean(&self.role_name);

        let value = match self.role_type.as_str() {
            "bool" => accessor.try_get(&arg_name)?.boolean()?.to_string(),
            "integer" => accessor.try_get(&arg_name)?.u64()?.to_string(),
            "decimal" => accessor.try_get(&arg_name)?.f64()?.to_string(),
            "uuid" | "string" => accessor.try_get(&arg_name)?.string()?.to_string(),
            _ => accessor.try_get(&arg_name)?.string()?.to_string(),
        };

        Ok(value)
    }
}
